-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
-- Date        : Wed Apr 17 14:29:24 2019
-- Host        : fpga01 running 64-bit unknown
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_AXIStream_Converter_0_0_sim_netlist.vhdl
-- Design      : design_1_AXIStream_Converter_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AXIStream_Converter_v1_0_M00_AXIS is
  port (
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    rd_en : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    INIT : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    almost_full : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    input_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    addTLAST : in STD_LOGIC;
    NUM_of_packet : in STD_LOGIC_VECTOR ( 31 downto 0 );
    empty : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AXIStream_Converter_v1_0_M00_AXIS;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AXIStream_Converter_v1_0_M00_AXIS is
  signal COUNT : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal COUNT0 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \COUNT0_carry__0_n_0\ : STD_LOGIC;
  signal \COUNT0_carry__0_n_1\ : STD_LOGIC;
  signal \COUNT0_carry__0_n_2\ : STD_LOGIC;
  signal \COUNT0_carry__0_n_3\ : STD_LOGIC;
  signal \COUNT0_carry__1_n_0\ : STD_LOGIC;
  signal \COUNT0_carry__1_n_1\ : STD_LOGIC;
  signal \COUNT0_carry__1_n_2\ : STD_LOGIC;
  signal \COUNT0_carry__1_n_3\ : STD_LOGIC;
  signal \COUNT0_carry__2_n_0\ : STD_LOGIC;
  signal \COUNT0_carry__2_n_1\ : STD_LOGIC;
  signal \COUNT0_carry__2_n_2\ : STD_LOGIC;
  signal \COUNT0_carry__2_n_3\ : STD_LOGIC;
  signal \COUNT0_carry__3_n_0\ : STD_LOGIC;
  signal \COUNT0_carry__3_n_1\ : STD_LOGIC;
  signal \COUNT0_carry__3_n_2\ : STD_LOGIC;
  signal \COUNT0_carry__3_n_3\ : STD_LOGIC;
  signal \COUNT0_carry__4_n_0\ : STD_LOGIC;
  signal \COUNT0_carry__4_n_1\ : STD_LOGIC;
  signal \COUNT0_carry__4_n_2\ : STD_LOGIC;
  signal \COUNT0_carry__4_n_3\ : STD_LOGIC;
  signal \COUNT0_carry__5_n_0\ : STD_LOGIC;
  signal \COUNT0_carry__5_n_1\ : STD_LOGIC;
  signal \COUNT0_carry__5_n_2\ : STD_LOGIC;
  signal \COUNT0_carry__5_n_3\ : STD_LOGIC;
  signal \COUNT0_carry__6_n_2\ : STD_LOGIC;
  signal \COUNT0_carry__6_n_3\ : STD_LOGIC;
  signal COUNT0_carry_n_0 : STD_LOGIC;
  signal COUNT0_carry_n_1 : STD_LOGIC;
  signal COUNT0_carry_n_2 : STD_LOGIC;
  signal COUNT0_carry_n_3 : STD_LOGIC;
  signal COUNT2 : STD_LOGIC;
  signal \COUNT2_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \COUNT2_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \COUNT2_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \COUNT2_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \COUNT2_carry__0_n_0\ : STD_LOGIC;
  signal \COUNT2_carry__0_n_1\ : STD_LOGIC;
  signal \COUNT2_carry__0_n_2\ : STD_LOGIC;
  signal \COUNT2_carry__0_n_3\ : STD_LOGIC;
  signal \COUNT2_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \COUNT2_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \COUNT2_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \COUNT2_carry__1_n_2\ : STD_LOGIC;
  signal \COUNT2_carry__1_n_3\ : STD_LOGIC;
  signal COUNT2_carry_i_1_n_0 : STD_LOGIC;
  signal COUNT2_carry_i_2_n_0 : STD_LOGIC;
  signal COUNT2_carry_i_3_n_0 : STD_LOGIC;
  signal COUNT2_carry_i_4_n_0 : STD_LOGIC;
  signal COUNT2_carry_n_0 : STD_LOGIC;
  signal COUNT2_carry_n_1 : STD_LOGIC;
  signal COUNT2_carry_n_2 : STD_LOGIC;
  signal COUNT2_carry_n_3 : STD_LOGIC;
  signal COUNT3 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \COUNT3_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__0_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__0_n_1\ : STD_LOGIC;
  signal \COUNT3_carry__0_n_2\ : STD_LOGIC;
  signal \COUNT3_carry__0_n_3\ : STD_LOGIC;
  signal \COUNT3_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__1_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__1_n_1\ : STD_LOGIC;
  signal \COUNT3_carry__1_n_2\ : STD_LOGIC;
  signal \COUNT3_carry__1_n_3\ : STD_LOGIC;
  signal \COUNT3_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__2_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__2_n_1\ : STD_LOGIC;
  signal \COUNT3_carry__2_n_2\ : STD_LOGIC;
  signal \COUNT3_carry__2_n_3\ : STD_LOGIC;
  signal \COUNT3_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__3_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__3_n_1\ : STD_LOGIC;
  signal \COUNT3_carry__3_n_2\ : STD_LOGIC;
  signal \COUNT3_carry__3_n_3\ : STD_LOGIC;
  signal \COUNT3_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__4_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__4_n_1\ : STD_LOGIC;
  signal \COUNT3_carry__4_n_2\ : STD_LOGIC;
  signal \COUNT3_carry__4_n_3\ : STD_LOGIC;
  signal \COUNT3_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__5_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__5_n_1\ : STD_LOGIC;
  signal \COUNT3_carry__5_n_2\ : STD_LOGIC;
  signal \COUNT3_carry__5_n_3\ : STD_LOGIC;
  signal \COUNT3_carry__6_i_1_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__6_i_2_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__6_i_3_n_0\ : STD_LOGIC;
  signal \COUNT3_carry__6_n_2\ : STD_LOGIC;
  signal \COUNT3_carry__6_n_3\ : STD_LOGIC;
  signal COUNT3_carry_i_1_n_0 : STD_LOGIC;
  signal COUNT3_carry_i_2_n_0 : STD_LOGIC;
  signal COUNT3_carry_i_3_n_0 : STD_LOGIC;
  signal COUNT3_carry_i_4_n_0 : STD_LOGIC;
  signal COUNT3_carry_n_0 : STD_LOGIC;
  signal COUNT3_carry_n_1 : STD_LOGIC;
  signal COUNT3_carry_n_2 : STD_LOGIC;
  signal COUNT3_carry_n_3 : STD_LOGIC;
  signal \COUNT[31]_i_2_n_0\ : STD_LOGIC;
  signal \COUNT[31]_i_4_n_0\ : STD_LOGIC;
  signal COUNT_0 : STD_LOGIC;
  signal M_AXIS_TDATA_reg : STD_LOGIC_VECTOR ( 26 downto 0 );
  signal \M_AXIS_TDATA_reg[14]_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[16]_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[16]_i_3_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[16]_i_4_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[16]_i_5_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[17]_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[18]_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[18]_i_3_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[18]_i_4_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[21]_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[22]_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[24]_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[24]_i_3_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[25]_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[26]_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[29]_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[30]_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[30]_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[30]_i_4_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[31]_i_3_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[31]_i_4_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[31]_i_5_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[5]_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[6]_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TDATA_reg[8]_i_2_n_0\ : STD_LOGIC;
  signal M_AXIS_TLAST_reg : STD_LOGIC;
  signal M_AXIS_TLAST_reg_i_2_n_0 : STD_LOGIC;
  signal M_AXIS_TVALID_reg11_out : STD_LOGIC;
  signal M_AXIS_TVALID_reg_i_1_n_0 : STD_LOGIC;
  signal TLAST_COUNT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \TLAST_COUNT[0]_i_1_n_0\ : STD_LOGIC;
  signal \TLAST_COUNT[0]_i_2_n_0\ : STD_LOGIC;
  signal \TLAST_COUNT[0]_i_3_n_0\ : STD_LOGIC;
  signal \TLAST_COUNT[0]_i_4_n_0\ : STD_LOGIC;
  signal \TLAST_COUNT[0]_i_5_n_0\ : STD_LOGIC;
  signal \TLAST_COUNT[0]_i_6_n_0\ : STD_LOGIC;
  signal \TLAST_COUNT[0]_i_7_n_0\ : STD_LOGIC;
  signal \TLAST_COUNT[1]_i_1_n_0\ : STD_LOGIC;
  signal \TLAST_COUNT[2]_i_1_n_0\ : STD_LOGIC;
  signal \TLAST_COUNT[2]_i_2_n_0\ : STD_LOGIC;
  signal \TLAST_COUNT[2]_i_3_n_0\ : STD_LOGIC;
  signal \TLAST_COUNT[2]_i_4_n_0\ : STD_LOGIC;
  signal TLAST_FLAG_i_1_n_0 : STD_LOGIC;
  signal TLAST_FLAG_reg_n_0 : STD_LOGIC;
  signal addTLAST_VETO : STD_LOGIC;
  signal addTLAST_VETO_i_1_n_0 : STD_LOGIC;
  signal addTLAST_VETO_reg_n_0 : STD_LOGIC;
  signal input_DATA_VALID_FLAG : STD_LOGIC;
  signal input_DATA_VALID_FLAG_i_1_n_0 : STD_LOGIC;
  signal input_data_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \input_data_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \input_data_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal input_data_reg_VALID_FLAG_i_1_n_0 : STD_LOGIC;
  signal input_data_reg_VALID_FLAG_i_2_n_0 : STD_LOGIC;
  signal input_data_reg_VALID_FLAG_i_3_n_0 : STD_LOGIC;
  signal input_data_reg_VALID_FLAG_i_4_n_0 : STD_LOGIC;
  signal input_data_reg_VALID_FLAG_i_5_n_0 : STD_LOGIC;
  signal input_data_reg_VALID_FLAG_reg_n_0 : STD_LOGIC;
  signal \^m00_axis_tlast\ : STD_LOGIC;
  signal \^m00_axis_tvalid\ : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^rd_en\ : STD_LOGIC;
  signal rd_en_reg_i_1_n_0 : STD_LOGIC;
  signal rd_en_reg_i_2_n_0 : STD_LOGIC;
  signal rd_en_reg_i_3_n_0 : STD_LOGIC;
  signal rd_en_reg_i_4_n_0 : STD_LOGIC;
  signal \NLW_COUNT0_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_COUNT0_carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_COUNT2_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_COUNT2_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_COUNT2_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_COUNT2_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_COUNT3_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_COUNT3_carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \COUNT[0]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \COUNT[10]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \COUNT[11]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \COUNT[12]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \COUNT[13]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \COUNT[14]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \COUNT[15]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \COUNT[16]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \COUNT[17]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \COUNT[18]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \COUNT[19]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \COUNT[1]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \COUNT[20]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \COUNT[21]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \COUNT[22]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \COUNT[23]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \COUNT[24]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \COUNT[25]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \COUNT[26]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \COUNT[27]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \COUNT[28]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \COUNT[29]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \COUNT[2]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \COUNT[30]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \COUNT[31]_i_3\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \COUNT[3]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \COUNT[4]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \COUNT[5]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \COUNT[6]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \COUNT[7]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \COUNT[8]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \COUNT[9]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[0]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[13]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[14]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[16]_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[16]_i_4\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[16]_i_5\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[17]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[17]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[18]_i_2\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[21]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[22]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[26]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[27]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[29]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[30]_i_2\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[30]_i_3\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[31]_i_3\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[31]_i_4\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[5]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[6]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \M_AXIS_TDATA_reg[8]_i_2\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \TLAST_COUNT[0]_i_2\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \TLAST_COUNT[0]_i_6\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \TLAST_COUNT[0]_i_7\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \TLAST_COUNT[2]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \TLAST_COUNT[2]_i_3\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of addTLAST_VETO_i_1 : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of input_data_reg_VALID_FLAG_i_3 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of input_data_reg_VALID_FLAG_i_4 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of input_data_reg_VALID_FLAG_i_5 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of rd_en_reg_i_3 : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of rd_en_reg_i_5 : label is "soft_lutpair4";
begin
  m00_axis_tlast <= \^m00_axis_tlast\;
  m00_axis_tvalid <= \^m00_axis_tvalid\;
  rd_en <= \^rd_en\;
COUNT0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => COUNT0_carry_n_0,
      CO(2) => COUNT0_carry_n_1,
      CO(1) => COUNT0_carry_n_2,
      CO(0) => COUNT0_carry_n_3,
      CYINIT => COUNT(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => COUNT0(4 downto 1),
      S(3 downto 0) => COUNT(4 downto 1)
    );
\COUNT0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => COUNT0_carry_n_0,
      CO(3) => \COUNT0_carry__0_n_0\,
      CO(2) => \COUNT0_carry__0_n_1\,
      CO(1) => \COUNT0_carry__0_n_2\,
      CO(0) => \COUNT0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => COUNT0(8 downto 5),
      S(3 downto 0) => COUNT(8 downto 5)
    );
\COUNT0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \COUNT0_carry__0_n_0\,
      CO(3) => \COUNT0_carry__1_n_0\,
      CO(2) => \COUNT0_carry__1_n_1\,
      CO(1) => \COUNT0_carry__1_n_2\,
      CO(0) => \COUNT0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => COUNT0(12 downto 9),
      S(3 downto 0) => COUNT(12 downto 9)
    );
\COUNT0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \COUNT0_carry__1_n_0\,
      CO(3) => \COUNT0_carry__2_n_0\,
      CO(2) => \COUNT0_carry__2_n_1\,
      CO(1) => \COUNT0_carry__2_n_2\,
      CO(0) => \COUNT0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => COUNT0(16 downto 13),
      S(3 downto 0) => COUNT(16 downto 13)
    );
\COUNT0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \COUNT0_carry__2_n_0\,
      CO(3) => \COUNT0_carry__3_n_0\,
      CO(2) => \COUNT0_carry__3_n_1\,
      CO(1) => \COUNT0_carry__3_n_2\,
      CO(0) => \COUNT0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => COUNT0(20 downto 17),
      S(3 downto 0) => COUNT(20 downto 17)
    );
\COUNT0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \COUNT0_carry__3_n_0\,
      CO(3) => \COUNT0_carry__4_n_0\,
      CO(2) => \COUNT0_carry__4_n_1\,
      CO(1) => \COUNT0_carry__4_n_2\,
      CO(0) => \COUNT0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => COUNT0(24 downto 21),
      S(3 downto 0) => COUNT(24 downto 21)
    );
\COUNT0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \COUNT0_carry__4_n_0\,
      CO(3) => \COUNT0_carry__5_n_0\,
      CO(2) => \COUNT0_carry__5_n_1\,
      CO(1) => \COUNT0_carry__5_n_2\,
      CO(0) => \COUNT0_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => COUNT0(28 downto 25),
      S(3 downto 0) => COUNT(28 downto 25)
    );
\COUNT0_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \COUNT0_carry__5_n_0\,
      CO(3 downto 2) => \NLW_COUNT0_carry__6_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \COUNT0_carry__6_n_2\,
      CO(0) => \COUNT0_carry__6_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_COUNT0_carry__6_O_UNCONNECTED\(3),
      O(2 downto 0) => COUNT0(31 downto 29),
      S(3) => '0',
      S(2 downto 0) => COUNT(31 downto 29)
    );
COUNT2_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => COUNT2_carry_n_0,
      CO(2) => COUNT2_carry_n_1,
      CO(1) => COUNT2_carry_n_2,
      CO(0) => COUNT2_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_COUNT2_carry_O_UNCONNECTED(3 downto 0),
      S(3) => COUNT2_carry_i_1_n_0,
      S(2) => COUNT2_carry_i_2_n_0,
      S(1) => COUNT2_carry_i_3_n_0,
      S(0) => COUNT2_carry_i_4_n_0
    );
\COUNT2_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => COUNT2_carry_n_0,
      CO(3) => \COUNT2_carry__0_n_0\,
      CO(2) => \COUNT2_carry__0_n_1\,
      CO(1) => \COUNT2_carry__0_n_2\,
      CO(0) => \COUNT2_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_COUNT2_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \COUNT2_carry__0_i_1_n_0\,
      S(2) => \COUNT2_carry__0_i_2_n_0\,
      S(1) => \COUNT2_carry__0_i_3_n_0\,
      S(0) => \COUNT2_carry__0_i_4_n_0\
    );
\COUNT2_carry__0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => COUNT3(23),
      I1 => COUNT(23),
      I2 => COUNT3(22),
      I3 => COUNT(22),
      I4 => COUNT(21),
      I5 => COUNT3(21),
      O => \COUNT2_carry__0_i_1_n_0\
    );
\COUNT2_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => COUNT3(20),
      I1 => COUNT(20),
      I2 => COUNT3(19),
      I3 => COUNT(19),
      I4 => COUNT(18),
      I5 => COUNT3(18),
      O => \COUNT2_carry__0_i_2_n_0\
    );
\COUNT2_carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => COUNT3(17),
      I1 => COUNT(17),
      I2 => COUNT3(16),
      I3 => COUNT(16),
      I4 => COUNT(15),
      I5 => COUNT3(15),
      O => \COUNT2_carry__0_i_3_n_0\
    );
\COUNT2_carry__0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => COUNT3(14),
      I1 => COUNT(14),
      I2 => COUNT3(13),
      I3 => COUNT(13),
      I4 => COUNT(12),
      I5 => COUNT3(12),
      O => \COUNT2_carry__0_i_4_n_0\
    );
\COUNT2_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \COUNT2_carry__0_n_0\,
      CO(3) => \NLW_COUNT2_carry__1_CO_UNCONNECTED\(3),
      CO(2) => COUNT2,
      CO(1) => \COUNT2_carry__1_n_2\,
      CO(0) => \COUNT2_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_COUNT2_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => \COUNT2_carry__1_i_1_n_0\,
      S(1) => \COUNT2_carry__1_i_2_n_0\,
      S(0) => \COUNT2_carry__1_i_3_n_0\
    );
\COUNT2_carry__1_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => COUNT3(31),
      I1 => COUNT(31),
      I2 => COUNT3(30),
      I3 => COUNT(30),
      O => \COUNT2_carry__1_i_1_n_0\
    );
\COUNT2_carry__1_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => COUNT3(29),
      I1 => COUNT(29),
      I2 => COUNT3(28),
      I3 => COUNT(28),
      I4 => COUNT(27),
      I5 => COUNT3(27),
      O => \COUNT2_carry__1_i_2_n_0\
    );
\COUNT2_carry__1_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => COUNT3(26),
      I1 => COUNT(26),
      I2 => COUNT3(25),
      I3 => COUNT(25),
      I4 => COUNT(24),
      I5 => COUNT3(24),
      O => \COUNT2_carry__1_i_3_n_0\
    );
COUNT2_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => COUNT3(11),
      I1 => COUNT(11),
      I2 => COUNT3(10),
      I3 => COUNT(10),
      I4 => COUNT(9),
      I5 => COUNT3(9),
      O => COUNT2_carry_i_1_n_0
    );
COUNT2_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => COUNT3(8),
      I1 => COUNT(8),
      I2 => COUNT3(7),
      I3 => COUNT(7),
      I4 => COUNT(6),
      I5 => COUNT3(6),
      O => COUNT2_carry_i_2_n_0
    );
COUNT2_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => COUNT3(5),
      I1 => COUNT(5),
      I2 => COUNT3(4),
      I3 => COUNT(4),
      I4 => COUNT(3),
      I5 => COUNT3(3),
      O => COUNT2_carry_i_3_n_0
    );
COUNT2_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6006000000006006"
    )
        port map (
      I0 => COUNT(0),
      I1 => NUM_of_packet(0),
      I2 => COUNT3(2),
      I3 => COUNT(2),
      I4 => COUNT(1),
      I5 => COUNT3(1),
      O => COUNT2_carry_i_4_n_0
    );
COUNT3_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => COUNT3_carry_n_0,
      CO(2) => COUNT3_carry_n_1,
      CO(1) => COUNT3_carry_n_2,
      CO(0) => COUNT3_carry_n_3,
      CYINIT => NUM_of_packet(0),
      DI(3 downto 0) => NUM_of_packet(4 downto 1),
      O(3 downto 0) => COUNT3(4 downto 1),
      S(3) => COUNT3_carry_i_1_n_0,
      S(2) => COUNT3_carry_i_2_n_0,
      S(1) => COUNT3_carry_i_3_n_0,
      S(0) => COUNT3_carry_i_4_n_0
    );
\COUNT3_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => COUNT3_carry_n_0,
      CO(3) => \COUNT3_carry__0_n_0\,
      CO(2) => \COUNT3_carry__0_n_1\,
      CO(1) => \COUNT3_carry__0_n_2\,
      CO(0) => \COUNT3_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => NUM_of_packet(8 downto 5),
      O(3 downto 0) => COUNT3(8 downto 5),
      S(3) => \COUNT3_carry__0_i_1_n_0\,
      S(2) => \COUNT3_carry__0_i_2_n_0\,
      S(1) => \COUNT3_carry__0_i_3_n_0\,
      S(0) => \COUNT3_carry__0_i_4_n_0\
    );
\COUNT3_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(8),
      O => \COUNT3_carry__0_i_1_n_0\
    );
\COUNT3_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(7),
      O => \COUNT3_carry__0_i_2_n_0\
    );
\COUNT3_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(6),
      O => \COUNT3_carry__0_i_3_n_0\
    );
\COUNT3_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(5),
      O => \COUNT3_carry__0_i_4_n_0\
    );
\COUNT3_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \COUNT3_carry__0_n_0\,
      CO(3) => \COUNT3_carry__1_n_0\,
      CO(2) => \COUNT3_carry__1_n_1\,
      CO(1) => \COUNT3_carry__1_n_2\,
      CO(0) => \COUNT3_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => NUM_of_packet(12 downto 9),
      O(3 downto 0) => COUNT3(12 downto 9),
      S(3) => \COUNT3_carry__1_i_1_n_0\,
      S(2) => \COUNT3_carry__1_i_2_n_0\,
      S(1) => \COUNT3_carry__1_i_3_n_0\,
      S(0) => \COUNT3_carry__1_i_4_n_0\
    );
\COUNT3_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(12),
      O => \COUNT3_carry__1_i_1_n_0\
    );
\COUNT3_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(11),
      O => \COUNT3_carry__1_i_2_n_0\
    );
\COUNT3_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(10),
      O => \COUNT3_carry__1_i_3_n_0\
    );
\COUNT3_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(9),
      O => \COUNT3_carry__1_i_4_n_0\
    );
\COUNT3_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \COUNT3_carry__1_n_0\,
      CO(3) => \COUNT3_carry__2_n_0\,
      CO(2) => \COUNT3_carry__2_n_1\,
      CO(1) => \COUNT3_carry__2_n_2\,
      CO(0) => \COUNT3_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => NUM_of_packet(16 downto 13),
      O(3 downto 0) => COUNT3(16 downto 13),
      S(3) => \COUNT3_carry__2_i_1_n_0\,
      S(2) => \COUNT3_carry__2_i_2_n_0\,
      S(1) => \COUNT3_carry__2_i_3_n_0\,
      S(0) => \COUNT3_carry__2_i_4_n_0\
    );
\COUNT3_carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(16),
      O => \COUNT3_carry__2_i_1_n_0\
    );
\COUNT3_carry__2_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(15),
      O => \COUNT3_carry__2_i_2_n_0\
    );
\COUNT3_carry__2_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(14),
      O => \COUNT3_carry__2_i_3_n_0\
    );
\COUNT3_carry__2_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(13),
      O => \COUNT3_carry__2_i_4_n_0\
    );
\COUNT3_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \COUNT3_carry__2_n_0\,
      CO(3) => \COUNT3_carry__3_n_0\,
      CO(2) => \COUNT3_carry__3_n_1\,
      CO(1) => \COUNT3_carry__3_n_2\,
      CO(0) => \COUNT3_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => NUM_of_packet(20 downto 17),
      O(3 downto 0) => COUNT3(20 downto 17),
      S(3) => \COUNT3_carry__3_i_1_n_0\,
      S(2) => \COUNT3_carry__3_i_2_n_0\,
      S(1) => \COUNT3_carry__3_i_3_n_0\,
      S(0) => \COUNT3_carry__3_i_4_n_0\
    );
\COUNT3_carry__3_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(20),
      O => \COUNT3_carry__3_i_1_n_0\
    );
\COUNT3_carry__3_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(19),
      O => \COUNT3_carry__3_i_2_n_0\
    );
\COUNT3_carry__3_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(18),
      O => \COUNT3_carry__3_i_3_n_0\
    );
\COUNT3_carry__3_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(17),
      O => \COUNT3_carry__3_i_4_n_0\
    );
\COUNT3_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \COUNT3_carry__3_n_0\,
      CO(3) => \COUNT3_carry__4_n_0\,
      CO(2) => \COUNT3_carry__4_n_1\,
      CO(1) => \COUNT3_carry__4_n_2\,
      CO(0) => \COUNT3_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => NUM_of_packet(24 downto 21),
      O(3 downto 0) => COUNT3(24 downto 21),
      S(3) => \COUNT3_carry__4_i_1_n_0\,
      S(2) => \COUNT3_carry__4_i_2_n_0\,
      S(1) => \COUNT3_carry__4_i_3_n_0\,
      S(0) => \COUNT3_carry__4_i_4_n_0\
    );
\COUNT3_carry__4_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(24),
      O => \COUNT3_carry__4_i_1_n_0\
    );
\COUNT3_carry__4_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(23),
      O => \COUNT3_carry__4_i_2_n_0\
    );
\COUNT3_carry__4_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(22),
      O => \COUNT3_carry__4_i_3_n_0\
    );
\COUNT3_carry__4_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(21),
      O => \COUNT3_carry__4_i_4_n_0\
    );
\COUNT3_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \COUNT3_carry__4_n_0\,
      CO(3) => \COUNT3_carry__5_n_0\,
      CO(2) => \COUNT3_carry__5_n_1\,
      CO(1) => \COUNT3_carry__5_n_2\,
      CO(0) => \COUNT3_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => NUM_of_packet(28 downto 25),
      O(3 downto 0) => COUNT3(28 downto 25),
      S(3) => \COUNT3_carry__5_i_1_n_0\,
      S(2) => \COUNT3_carry__5_i_2_n_0\,
      S(1) => \COUNT3_carry__5_i_3_n_0\,
      S(0) => \COUNT3_carry__5_i_4_n_0\
    );
\COUNT3_carry__5_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(28),
      O => \COUNT3_carry__5_i_1_n_0\
    );
\COUNT3_carry__5_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(27),
      O => \COUNT3_carry__5_i_2_n_0\
    );
\COUNT3_carry__5_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(26),
      O => \COUNT3_carry__5_i_3_n_0\
    );
\COUNT3_carry__5_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(25),
      O => \COUNT3_carry__5_i_4_n_0\
    );
\COUNT3_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \COUNT3_carry__5_n_0\,
      CO(3 downto 2) => \NLW_COUNT3_carry__6_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \COUNT3_carry__6_n_2\,
      CO(0) => \COUNT3_carry__6_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1 downto 0) => NUM_of_packet(30 downto 29),
      O(3) => \NLW_COUNT3_carry__6_O_UNCONNECTED\(3),
      O(2 downto 0) => COUNT3(31 downto 29),
      S(3) => '0',
      S(2) => \COUNT3_carry__6_i_1_n_0\,
      S(1) => \COUNT3_carry__6_i_2_n_0\,
      S(0) => \COUNT3_carry__6_i_3_n_0\
    );
\COUNT3_carry__6_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(31),
      O => \COUNT3_carry__6_i_1_n_0\
    );
\COUNT3_carry__6_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(30),
      O => \COUNT3_carry__6_i_2_n_0\
    );
\COUNT3_carry__6_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(29),
      O => \COUNT3_carry__6_i_3_n_0\
    );
COUNT3_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(4),
      O => COUNT3_carry_i_1_n_0
    );
COUNT3_carry_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(3),
      O => COUNT3_carry_i_2_n_0
    );
COUNT3_carry_i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(2),
      O => COUNT3_carry_i_3_n_0
    );
COUNT3_carry_i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => NUM_of_packet(1),
      O => COUNT3_carry_i_4_n_0
    );
\COUNT[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT(0),
      O => p_1_in(0)
    );
\COUNT[10]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(10),
      O => p_1_in(10)
    );
\COUNT[11]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(11),
      O => p_1_in(11)
    );
\COUNT[12]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(12),
      O => p_1_in(12)
    );
\COUNT[13]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(13),
      O => p_1_in(13)
    );
\COUNT[14]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(14),
      O => p_1_in(14)
    );
\COUNT[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(15),
      O => p_1_in(15)
    );
\COUNT[16]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(16),
      O => p_1_in(16)
    );
\COUNT[17]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(17),
      O => p_1_in(17)
    );
\COUNT[18]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(18),
      O => p_1_in(18)
    );
\COUNT[19]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(19),
      O => p_1_in(19)
    );
\COUNT[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(1),
      O => p_1_in(1)
    );
\COUNT[20]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(20),
      O => p_1_in(20)
    );
\COUNT[21]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(21),
      O => p_1_in(21)
    );
\COUNT[22]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(22),
      O => p_1_in(22)
    );
\COUNT[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(23),
      O => p_1_in(23)
    );
\COUNT[24]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(24),
      O => p_1_in(24)
    );
\COUNT[25]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(25),
      O => p_1_in(25)
    );
\COUNT[26]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(26),
      O => p_1_in(26)
    );
\COUNT[27]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(27),
      O => p_1_in(27)
    );
\COUNT[28]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(28),
      O => p_1_in(28)
    );
\COUNT[29]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(29),
      O => p_1_in(29)
    );
\COUNT[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(2),
      O => p_1_in(2)
    );
\COUNT[30]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(30),
      O => p_1_in(30)
    );
\COUNT[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0400000000000000"
    )
        port map (
      I0 => TLAST_FLAG_reg_n_0,
      I1 => m00_axis_tready,
      I2 => INIT,
      I3 => m00_axis_aresetn,
      I4 => \^m00_axis_tvalid\,
      I5 => COUNT2,
      O => COUNT_0
    );
\COUNT[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F4F4F4F4F4F4F4FF"
    )
        port map (
      I0 => addTLAST_VETO_reg_n_0,
      I1 => addTLAST,
      I2 => addTLAST_VETO,
      I3 => rd_en_reg_i_2_n_0,
      I4 => \^m00_axis_tlast\,
      I5 => TLAST_FLAG_reg_n_0,
      O => \COUNT[31]_i_2_n_0\
    );
\COUNT[31]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(31),
      O => p_1_in(31)
    );
\COUNT[31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000004000000"
    )
        port map (
      I0 => TLAST_FLAG_reg_n_0,
      I1 => m00_axis_tready,
      I2 => INIT,
      I3 => m00_axis_aresetn,
      I4 => \^m00_axis_tvalid\,
      I5 => \^m00_axis_tlast\,
      O => \COUNT[31]_i_4_n_0\
    );
\COUNT[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(3),
      O => p_1_in(3)
    );
\COUNT[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(4),
      O => p_1_in(4)
    );
\COUNT[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(5),
      O => p_1_in(5)
    );
\COUNT[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(6),
      O => p_1_in(6)
    );
\COUNT[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(7),
      O => p_1_in(7)
    );
\COUNT[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(8),
      O => p_1_in(8)
    );
\COUNT[9]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \COUNT[31]_i_4_n_0\,
      I1 => COUNT0(9),
      O => p_1_in(9)
    );
\COUNT_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(0),
      Q => COUNT(0),
      R => COUNT_0
    );
\COUNT_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(10),
      Q => COUNT(10),
      R => COUNT_0
    );
\COUNT_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(11),
      Q => COUNT(11),
      R => COUNT_0
    );
\COUNT_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(12),
      Q => COUNT(12),
      R => COUNT_0
    );
\COUNT_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(13),
      Q => COUNT(13),
      R => COUNT_0
    );
\COUNT_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(14),
      Q => COUNT(14),
      R => COUNT_0
    );
\COUNT_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(15),
      Q => COUNT(15),
      R => COUNT_0
    );
\COUNT_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(16),
      Q => COUNT(16),
      R => COUNT_0
    );
\COUNT_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(17),
      Q => COUNT(17),
      R => COUNT_0
    );
\COUNT_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(18),
      Q => COUNT(18),
      R => COUNT_0
    );
\COUNT_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(19),
      Q => COUNT(19),
      R => COUNT_0
    );
\COUNT_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(1),
      Q => COUNT(1),
      R => COUNT_0
    );
\COUNT_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(20),
      Q => COUNT(20),
      R => COUNT_0
    );
\COUNT_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(21),
      Q => COUNT(21),
      R => COUNT_0
    );
\COUNT_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(22),
      Q => COUNT(22),
      R => COUNT_0
    );
\COUNT_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(23),
      Q => COUNT(23),
      R => COUNT_0
    );
\COUNT_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(24),
      Q => COUNT(24),
      R => COUNT_0
    );
\COUNT_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(25),
      Q => COUNT(25),
      R => COUNT_0
    );
\COUNT_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(26),
      Q => COUNT(26),
      R => COUNT_0
    );
\COUNT_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(27),
      Q => COUNT(27),
      R => COUNT_0
    );
\COUNT_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(28),
      Q => COUNT(28),
      R => COUNT_0
    );
\COUNT_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(29),
      Q => COUNT(29),
      R => COUNT_0
    );
\COUNT_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(2),
      Q => COUNT(2),
      R => COUNT_0
    );
\COUNT_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(30),
      Q => COUNT(30),
      R => COUNT_0
    );
\COUNT_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(31),
      Q => COUNT(31),
      R => COUNT_0
    );
\COUNT_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(3),
      Q => COUNT(3),
      R => COUNT_0
    );
\COUNT_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(4),
      Q => COUNT(4),
      R => COUNT_0
    );
\COUNT_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(5),
      Q => COUNT(5),
      R => COUNT_0
    );
\COUNT_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(6),
      Q => COUNT(6),
      R => COUNT_0
    );
\COUNT_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(7),
      Q => COUNT(7),
      R => COUNT_0
    );
\COUNT_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(8),
      Q => COUNT(8),
      R => COUNT_0
    );
\COUNT_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \COUNT[31]_i_2_n_0\,
      D => p_1_in(9),
      Q => COUNT(9),
      R => COUNT_0
    );
\M_AXIS_TDATA_reg[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => input_data_reg(0),
      I1 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I2 => \M_AXIS_TDATA_reg[17]_i_2_n_0\,
      O => M_AXIS_TDATA_reg(0)
    );
\M_AXIS_TDATA_reg[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888FF8"
    )
        port map (
      I0 => input_data_reg(10),
      I1 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I2 => TLAST_COUNT(1),
      I3 => TLAST_COUNT(2),
      I4 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      I5 => TLAST_COUNT(0),
      O => M_AXIS_TDATA_reg(10)
    );
\M_AXIS_TDATA_reg[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888F8F88888F8888"
    )
        port map (
      I0 => input_data_reg(11),
      I1 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I2 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      I3 => TLAST_COUNT(1),
      I4 => TLAST_COUNT(2),
      I5 => TLAST_COUNT(0),
      O => M_AXIS_TDATA_reg(11)
    );
\M_AXIS_TDATA_reg[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFF040404"
    )
        port map (
      I0 => TLAST_COUNT(2),
      I1 => TLAST_COUNT(1),
      I2 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      I3 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I4 => input_data_reg(12),
      I5 => \M_AXIS_TDATA_reg[18]_i_3_n_0\,
      O => M_AXIS_TDATA_reg(12)
    );
\M_AXIS_TDATA_reg[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => input_data_reg(13),
      I1 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I2 => \M_AXIS_TDATA_reg[24]_i_3_n_0\,
      O => M_AXIS_TDATA_reg(13)
    );
\M_AXIS_TDATA_reg[14]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I1 => input_data_reg(14),
      O => \M_AXIS_TDATA_reg[14]_i_1_n_0\
    );
\M_AXIS_TDATA_reg[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFABAAABAAABAA"
    )
        port map (
      I0 => \M_AXIS_TDATA_reg[16]_i_2_n_0\,
      I1 => \M_AXIS_TDATA_reg[16]_i_3_n_0\,
      I2 => \M_AXIS_TDATA_reg[16]_i_4_n_0\,
      I3 => \M_AXIS_TDATA_reg[16]_i_5_n_0\,
      I4 => input_data_reg(16),
      I5 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      O => M_AXIS_TDATA_reg(16)
    );
\M_AXIS_TDATA_reg[16]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0130"
    )
        port map (
      I0 => TLAST_COUNT(0),
      I1 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      I2 => TLAST_COUNT(2),
      I3 => TLAST_COUNT(1),
      O => \M_AXIS_TDATA_reg[16]_i_2_n_0\
    );
\M_AXIS_TDATA_reg[16]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => almost_full,
      I1 => TLAST_FLAG_reg_n_0,
      O => \M_AXIS_TDATA_reg[16]_i_3_n_0\
    );
\M_AXIS_TDATA_reg[16]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFDFFFFF"
    )
        port map (
      I0 => m00_axis_tready,
      I1 => INIT,
      I2 => m00_axis_aresetn,
      I3 => TLAST_COUNT(1),
      I4 => TLAST_COUNT(0),
      O => \M_AXIS_TDATA_reg[16]_i_4_n_0\
    );
\M_AXIS_TDATA_reg[16]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"C1"
    )
        port map (
      I0 => TLAST_COUNT(2),
      I1 => \^m00_axis_tvalid\,
      I2 => input_data_reg_VALID_FLAG_reg_n_0,
      O => \M_AXIS_TDATA_reg[16]_i_5_n_0\
    );
\M_AXIS_TDATA_reg[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => input_data_reg(17),
      I1 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I2 => \M_AXIS_TDATA_reg[17]_i_2_n_0\,
      O => M_AXIS_TDATA_reg(17)
    );
\M_AXIS_TDATA_reg[17]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"ABBAAAAA"
    )
        port map (
      I0 => \M_AXIS_TDATA_reg[30]_i_4_n_0\,
      I1 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      I2 => TLAST_COUNT(2),
      I3 => TLAST_COUNT(1),
      I4 => TLAST_COUNT(0),
      O => \M_AXIS_TDATA_reg[17]_i_2_n_0\
    );
\M_AXIS_TDATA_reg[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFF888"
    )
        port map (
      I0 => \M_AXIS_TDATA_reg[18]_i_2_n_0\,
      I1 => TLAST_COUNT(0),
      I2 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I3 => input_data_reg(18),
      I4 => \M_AXIS_TDATA_reg[18]_i_3_n_0\,
      O => M_AXIS_TDATA_reg(18)
    );
\M_AXIS_TDATA_reg[18]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => TLAST_COUNT(2),
      I1 => TLAST_COUNT(1),
      I2 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      O => \M_AXIS_TDATA_reg[18]_i_2_n_0\
    );
\M_AXIS_TDATA_reg[18]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000808FF08"
    )
        port map (
      I0 => TLAST_COUNT(0),
      I1 => input_data_reg_VALID_FLAG_reg_n_0,
      I2 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      I3 => \M_AXIS_TDATA_reg[18]_i_4_n_0\,
      I4 => input_data_reg_VALID_FLAG_i_3_n_0,
      I5 => TLAST_COUNT(2),
      O => \M_AXIS_TDATA_reg[18]_i_3_n_0\
    );
\M_AXIS_TDATA_reg[18]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000400000000"
    )
        port map (
      I0 => almost_full,
      I1 => TLAST_FLAG_reg_n_0,
      I2 => input_data_reg_VALID_FLAG_reg_n_0,
      I3 => \^m00_axis_tvalid\,
      I4 => TLAST_COUNT(1),
      I5 => TLAST_COUNT(0),
      O => \M_AXIS_TDATA_reg[18]_i_4_n_0\
    );
\M_AXIS_TDATA_reg[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF8F888888"
    )
        port map (
      I0 => input_data_reg(19),
      I1 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I2 => TLAST_COUNT(1),
      I3 => TLAST_COUNT(2),
      I4 => \M_AXIS_TDATA_reg[25]_i_2_n_0\,
      I5 => \M_AXIS_TDATA_reg[26]_i_2_n_0\,
      O => M_AXIS_TDATA_reg(19)
    );
\M_AXIS_TDATA_reg[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888FF8"
    )
        port map (
      I0 => input_data_reg(1),
      I1 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I2 => TLAST_COUNT(1),
      I3 => TLAST_COUNT(2),
      I4 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      I5 => TLAST_COUNT(0),
      O => M_AXIS_TDATA_reg(1)
    );
\M_AXIS_TDATA_reg[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888F8F8888888888"
    )
        port map (
      I0 => input_data_reg(20),
      I1 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I2 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      I3 => TLAST_COUNT(2),
      I4 => TLAST_COUNT(1),
      I5 => TLAST_COUNT(0),
      O => M_AXIS_TDATA_reg(20)
    );
\M_AXIS_TDATA_reg[21]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I1 => input_data_reg(21),
      O => \M_AXIS_TDATA_reg[21]_i_1_n_0\
    );
\M_AXIS_TDATA_reg[22]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I1 => input_data_reg(22),
      O => \M_AXIS_TDATA_reg[22]_i_1_n_0\
    );
\M_AXIS_TDATA_reg[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFF040404"
    )
        port map (
      I0 => TLAST_COUNT(2),
      I1 => TLAST_COUNT(1),
      I2 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      I3 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I4 => input_data_reg(24),
      I5 => \M_AXIS_TDATA_reg[24]_i_3_n_0\,
      O => M_AXIS_TDATA_reg(24)
    );
\M_AXIS_TDATA_reg[24]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFDFFFFFFFFFFF"
    )
        port map (
      I0 => TLAST_FLAG_reg_n_0,
      I1 => almost_full,
      I2 => \^m00_axis_tvalid\,
      I3 => m00_axis_aresetn,
      I4 => INIT,
      I5 => m00_axis_tready,
      O => \M_AXIS_TDATA_reg[24]_i_2_n_0\
    );
\M_AXIS_TDATA_reg[24]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0030002100000000"
    )
        port map (
      I0 => TLAST_COUNT(2),
      I1 => input_data_reg_VALID_FLAG_i_3_n_0,
      I2 => \^m00_axis_tvalid\,
      I3 => \M_AXIS_TDATA_reg[16]_i_3_n_0\,
      I4 => input_data_reg_VALID_FLAG_reg_n_0,
      I5 => input_data_reg_VALID_FLAG_i_2_n_0,
      O => \M_AXIS_TDATA_reg[24]_i_3_n_0\
    );
\M_AXIS_TDATA_reg[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8F8FFF8FFF8F8F8"
    )
        port map (
      I0 => input_data_reg(25),
      I1 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I2 => \M_AXIS_TDATA_reg[26]_i_2_n_0\,
      I3 => \M_AXIS_TDATA_reg[25]_i_2_n_0\,
      I4 => TLAST_COUNT(2),
      I5 => TLAST_COUNT(1),
      O => M_AXIS_TDATA_reg(25)
    );
\M_AXIS_TDATA_reg[25]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000200000"
    )
        port map (
      I0 => m00_axis_tready,
      I1 => addTLAST_VETO,
      I2 => \^m00_axis_tvalid\,
      I3 => almost_full,
      I4 => TLAST_FLAG_reg_n_0,
      I5 => TLAST_COUNT(0),
      O => \M_AXIS_TDATA_reg[25]_i_2_n_0\
    );
\M_AXIS_TDATA_reg[26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => input_data_reg(26),
      I1 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I2 => \M_AXIS_TDATA_reg[26]_i_2_n_0\,
      O => M_AXIS_TDATA_reg(26)
    );
\M_AXIS_TDATA_reg[26]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000400004"
    )
        port map (
      I0 => TLAST_COUNT(2),
      I1 => input_data_reg_VALID_FLAG_i_2_n_0,
      I2 => input_data_reg_VALID_FLAG_reg_n_0,
      I3 => input_data_reg_VALID_FLAG_i_3_n_0,
      I4 => \^m00_axis_tvalid\,
      I5 => \M_AXIS_TDATA_reg[16]_i_3_n_0\,
      O => \M_AXIS_TDATA_reg[26]_i_2_n_0\
    );
\M_AXIS_TDATA_reg[27]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I1 => input_data_reg(27),
      O => \M_AXIS_TDATA_reg[27]_i_1_n_0\
    );
\M_AXIS_TDATA_reg[29]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I1 => input_data_reg(29),
      O => \M_AXIS_TDATA_reg[29]_i_1_n_0\
    );
\M_AXIS_TDATA_reg[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88888F8888888FF8"
    )
        port map (
      I0 => input_data_reg(2),
      I1 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I2 => TLAST_COUNT(2),
      I3 => TLAST_COUNT(1),
      I4 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      I5 => TLAST_COUNT(0),
      O => M_AXIS_TDATA_reg(2)
    );
\M_AXIS_TDATA_reg[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFEFEF00000000"
    )
        port map (
      I0 => \M_AXIS_TDATA_reg[31]_i_3_n_0\,
      I1 => INIT,
      I2 => m00_axis_aresetn,
      I3 => almost_full,
      I4 => \M_AXIS_TDATA_reg[31]_i_4_n_0\,
      I5 => M_AXIS_TVALID_reg11_out,
      O => \M_AXIS_TDATA_reg[30]_i_1_n_0\
    );
\M_AXIS_TDATA_reg[30]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I1 => input_data_reg(30),
      O => \M_AXIS_TDATA_reg[30]_i_2_n_0\
    );
\M_AXIS_TDATA_reg[30]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABBA"
    )
        port map (
      I0 => \M_AXIS_TDATA_reg[30]_i_4_n_0\,
      I1 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      I2 => TLAST_COUNT(1),
      I3 => TLAST_COUNT(2),
      O => M_AXIS_TVALID_reg11_out
    );
\M_AXIS_TDATA_reg[30]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000100000000000"
    )
        port map (
      I0 => input_data_reg_VALID_FLAG_i_3_n_0,
      I1 => TLAST_COUNT(1),
      I2 => TLAST_COUNT(0),
      I3 => TLAST_FLAG_reg_n_0,
      I4 => almost_full,
      I5 => \M_AXIS_TDATA_reg[16]_i_5_n_0\,
      O => \M_AXIS_TDATA_reg[30]_i_4_n_0\
    );
\M_AXIS_TDATA_reg[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EFFFEFEF"
    )
        port map (
      I0 => \M_AXIS_TDATA_reg[31]_i_3_n_0\,
      I1 => INIT,
      I2 => m00_axis_aresetn,
      I3 => almost_full,
      I4 => \M_AXIS_TDATA_reg[31]_i_4_n_0\,
      I5 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      O => \M_AXIS_TDATA_reg[31]_i_1_n_0\
    );
\M_AXIS_TDATA_reg[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFFFEFEF"
    )
        port map (
      I0 => \M_AXIS_TDATA_reg[31]_i_3_n_0\,
      I1 => INIT,
      I2 => m00_axis_aresetn,
      I3 => almost_full,
      I4 => \M_AXIS_TDATA_reg[31]_i_4_n_0\,
      O => \M_AXIS_TDATA_reg[31]_i_2_n_0\
    );
\M_AXIS_TDATA_reg[31]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFF"
    )
        port map (
      I0 => TLAST_COUNT(1),
      I1 => TLAST_COUNT(2),
      I2 => almost_full,
      I3 => TLAST_COUNT(0),
      I4 => TLAST_FLAG_reg_n_0,
      O => \M_AXIS_TDATA_reg[31]_i_3_n_0\
    );
\M_AXIS_TDATA_reg[31]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00C0C084"
    )
        port map (
      I0 => input_data_reg_VALID_FLAG_reg_n_0,
      I1 => m00_axis_tready,
      I2 => \^m00_axis_tvalid\,
      I3 => TLAST_COUNT(1),
      I4 => TLAST_COUNT(2),
      O => \M_AXIS_TDATA_reg[31]_i_4_n_0\
    );
\M_AXIS_TDATA_reg[31]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000055555557"
    )
        port map (
      I0 => TLAST_FLAG_reg_n_0,
      I1 => TLAST_COUNT(0),
      I2 => almost_full,
      I3 => TLAST_COUNT(2),
      I4 => TLAST_COUNT(1),
      I5 => addTLAST_VETO,
      O => \M_AXIS_TDATA_reg[31]_i_5_n_0\
    );
\M_AXIS_TDATA_reg[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88888F8888888FF8"
    )
        port map (
      I0 => input_data_reg(3),
      I1 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I2 => TLAST_COUNT(1),
      I3 => TLAST_COUNT(2),
      I4 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      I5 => TLAST_COUNT(0),
      O => M_AXIS_TDATA_reg(3)
    );
\M_AXIS_TDATA_reg[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888F88888"
    )
        port map (
      I0 => input_data_reg(4),
      I1 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I2 => TLAST_COUNT(0),
      I3 => TLAST_COUNT(2),
      I4 => TLAST_COUNT(1),
      I5 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      O => M_AXIS_TDATA_reg(4)
    );
\M_AXIS_TDATA_reg[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I1 => input_data_reg(5),
      O => \M_AXIS_TDATA_reg[5]_i_1_n_0\
    );
\M_AXIS_TDATA_reg[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I1 => input_data_reg(6),
      O => \M_AXIS_TDATA_reg[6]_i_1_n_0\
    );
\M_AXIS_TDATA_reg[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEEE"
    )
        port map (
      I0 => \M_AXIS_TDATA_reg[8]_i_2_n_0\,
      I1 => \M_AXIS_TDATA_reg[18]_i_3_n_0\,
      I2 => input_data_reg(8),
      I3 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      O => M_AXIS_TDATA_reg(8)
    );
\M_AXIS_TDATA_reg[8]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0130"
    )
        port map (
      I0 => TLAST_COUNT(0),
      I1 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      I2 => TLAST_COUNT(1),
      I3 => TLAST_COUNT(2),
      O => \M_AXIS_TDATA_reg[8]_i_2_n_0\
    );
\M_AXIS_TDATA_reg[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF8F888888"
    )
        port map (
      I0 => input_data_reg(9),
      I1 => \M_AXIS_TDATA_reg[31]_i_5_n_0\,
      I2 => TLAST_COUNT(1),
      I3 => TLAST_COUNT(2),
      I4 => \M_AXIS_TDATA_reg[25]_i_2_n_0\,
      I5 => \M_AXIS_TDATA_reg[26]_i_2_n_0\,
      O => M_AXIS_TDATA_reg(9)
    );
\M_AXIS_TDATA_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => M_AXIS_TDATA_reg(0),
      Q => m00_axis_tdata(0),
      R => '0'
    );
\M_AXIS_TDATA_reg_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => M_AXIS_TDATA_reg(10),
      Q => m00_axis_tdata(10),
      R => '0'
    );
\M_AXIS_TDATA_reg_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => M_AXIS_TDATA_reg(11),
      Q => m00_axis_tdata(11),
      R => '0'
    );
\M_AXIS_TDATA_reg_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => M_AXIS_TDATA_reg(12),
      Q => m00_axis_tdata(12),
      R => '0'
    );
\M_AXIS_TDATA_reg_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => M_AXIS_TDATA_reg(13),
      Q => m00_axis_tdata(13),
      R => '0'
    );
\M_AXIS_TDATA_reg_reg[14]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => \M_AXIS_TDATA_reg[14]_i_1_n_0\,
      Q => m00_axis_tdata(14),
      S => \M_AXIS_TDATA_reg[30]_i_1_n_0\
    );
\M_AXIS_TDATA_reg_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => input_data_reg(15),
      Q => m00_axis_tdata(15),
      R => \M_AXIS_TDATA_reg[31]_i_1_n_0\
    );
\M_AXIS_TDATA_reg_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => M_AXIS_TDATA_reg(16),
      Q => m00_axis_tdata(16),
      R => '0'
    );
\M_AXIS_TDATA_reg_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => M_AXIS_TDATA_reg(17),
      Q => m00_axis_tdata(17),
      R => '0'
    );
\M_AXIS_TDATA_reg_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => M_AXIS_TDATA_reg(18),
      Q => m00_axis_tdata(18),
      R => '0'
    );
\M_AXIS_TDATA_reg_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => M_AXIS_TDATA_reg(19),
      Q => m00_axis_tdata(19),
      R => '0'
    );
\M_AXIS_TDATA_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => M_AXIS_TDATA_reg(1),
      Q => m00_axis_tdata(1),
      R => '0'
    );
\M_AXIS_TDATA_reg_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => M_AXIS_TDATA_reg(20),
      Q => m00_axis_tdata(20),
      R => '0'
    );
\M_AXIS_TDATA_reg_reg[21]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => \M_AXIS_TDATA_reg[21]_i_1_n_0\,
      Q => m00_axis_tdata(21),
      S => \M_AXIS_TDATA_reg[30]_i_1_n_0\
    );
\M_AXIS_TDATA_reg_reg[22]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => \M_AXIS_TDATA_reg[22]_i_1_n_0\,
      Q => m00_axis_tdata(22),
      S => \M_AXIS_TDATA_reg[30]_i_1_n_0\
    );
\M_AXIS_TDATA_reg_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => input_data_reg(23),
      Q => m00_axis_tdata(23),
      R => \M_AXIS_TDATA_reg[31]_i_1_n_0\
    );
\M_AXIS_TDATA_reg_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => M_AXIS_TDATA_reg(24),
      Q => m00_axis_tdata(24),
      R => '0'
    );
\M_AXIS_TDATA_reg_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => M_AXIS_TDATA_reg(25),
      Q => m00_axis_tdata(25),
      R => '0'
    );
\M_AXIS_TDATA_reg_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => M_AXIS_TDATA_reg(26),
      Q => m00_axis_tdata(26),
      R => '0'
    );
\M_AXIS_TDATA_reg_reg[27]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => \M_AXIS_TDATA_reg[27]_i_1_n_0\,
      Q => m00_axis_tdata(27),
      S => \M_AXIS_TDATA_reg[30]_i_1_n_0\
    );
\M_AXIS_TDATA_reg_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => input_data_reg(28),
      Q => m00_axis_tdata(28),
      R => \M_AXIS_TDATA_reg[31]_i_1_n_0\
    );
\M_AXIS_TDATA_reg_reg[29]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => \M_AXIS_TDATA_reg[29]_i_1_n_0\,
      Q => m00_axis_tdata(29),
      S => \M_AXIS_TDATA_reg[30]_i_1_n_0\
    );
\M_AXIS_TDATA_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => M_AXIS_TDATA_reg(2),
      Q => m00_axis_tdata(2),
      R => '0'
    );
\M_AXIS_TDATA_reg_reg[30]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => \M_AXIS_TDATA_reg[30]_i_2_n_0\,
      Q => m00_axis_tdata(30),
      S => \M_AXIS_TDATA_reg[30]_i_1_n_0\
    );
\M_AXIS_TDATA_reg_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => input_data_reg(31),
      Q => m00_axis_tdata(31),
      R => \M_AXIS_TDATA_reg[31]_i_1_n_0\
    );
\M_AXIS_TDATA_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => M_AXIS_TDATA_reg(3),
      Q => m00_axis_tdata(3),
      R => '0'
    );
\M_AXIS_TDATA_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => M_AXIS_TDATA_reg(4),
      Q => m00_axis_tdata(4),
      R => '0'
    );
\M_AXIS_TDATA_reg_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => \M_AXIS_TDATA_reg[5]_i_1_n_0\,
      Q => m00_axis_tdata(5),
      S => \M_AXIS_TDATA_reg[30]_i_1_n_0\
    );
\M_AXIS_TDATA_reg_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => \M_AXIS_TDATA_reg[6]_i_1_n_0\,
      Q => m00_axis_tdata(6),
      S => \M_AXIS_TDATA_reg[30]_i_1_n_0\
    );
\M_AXIS_TDATA_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => input_data_reg(7),
      Q => m00_axis_tdata(7),
      R => \M_AXIS_TDATA_reg[31]_i_1_n_0\
    );
\M_AXIS_TDATA_reg_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => M_AXIS_TDATA_reg(8),
      Q => m00_axis_tdata(8),
      R => '0'
    );
\M_AXIS_TDATA_reg_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA_reg[31]_i_2_n_0\,
      D => M_AXIS_TDATA_reg(9),
      Q => m00_axis_tdata(9),
      R => '0'
    );
M_AXIS_TLAST_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000080000000000"
    )
        port map (
      I0 => \^m00_axis_tlast\,
      I1 => m00_axis_tready,
      I2 => addTLAST_VETO,
      I3 => \^m00_axis_tvalid\,
      I4 => almost_full,
      I5 => TLAST_FLAG_reg_n_0,
      O => M_AXIS_TLAST_reg
    );
M_AXIS_TLAST_reg_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5755555503000000"
    )
        port map (
      I0 => addTLAST_VETO,
      I1 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      I2 => TLAST_COUNT(1),
      I3 => TLAST_COUNT(0),
      I4 => TLAST_COUNT(2),
      I5 => \^m00_axis_tlast\,
      O => M_AXIS_TLAST_reg_i_2_n_0
    );
M_AXIS_TLAST_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => M_AXIS_TLAST_reg_i_2_n_0,
      Q => \^m00_axis_tlast\,
      R => M_AXIS_TLAST_reg
    );
M_AXIS_TVALID_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF2030FFFF2000"
    )
        port map (
      I0 => input_data_reg_VALID_FLAG_reg_n_0,
      I1 => INIT,
      I2 => m00_axis_aresetn,
      I3 => \M_AXIS_TDATA_reg[31]_i_3_n_0\,
      I4 => M_AXIS_TVALID_reg11_out,
      I5 => \^m00_axis_tvalid\,
      O => M_AXIS_TVALID_reg_i_1_n_0
    );
M_AXIS_TVALID_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => M_AXIS_TVALID_reg_i_1_n_0,
      Q => \^m00_axis_tvalid\,
      R => M_AXIS_TLAST_reg
    );
\TLAST_COUNT[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFF00020222000"
    )
        port map (
      I0 => \TLAST_COUNT[0]_i_2_n_0\,
      I1 => \^m00_axis_tlast\,
      I2 => \TLAST_COUNT[0]_i_3_n_0\,
      I3 => \TLAST_COUNT[0]_i_4_n_0\,
      I4 => TLAST_COUNT(0),
      I5 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      O => \TLAST_COUNT[0]_i_1_n_0\
    );
\TLAST_COUNT[0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => TLAST_COUNT(1),
      I1 => TLAST_COUNT(0),
      I2 => TLAST_COUNT(2),
      O => \TLAST_COUNT[0]_i_2_n_0\
    );
\TLAST_COUNT[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0005005500050057"
    )
        port map (
      I0 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      I1 => \M_AXIS_TDATA_reg[16]_i_3_n_0\,
      I2 => TLAST_COUNT(2),
      I3 => TLAST_COUNT(0),
      I4 => TLAST_COUNT(1),
      I5 => addTLAST_VETO,
      O => \TLAST_COUNT[0]_i_3_n_0\
    );
\TLAST_COUNT[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFEEEFEEEE"
    )
        port map (
      I0 => \TLAST_COUNT[0]_i_5_n_0\,
      I1 => \TLAST_COUNT[2]_i_3_n_0\,
      I2 => TLAST_FLAG_reg_n_0,
      I3 => rd_en_reg_i_2_n_0,
      I4 => COUNT2,
      I5 => addTLAST_VETO,
      O => \TLAST_COUNT[0]_i_4_n_0\
    );
\TLAST_COUNT[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000C00AF000C00AE"
    )
        port map (
      I0 => \TLAST_COUNT[0]_i_6_n_0\,
      I1 => \TLAST_COUNT[0]_i_7_n_0\,
      I2 => TLAST_COUNT(2),
      I3 => \M_AXIS_TDATA_reg[16]_i_3_n_0\,
      I4 => rd_en_reg_i_2_n_0,
      I5 => input_data_reg_VALID_FLAG_reg_n_0,
      O => \TLAST_COUNT[0]_i_5_n_0\
    );
\TLAST_COUNT[0]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"35"
    )
        port map (
      I0 => TLAST_COUNT(0),
      I1 => TLAST_COUNT(2),
      I2 => TLAST_COUNT(1),
      O => \TLAST_COUNT[0]_i_6_n_0\
    );
\TLAST_COUNT[0]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
        port map (
      I0 => TLAST_COUNT(1),
      I1 => input_data_reg_VALID_FLAG_reg_n_0,
      I2 => \^m00_axis_tvalid\,
      I3 => m00_axis_tready,
      O => \TLAST_COUNT[0]_i_7_n_0\
    );
\TLAST_COUNT[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAABFFFFAAAA0000"
    )
        port map (
      I0 => \M_AXIS_TDATA_reg[24]_i_3_n_0\,
      I1 => TLAST_COUNT(2),
      I2 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      I3 => TLAST_COUNT(0),
      I4 => \TLAST_COUNT[2]_i_2_n_0\,
      I5 => TLAST_COUNT(1),
      O => \TLAST_COUNT[1]_i_1_n_0\
    );
\TLAST_COUNT[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"03FF0800"
    )
        port map (
      I0 => TLAST_COUNT(0),
      I1 => TLAST_COUNT(1),
      I2 => \M_AXIS_TDATA_reg[24]_i_2_n_0\,
      I3 => \TLAST_COUNT[2]_i_2_n_0\,
      I4 => TLAST_COUNT(2),
      O => \TLAST_COUNT[2]_i_1_n_0\
    );
\TLAST_COUNT[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFABAA"
    )
        port map (
      I0 => \TLAST_COUNT[2]_i_3_n_0\,
      I1 => TLAST_FLAG_reg_n_0,
      I2 => rd_en_reg_i_2_n_0,
      I3 => COUNT2,
      I4 => addTLAST_VETO,
      I5 => \TLAST_COUNT[2]_i_4_n_0\,
      O => \TLAST_COUNT[2]_i_2_n_0\
    );
\TLAST_COUNT[2]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010000"
    )
        port map (
      I0 => TLAST_COUNT(1),
      I1 => TLAST_COUNT(0),
      I2 => TLAST_COUNT(2),
      I3 => almost_full,
      I4 => TLAST_FLAG_reg_n_0,
      O => \TLAST_COUNT[2]_i_3_n_0\
    );
\TLAST_COUNT[2]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000070006100"
    )
        port map (
      I0 => TLAST_COUNT(2),
      I1 => TLAST_COUNT(1),
      I2 => \^m00_axis_tvalid\,
      I3 => m00_axis_tready,
      I4 => input_data_reg_VALID_FLAG_reg_n_0,
      I5 => \M_AXIS_TDATA_reg[16]_i_3_n_0\,
      O => \TLAST_COUNT[2]_i_4_n_0\
    );
\TLAST_COUNT_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \TLAST_COUNT[0]_i_1_n_0\,
      Q => TLAST_COUNT(0),
      R => '0'
    );
\TLAST_COUNT_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \TLAST_COUNT[1]_i_1_n_0\,
      Q => TLAST_COUNT(1),
      R => M_AXIS_TLAST_reg
    );
\TLAST_COUNT_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \TLAST_COUNT[2]_i_1_n_0\,
      Q => TLAST_COUNT(2),
      R => M_AXIS_TLAST_reg
    );
TLAST_FLAG_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00D50055"
    )
        port map (
      I0 => rd_en_reg_i_3_n_0,
      I1 => COUNT2,
      I2 => \^m00_axis_tvalid\,
      I3 => addTLAST_VETO,
      I4 => m00_axis_tready,
      I5 => TLAST_FLAG_reg_n_0,
      O => TLAST_FLAG_i_1_n_0
    );
TLAST_FLAG_reg: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => TLAST_FLAG_i_1_n_0,
      Q => TLAST_FLAG_reg_n_0,
      R => M_AXIS_TLAST_reg
    );
addTLAST_VETO_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => INIT,
      I1 => m00_axis_aresetn,
      I2 => addTLAST,
      O => addTLAST_VETO_i_1_n_0
    );
addTLAST_VETO_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => addTLAST_VETO_i_1_n_0,
      Q => addTLAST_VETO_reg_n_0,
      R => '0'
    );
input_DATA_VALID_FLAG_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^rd_en\,
      I1 => empty,
      O => input_DATA_VALID_FLAG_i_1_n_0
    );
input_DATA_VALID_FLAG_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_DATA_VALID_FLAG_i_1_n_0,
      Q => input_DATA_VALID_FLAG,
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => INIT,
      I1 => m00_axis_aresetn,
      O => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg[31]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => TLAST_FLAG_reg_n_0,
      O => \input_data_reg[31]_i_2_n_0\
    );
input_data_reg_VALID_FLAG_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FDFFFFFD00000000"
    )
        port map (
      I0 => input_data_reg_VALID_FLAG_i_2_n_0,
      I1 => input_data_reg_VALID_FLAG_i_3_n_0,
      I2 => input_data_reg_VALID_FLAG_i_4_n_0,
      I3 => \^m00_axis_tvalid\,
      I4 => input_data_reg_VALID_FLAG_reg_n_0,
      I5 => input_data_reg_VALID_FLAG_i_5_n_0,
      O => input_data_reg_VALID_FLAG_i_1_n_0
    );
input_data_reg_VALID_FLAG_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => TLAST_COUNT(0),
      I1 => TLAST_COUNT(1),
      O => input_data_reg_VALID_FLAG_i_2_n_0
    );
input_data_reg_VALID_FLAG_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => m00_axis_aresetn,
      I1 => INIT,
      I2 => m00_axis_tready,
      O => input_data_reg_VALID_FLAG_i_3_n_0
    );
input_data_reg_VALID_FLAG_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => TLAST_FLAG_reg_n_0,
      I1 => almost_full,
      I2 => TLAST_COUNT(2),
      O => input_data_reg_VALID_FLAG_i_4_n_0
    );
input_data_reg_VALID_FLAG_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0C080008"
    )
        port map (
      I0 => input_DATA_VALID_FLAG,
      I1 => m00_axis_aresetn,
      I2 => INIT,
      I3 => TLAST_FLAG_reg_n_0,
      I4 => input_data_reg_VALID_FLAG_reg_n_0,
      O => input_data_reg_VALID_FLAG_i_5_n_0
    );
input_data_reg_VALID_FLAG_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => input_data_reg_VALID_FLAG_i_1_n_0,
      Q => input_data_reg_VALID_FLAG_reg_n_0,
      R => '0'
    );
\input_data_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(0),
      Q => input_data_reg(0),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(10),
      Q => input_data_reg(10),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(11),
      Q => input_data_reg(11),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(12),
      Q => input_data_reg(12),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(13),
      Q => input_data_reg(13),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(14),
      Q => input_data_reg(14),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(15),
      Q => input_data_reg(15),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(16),
      Q => input_data_reg(16),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(17),
      Q => input_data_reg(17),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(18),
      Q => input_data_reg(18),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(19),
      Q => input_data_reg(19),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(1),
      Q => input_data_reg(1),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(20),
      Q => input_data_reg(20),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(21),
      Q => input_data_reg(21),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(22),
      Q => input_data_reg(22),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(23),
      Q => input_data_reg(23),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(24),
      Q => input_data_reg(24),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(25),
      Q => input_data_reg(25),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(26),
      Q => input_data_reg(26),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(27),
      Q => input_data_reg(27),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(28),
      Q => input_data_reg(28),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(29),
      Q => input_data_reg(29),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(2),
      Q => input_data_reg(2),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(30),
      Q => input_data_reg(30),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(31),
      Q => input_data_reg(31),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(3),
      Q => input_data_reg(3),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(4),
      Q => input_data_reg(4),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(5),
      Q => input_data_reg(5),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(6),
      Q => input_data_reg(6),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(7),
      Q => input_data_reg(7),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(8),
      Q => input_data_reg(8),
      R => \input_data_reg[31]_i_1_n_0\
    );
\input_data_reg_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \input_data_reg[31]_i_2_n_0\,
      D => input_data(9),
      Q => input_data_reg(9),
      R => \input_data_reg[31]_i_1_n_0\
    );
rd_en_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000FB510000"
    )
        port map (
      I0 => TLAST_FLAG_reg_n_0,
      I1 => COUNT2,
      I2 => rd_en_reg_i_2_n_0,
      I3 => rd_en_reg_i_3_n_0,
      I4 => rd_en_reg_i_4_n_0,
      I5 => addTLAST_VETO,
      O => rd_en_reg_i_1_n_0
    );
rd_en_reg_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => m00_axis_tready,
      I1 => \^m00_axis_tvalid\,
      O => rd_en_reg_i_2_n_0
    );
rd_en_reg_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => addTLAST_VETO_reg_n_0,
      I1 => addTLAST,
      O => rd_en_reg_i_3_n_0
    );
rd_en_reg_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00F0001000000010"
    )
        port map (
      I0 => almost_full,
      I1 => empty,
      I2 => m00_axis_aresetn,
      I3 => INIT,
      I4 => TLAST_FLAG_reg_n_0,
      I5 => \^rd_en\,
      O => rd_en_reg_i_4_n_0
    );
rd_en_reg_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => INIT,
      I1 => m00_axis_aresetn,
      O => addTLAST_VETO
    );
rd_en_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => rd_en_reg_i_1_n_0,
      Q => \^rd_en\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AXIStream_Converter_v1_0 is
  port (
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    rd_en : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    INIT : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    almost_full : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    input_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    addTLAST : in STD_LOGIC;
    NUM_of_packet : in STD_LOGIC_VECTOR ( 31 downto 0 );
    empty : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AXIStream_Converter_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AXIStream_Converter_v1_0 is
begin
AXIStream_Converter_v1_0_M00_AXIS_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AXIStream_Converter_v1_0_M00_AXIS
     port map (
      INIT => INIT,
      NUM_of_packet(31 downto 0) => NUM_of_packet(31 downto 0),
      addTLAST => addTLAST,
      almost_full => almost_full,
      empty => empty,
      input_data(31 downto 0) => input_data(31 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_aresetn => m00_axis_aresetn,
      m00_axis_tdata(31 downto 0) => m00_axis_tdata(31 downto 0),
      m00_axis_tlast => m00_axis_tlast,
      m00_axis_tready => m00_axis_tready,
      m00_axis_tvalid => m00_axis_tvalid,
      rd_en => rd_en
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    INIT : in STD_LOGIC;
    empty : in STD_LOGIC;
    almost_full : in STD_LOGIC;
    addTLAST : in STD_LOGIC;
    input_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    NUM_of_packet : in STD_LOGIC_VECTOR ( 31 downto 0 );
    rd_en : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_AXIStream_Converter_0_0,AXIStream_Converter_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "AXIStream_Converter_v1_0,Vivado 2017.4";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const1>\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of m00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 M00_AXIS_CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of m00_axis_aclk : signal is "XIL_INTERFACENAME M00_AXIS_CLK, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0";
  attribute X_INTERFACE_INFO of m00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 M00_AXIS_RST RST";
  attribute X_INTERFACE_PARAMETER of m00_axis_aresetn : signal is "XIL_INTERFACENAME M00_AXIS_RST, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of m00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TLAST";
  attribute X_INTERFACE_INFO of m00_axis_tready : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_tready : signal is "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of m00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TVALID";
  attribute X_INTERFACE_INFO of m00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB";
begin
  m00_axis_tstrb(3) <= \<const1>\;
  m00_axis_tstrb(2) <= \<const1>\;
  m00_axis_tstrb(1) <= \<const1>\;
  m00_axis_tstrb(0) <= \<const1>\;
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AXIStream_Converter_v1_0
     port map (
      INIT => INIT,
      NUM_of_packet(31 downto 0) => NUM_of_packet(31 downto 0),
      addTLAST => addTLAST,
      almost_full => almost_full,
      empty => empty,
      input_data(31 downto 0) => input_data(31 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_aresetn => m00_axis_aresetn,
      m00_axis_tdata(31 downto 0) => m00_axis_tdata(31 downto 0),
      m00_axis_tlast => m00_axis_tlast,
      m00_axis_tready => m00_axis_tready,
      m00_axis_tvalid => m00_axis_tvalid,
      rd_en => rd_en
    );
end STRUCTURE;
