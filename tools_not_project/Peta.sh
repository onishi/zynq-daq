#! /bin/sh

BASEDIR=$(cd $(dirname $0);pwd)
CURRENTDIR=$(basename $(cd .;pwd))
#echo ${BASEDIR}
#echo ${CURRENTDIR}

##################################################################
NUM=1
FILENAME="./${CURRENTDIR}_0${NUM}"
while true
do
    if [ ! -e ${FILENAME} ]; then
#echo "${FILENAME} is not exist."
echo "project ${FILENAME} is made by Petalinux Tool"
	break
    fi

    NUM=$((${NUM}+1))

    if [ $NUM -lt 10 ] ; then 
	FILENAME="./${CURRENTDIR}_0${NUM}"
    else
	FILENAME="./${CURRENTDIR}_${NUM}"
    fi
done 
#touch "${FILENAME}"
##################################################################
source ~/Xilinx/Petalinux/settings.sh
petalinux-create --type project --template zynq --name "${FILENAME}"
cd "${FILENAME}"
echo "cd to ${FILENAME}"
petalinux-config --get-hw-description=../${CURRENTDIR}.sdk/design_1_wrapper_hw_platform_0
##################################################################
echo ' ' >>./project-spec/meta-user/recipes-core/images/petalinux-image.bbappend
echo 'IMAGE_INSTALL_append = " iperf3"' >>./project-spec/meta-user/recipes-core/images/petalinux-image.bbappend
echo ' ' >>./project-spec/meta-user/recipes-core/images/petalinux-image.bbappend
##################################################################
petalinux-config
petalinux-config -c kernel
petalinux-config -c rootfs
petalinux-config -c u-boot
petalinux-build
petalinux-build -c kernel
petalinux-package --boot --fsbl ./images/linux/zynq_fsbl.elf --fpga ./images/linux/design_1_wrapper.bit --u-boot


