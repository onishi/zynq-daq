#better?
#echo 16777216 >  /proc/sys/vm/dirty_bytes 
#echo 8388608 >  /proc/sys/vm/dirty_bytes 

#echo 2147483648 >  /proc/sys/vm/dirty_bytes 
#echo 1073741824 >  /proc/sys/vm/dirty_bytes 
#echo 536870912 >  /proc/sys/vm/dirty_bytes 
#echo 268435456 >  /proc/sys/vm/dirty_bytes 
#echo 134217728 >  /proc/sys/vm/dirty_bytes 
#echo 67108864 >  /proc/sys/vm/dirty_bytes 
#echo 33554432 >  /proc/sys/vm/dirty_bytes 
echo 16777216 >  /proc/sys/vm/dirty_bytes 
#echo 8388608 >  /proc/sys/vm/dirty_bytes 
#echo 4194304 >  /proc/sys/vm/dirty_bytes 
#echo 2097152 >  /proc/sys/vm/dirty_bytes 
#echo 1048576 >  /proc/sys/vm/dirty_bytes 
#echo 524288 >  /proc/sys/vm/dirty_bytes 
#echo 262144 >  /proc/sys/vm/dirty_bytes 
#echo 131072 >  /proc/sys/vm/dirty_bytes 
#echo 65536 >  /proc/sys/vm/dirty_bytes 
#echo 32768 >  /proc/sys/vm/dirty_bytes 
#echo 16384 >  /proc/sys/vm/dirty_bytes 

#####Minimum#####
#echo 8192 >  /proc/sys/vm/dirty_bytes 


#echo 20 >  /proc/sys/vm/dirty_ratio 
echo 3000 >  /proc/sys/vm/dirty_expire_centisecs
echo 500 >/proc/sys/vm/dirty_writeback_centisecs


######2GB yhreshold#####
#echo 2147483648 >  /proc/sys/vm/dirty_bytes 

######default kernel parameter######
#echo 0 >  /proc/sys/vm/dirty_bytes 
#echo 20 >  /proc/sys/vm/dirty_ratio 
#echo 3000 >  /proc/sys/vm/dirty_expire_centisecs
#echo 500 >/proc/sys/vm/dirty_writeback_centisecs
####################################
sysctl -a 2>&1 | grep dirty_expire_centisecs
sysctl -a 2>&1 | grep dirty_writeback_centisecs
sysctl -a 2>&1 | grep dirty_bytes
sysctl -a 2>&1 | grep dirty_ratio
