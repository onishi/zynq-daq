// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
// Date        : Wed Apr 17 14:31:29 2019
// Host        : fpga01 running 64-bit unknown
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_SeriPara_0_0_stub.v
// Design      : design_1_SeriPara_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "SeriPara,Vivado 2017.4" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(clk_in, SerialSignal, ParallelSignal, 
  ParallelEnable)
/* synthesis syn_black_box black_box_pad_pin="clk_in,SerialSignal,ParallelSignal[31:0],ParallelEnable" */;
  input clk_in;
  input SerialSignal;
  output [31:0]ParallelSignal;
  output ParallelEnable;
endmodule
