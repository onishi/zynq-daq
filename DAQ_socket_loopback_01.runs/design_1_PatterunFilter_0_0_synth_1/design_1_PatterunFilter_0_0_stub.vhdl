-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
-- Date        : Wed Apr 17 14:31:25 2019
-- Host        : fpga01 running 64-bit unknown
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_PatterunFilter_0_0_stub.vhdl
-- Design      : design_1_PatterunFilter_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    through_filter : in STD_LOGIC;
    filter_pattern : in STD_LOGIC_VECTOR ( 31 downto 0 );
    in_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    in_data_Enable : in STD_LOGIC;
    out_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    out_data_Enable : out STD_LOGIC;
    clk_in : in STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "through_filter,filter_pattern[31:0],in_data[31:0],in_data_Enable,out_data[31:0],out_data_Enable,clk_in";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "PatterunFilter,Vivado 2017.4";
begin
end;
