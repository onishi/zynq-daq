// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
// Date        : Wed Apr 17 14:31:25 2019
// Host        : fpga01 running 64-bit unknown
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_PatterunFilter_0_0_sim_netlist.v
// Design      : design_1_PatterunFilter_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PatterunFilter
   (out_data,
    out_data_Enable,
    in_data,
    clk_in,
    filter_pattern,
    through_filter,
    in_data_Enable);
  output [31:0]out_data;
  output out_data_Enable;
  input [31:0]in_data;
  input clk_in;
  input [31:0]filter_pattern;
  input through_filter;
  input in_data_Enable;

  wire clk_in;
  wire [31:0]filter_pattern;
  wire [31:0]in_data;
  wire in_data_Enable;
  wire [31:0]out_data;
  wire out_data_Enable;
  wire out_data_Enable_reg3_carry__0_i_1_n_0;
  wire out_data_Enable_reg3_carry__0_i_2_n_0;
  wire out_data_Enable_reg3_carry__0_i_3_n_0;
  wire out_data_Enable_reg3_carry__0_i_4_n_0;
  wire out_data_Enable_reg3_carry__0_n_0;
  wire out_data_Enable_reg3_carry__0_n_1;
  wire out_data_Enable_reg3_carry__0_n_2;
  wire out_data_Enable_reg3_carry__0_n_3;
  wire out_data_Enable_reg3_carry__1_i_1_n_0;
  wire out_data_Enable_reg3_carry__1_i_2_n_0;
  wire out_data_Enable_reg3_carry__1_i_3_n_0;
  wire out_data_Enable_reg3_carry__1_n_1;
  wire out_data_Enable_reg3_carry__1_n_2;
  wire out_data_Enable_reg3_carry__1_n_3;
  wire out_data_Enable_reg3_carry_i_1_n_0;
  wire out_data_Enable_reg3_carry_i_2_n_0;
  wire out_data_Enable_reg3_carry_i_3_n_0;
  wire out_data_Enable_reg3_carry_i_4_n_0;
  wire out_data_Enable_reg3_carry_n_0;
  wire out_data_Enable_reg3_carry_n_1;
  wire out_data_Enable_reg3_carry_n_2;
  wire out_data_Enable_reg3_carry_n_3;
  wire out_data_Enable_reg_i_10_n_0;
  wire out_data_Enable_reg_i_1_n_0;
  wire out_data_Enable_reg_i_2_n_0;
  wire out_data_Enable_reg_i_3_n_0;
  wire out_data_Enable_reg_i_4_n_0;
  wire out_data_Enable_reg_i_5_n_0;
  wire out_data_Enable_reg_i_6_n_0;
  wire out_data_Enable_reg_i_7_n_0;
  wire out_data_Enable_reg_i_8_n_0;
  wire out_data_Enable_reg_i_9_n_0;
  wire through_filter;
  wire [3:0]NLW_out_data_Enable_reg3_carry_O_UNCONNECTED;
  wire [3:0]NLW_out_data_Enable_reg3_carry__0_O_UNCONNECTED;
  wire [3:3]NLW_out_data_Enable_reg3_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_out_data_Enable_reg3_carry__1_O_UNCONNECTED;

  CARRY4 out_data_Enable_reg3_carry
       (.CI(1'b0),
        .CO({out_data_Enable_reg3_carry_n_0,out_data_Enable_reg3_carry_n_1,out_data_Enable_reg3_carry_n_2,out_data_Enable_reg3_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_out_data_Enable_reg3_carry_O_UNCONNECTED[3:0]),
        .S({out_data_Enable_reg3_carry_i_1_n_0,out_data_Enable_reg3_carry_i_2_n_0,out_data_Enable_reg3_carry_i_3_n_0,out_data_Enable_reg3_carry_i_4_n_0}));
  CARRY4 out_data_Enable_reg3_carry__0
       (.CI(out_data_Enable_reg3_carry_n_0),
        .CO({out_data_Enable_reg3_carry__0_n_0,out_data_Enable_reg3_carry__0_n_1,out_data_Enable_reg3_carry__0_n_2,out_data_Enable_reg3_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_out_data_Enable_reg3_carry__0_O_UNCONNECTED[3:0]),
        .S({out_data_Enable_reg3_carry__0_i_1_n_0,out_data_Enable_reg3_carry__0_i_2_n_0,out_data_Enable_reg3_carry__0_i_3_n_0,out_data_Enable_reg3_carry__0_i_4_n_0}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    out_data_Enable_reg3_carry__0_i_1
       (.I0(in_data[21]),
        .I1(filter_pattern[21]),
        .I2(filter_pattern[23]),
        .I3(in_data[23]),
        .I4(filter_pattern[22]),
        .I5(in_data[22]),
        .O(out_data_Enable_reg3_carry__0_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    out_data_Enable_reg3_carry__0_i_2
       (.I0(in_data[18]),
        .I1(filter_pattern[18]),
        .I2(filter_pattern[20]),
        .I3(in_data[20]),
        .I4(filter_pattern[19]),
        .I5(in_data[19]),
        .O(out_data_Enable_reg3_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    out_data_Enable_reg3_carry__0_i_3
       (.I0(in_data[15]),
        .I1(filter_pattern[15]),
        .I2(filter_pattern[17]),
        .I3(in_data[17]),
        .I4(filter_pattern[16]),
        .I5(in_data[16]),
        .O(out_data_Enable_reg3_carry__0_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    out_data_Enable_reg3_carry__0_i_4
       (.I0(in_data[12]),
        .I1(filter_pattern[12]),
        .I2(filter_pattern[14]),
        .I3(in_data[14]),
        .I4(filter_pattern[13]),
        .I5(in_data[13]),
        .O(out_data_Enable_reg3_carry__0_i_4_n_0));
  CARRY4 out_data_Enable_reg3_carry__1
       (.CI(out_data_Enable_reg3_carry__0_n_0),
        .CO({NLW_out_data_Enable_reg3_carry__1_CO_UNCONNECTED[3],out_data_Enable_reg3_carry__1_n_1,out_data_Enable_reg3_carry__1_n_2,out_data_Enable_reg3_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_out_data_Enable_reg3_carry__1_O_UNCONNECTED[3:0]),
        .S({1'b0,out_data_Enable_reg3_carry__1_i_1_n_0,out_data_Enable_reg3_carry__1_i_2_n_0,out_data_Enable_reg3_carry__1_i_3_n_0}));
  LUT4 #(
    .INIT(16'h9009)) 
    out_data_Enable_reg3_carry__1_i_1
       (.I0(in_data[30]),
        .I1(filter_pattern[30]),
        .I2(in_data[31]),
        .I3(filter_pattern[31]),
        .O(out_data_Enable_reg3_carry__1_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    out_data_Enable_reg3_carry__1_i_2
       (.I0(in_data[27]),
        .I1(filter_pattern[27]),
        .I2(filter_pattern[29]),
        .I3(in_data[29]),
        .I4(filter_pattern[28]),
        .I5(in_data[28]),
        .O(out_data_Enable_reg3_carry__1_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    out_data_Enable_reg3_carry__1_i_3
       (.I0(in_data[24]),
        .I1(filter_pattern[24]),
        .I2(filter_pattern[26]),
        .I3(in_data[26]),
        .I4(filter_pattern[25]),
        .I5(in_data[25]),
        .O(out_data_Enable_reg3_carry__1_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    out_data_Enable_reg3_carry_i_1
       (.I0(in_data[9]),
        .I1(filter_pattern[9]),
        .I2(filter_pattern[11]),
        .I3(in_data[11]),
        .I4(filter_pattern[10]),
        .I5(in_data[10]),
        .O(out_data_Enable_reg3_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    out_data_Enable_reg3_carry_i_2
       (.I0(in_data[6]),
        .I1(filter_pattern[6]),
        .I2(filter_pattern[8]),
        .I3(in_data[8]),
        .I4(filter_pattern[7]),
        .I5(in_data[7]),
        .O(out_data_Enable_reg3_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    out_data_Enable_reg3_carry_i_3
       (.I0(in_data[3]),
        .I1(filter_pattern[3]),
        .I2(filter_pattern[5]),
        .I3(in_data[5]),
        .I4(filter_pattern[4]),
        .I5(in_data[4]),
        .O(out_data_Enable_reg3_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    out_data_Enable_reg3_carry_i_4
       (.I0(in_data[0]),
        .I1(filter_pattern[0]),
        .I2(filter_pattern[2]),
        .I3(in_data[2]),
        .I4(filter_pattern[1]),
        .I5(in_data[1]),
        .O(out_data_Enable_reg3_carry_i_4_n_0));
  LUT6 #(
    .INIT(64'h0808080808080800)) 
    out_data_Enable_reg_i_1
       (.I0(through_filter),
        .I1(in_data_Enable),
        .I2(out_data_Enable_reg3_carry__1_n_1),
        .I3(out_data_Enable_reg_i_2_n_0),
        .I4(out_data_Enable_reg_i_3_n_0),
        .I5(out_data_Enable_reg_i_4_n_0),
        .O(out_data_Enable_reg_i_1_n_0));
  LUT5 #(
    .INIT(32'h7FFFFFFE)) 
    out_data_Enable_reg_i_10
       (.I0(in_data[19]),
        .I1(in_data[20]),
        .I2(in_data[16]),
        .I3(in_data[17]),
        .I4(in_data[18]),
        .O(out_data_Enable_reg_i_10_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    out_data_Enable_reg_i_2
       (.I0(out_data_Enable_reg_i_5_n_0),
        .I1(out_data_Enable_reg_i_6_n_0),
        .I2(out_data_Enable_reg_i_7_n_0),
        .I3(out_data_Enable_reg_i_8_n_0),
        .O(out_data_Enable_reg_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFFF7FFE)) 
    out_data_Enable_reg_i_3
       (.I0(in_data[31]),
        .I1(in_data[30]),
        .I2(in_data[29]),
        .I3(in_data[28]),
        .I4(out_data_Enable_reg_i_9_n_0),
        .O(out_data_Enable_reg_i_3_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF7FFFFFFE)) 
    out_data_Enable_reg_i_4
       (.I0(in_data[22]),
        .I1(in_data[21]),
        .I2(in_data[20]),
        .I3(in_data[24]),
        .I4(in_data[23]),
        .I5(out_data_Enable_reg_i_10_n_0),
        .O(out_data_Enable_reg_i_4_n_0));
  LUT5 #(
    .INIT(32'h7FFFFFFE)) 
    out_data_Enable_reg_i_5
       (.I0(in_data[11]),
        .I1(in_data[12]),
        .I2(in_data[8]),
        .I3(in_data[9]),
        .I4(in_data[10]),
        .O(out_data_Enable_reg_i_5_n_0));
  LUT5 #(
    .INIT(32'h7FFFFFFE)) 
    out_data_Enable_reg_i_6
       (.I0(in_data[15]),
        .I1(in_data[16]),
        .I2(in_data[12]),
        .I3(in_data[13]),
        .I4(in_data[14]),
        .O(out_data_Enable_reg_i_6_n_0));
  LUT5 #(
    .INIT(32'h7FFFFFFE)) 
    out_data_Enable_reg_i_7
       (.I0(in_data[3]),
        .I1(in_data[4]),
        .I2(in_data[0]),
        .I3(in_data[1]),
        .I4(in_data[2]),
        .O(out_data_Enable_reg_i_7_n_0));
  LUT5 #(
    .INIT(32'h7FFFFFFE)) 
    out_data_Enable_reg_i_8
       (.I0(in_data[7]),
        .I1(in_data[8]),
        .I2(in_data[4]),
        .I3(in_data[5]),
        .I4(in_data[6]),
        .O(out_data_Enable_reg_i_8_n_0));
  LUT5 #(
    .INIT(32'h7FFFFFFE)) 
    out_data_Enable_reg_i_9
       (.I0(in_data[27]),
        .I1(in_data[28]),
        .I2(in_data[24]),
        .I3(in_data[25]),
        .I4(in_data[26]),
        .O(out_data_Enable_reg_i_9_n_0));
  FDRE #(
    .INIT(1'b0)) 
    out_data_Enable_reg_reg
       (.C(clk_in),
        .CE(1'b1),
        .D(out_data_Enable_reg_i_1_n_0),
        .Q(out_data_Enable),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[0] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[0]),
        .Q(out_data[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[10] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[10]),
        .Q(out_data[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[11] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[11]),
        .Q(out_data[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[12] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[12]),
        .Q(out_data[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[13] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[13]),
        .Q(out_data[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[14] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[14]),
        .Q(out_data[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[15] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[15]),
        .Q(out_data[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[16] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[16]),
        .Q(out_data[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[17] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[17]),
        .Q(out_data[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[18] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[18]),
        .Q(out_data[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[19] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[19]),
        .Q(out_data[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[1] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[1]),
        .Q(out_data[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[20] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[20]),
        .Q(out_data[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[21] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[21]),
        .Q(out_data[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[22] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[22]),
        .Q(out_data[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[23] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[23]),
        .Q(out_data[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[24] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[24]),
        .Q(out_data[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[25] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[25]),
        .Q(out_data[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[26] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[26]),
        .Q(out_data[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[27] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[27]),
        .Q(out_data[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[28] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[28]),
        .Q(out_data[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[29] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[29]),
        .Q(out_data[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[2] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[2]),
        .Q(out_data[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[30] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[30]),
        .Q(out_data[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[31] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[31]),
        .Q(out_data[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[3] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[3]),
        .Q(out_data[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[4] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[4]),
        .Q(out_data[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[5] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[5]),
        .Q(out_data[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[6] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[6]),
        .Q(out_data[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[7] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[7]),
        .Q(out_data[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[8] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[8]),
        .Q(out_data[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \out_data_reg_reg[9] 
       (.C(clk_in),
        .CE(1'b1),
        .D(in_data[9]),
        .Q(out_data[9]),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_PatterunFilter_0_0,PatterunFilter,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "PatterunFilter,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (through_filter,
    filter_pattern,
    in_data,
    in_data_Enable,
    out_data,
    out_data_Enable,
    clk_in);
  input through_filter;
  input [31:0]filter_pattern;
  input [31:0]in_data;
  input in_data_Enable;
  output [31:0]out_data;
  output out_data_Enable;
  input clk_in;

  wire clk_in;
  wire [31:0]filter_pattern;
  wire [31:0]in_data;
  wire in_data_Enable;
  wire [31:0]out_data;
  wire out_data_Enable;
  wire through_filter;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PatterunFilter inst
       (.clk_in(clk_in),
        .filter_pattern(filter_pattern),
        .in_data(in_data),
        .in_data_Enable(in_data_Enable),
        .out_data(out_data),
        .out_data_Enable(out_data_Enable),
        .through_filter(through_filter));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
