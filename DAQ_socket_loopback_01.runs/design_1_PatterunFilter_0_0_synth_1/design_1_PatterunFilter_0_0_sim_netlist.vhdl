-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
-- Date        : Wed Apr 17 14:31:25 2019
-- Host        : fpga01 running 64-bit unknown
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_PatterunFilter_0_0_sim_netlist.vhdl
-- Design      : design_1_PatterunFilter_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PatterunFilter is
  port (
    out_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    out_data_Enable : out STD_LOGIC;
    in_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    clk_in : in STD_LOGIC;
    filter_pattern : in STD_LOGIC_VECTOR ( 31 downto 0 );
    through_filter : in STD_LOGIC;
    in_data_Enable : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PatterunFilter;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PatterunFilter is
  signal \out_data_Enable_reg3_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \out_data_Enable_reg3_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \out_data_Enable_reg3_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \out_data_Enable_reg3_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \out_data_Enable_reg3_carry__0_n_0\ : STD_LOGIC;
  signal \out_data_Enable_reg3_carry__0_n_1\ : STD_LOGIC;
  signal \out_data_Enable_reg3_carry__0_n_2\ : STD_LOGIC;
  signal \out_data_Enable_reg3_carry__0_n_3\ : STD_LOGIC;
  signal \out_data_Enable_reg3_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \out_data_Enable_reg3_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \out_data_Enable_reg3_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \out_data_Enable_reg3_carry__1_n_1\ : STD_LOGIC;
  signal \out_data_Enable_reg3_carry__1_n_2\ : STD_LOGIC;
  signal \out_data_Enable_reg3_carry__1_n_3\ : STD_LOGIC;
  signal out_data_Enable_reg3_carry_i_1_n_0 : STD_LOGIC;
  signal out_data_Enable_reg3_carry_i_2_n_0 : STD_LOGIC;
  signal out_data_Enable_reg3_carry_i_3_n_0 : STD_LOGIC;
  signal out_data_Enable_reg3_carry_i_4_n_0 : STD_LOGIC;
  signal out_data_Enable_reg3_carry_n_0 : STD_LOGIC;
  signal out_data_Enable_reg3_carry_n_1 : STD_LOGIC;
  signal out_data_Enable_reg3_carry_n_2 : STD_LOGIC;
  signal out_data_Enable_reg3_carry_n_3 : STD_LOGIC;
  signal out_data_Enable_reg_i_10_n_0 : STD_LOGIC;
  signal out_data_Enable_reg_i_1_n_0 : STD_LOGIC;
  signal out_data_Enable_reg_i_2_n_0 : STD_LOGIC;
  signal out_data_Enable_reg_i_3_n_0 : STD_LOGIC;
  signal out_data_Enable_reg_i_4_n_0 : STD_LOGIC;
  signal out_data_Enable_reg_i_5_n_0 : STD_LOGIC;
  signal out_data_Enable_reg_i_6_n_0 : STD_LOGIC;
  signal out_data_Enable_reg_i_7_n_0 : STD_LOGIC;
  signal out_data_Enable_reg_i_8_n_0 : STD_LOGIC;
  signal out_data_Enable_reg_i_9_n_0 : STD_LOGIC;
  signal NLW_out_data_Enable_reg3_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_out_data_Enable_reg3_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_out_data_Enable_reg3_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_out_data_Enable_reg3_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
out_data_Enable_reg3_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => out_data_Enable_reg3_carry_n_0,
      CO(2) => out_data_Enable_reg3_carry_n_1,
      CO(1) => out_data_Enable_reg3_carry_n_2,
      CO(0) => out_data_Enable_reg3_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_out_data_Enable_reg3_carry_O_UNCONNECTED(3 downto 0),
      S(3) => out_data_Enable_reg3_carry_i_1_n_0,
      S(2) => out_data_Enable_reg3_carry_i_2_n_0,
      S(1) => out_data_Enable_reg3_carry_i_3_n_0,
      S(0) => out_data_Enable_reg3_carry_i_4_n_0
    );
\out_data_Enable_reg3_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => out_data_Enable_reg3_carry_n_0,
      CO(3) => \out_data_Enable_reg3_carry__0_n_0\,
      CO(2) => \out_data_Enable_reg3_carry__0_n_1\,
      CO(1) => \out_data_Enable_reg3_carry__0_n_2\,
      CO(0) => \out_data_Enable_reg3_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_out_data_Enable_reg3_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \out_data_Enable_reg3_carry__0_i_1_n_0\,
      S(2) => \out_data_Enable_reg3_carry__0_i_2_n_0\,
      S(1) => \out_data_Enable_reg3_carry__0_i_3_n_0\,
      S(0) => \out_data_Enable_reg3_carry__0_i_4_n_0\
    );
\out_data_Enable_reg3_carry__0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => in_data(21),
      I1 => filter_pattern(21),
      I2 => filter_pattern(23),
      I3 => in_data(23),
      I4 => filter_pattern(22),
      I5 => in_data(22),
      O => \out_data_Enable_reg3_carry__0_i_1_n_0\
    );
\out_data_Enable_reg3_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => in_data(18),
      I1 => filter_pattern(18),
      I2 => filter_pattern(20),
      I3 => in_data(20),
      I4 => filter_pattern(19),
      I5 => in_data(19),
      O => \out_data_Enable_reg3_carry__0_i_2_n_0\
    );
\out_data_Enable_reg3_carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => in_data(15),
      I1 => filter_pattern(15),
      I2 => filter_pattern(17),
      I3 => in_data(17),
      I4 => filter_pattern(16),
      I5 => in_data(16),
      O => \out_data_Enable_reg3_carry__0_i_3_n_0\
    );
\out_data_Enable_reg3_carry__0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => in_data(12),
      I1 => filter_pattern(12),
      I2 => filter_pattern(14),
      I3 => in_data(14),
      I4 => filter_pattern(13),
      I5 => in_data(13),
      O => \out_data_Enable_reg3_carry__0_i_4_n_0\
    );
\out_data_Enable_reg3_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \out_data_Enable_reg3_carry__0_n_0\,
      CO(3) => \NLW_out_data_Enable_reg3_carry__1_CO_UNCONNECTED\(3),
      CO(2) => \out_data_Enable_reg3_carry__1_n_1\,
      CO(1) => \out_data_Enable_reg3_carry__1_n_2\,
      CO(0) => \out_data_Enable_reg3_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_out_data_Enable_reg3_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => \out_data_Enable_reg3_carry__1_i_1_n_0\,
      S(1) => \out_data_Enable_reg3_carry__1_i_2_n_0\,
      S(0) => \out_data_Enable_reg3_carry__1_i_3_n_0\
    );
\out_data_Enable_reg3_carry__1_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => in_data(30),
      I1 => filter_pattern(30),
      I2 => in_data(31),
      I3 => filter_pattern(31),
      O => \out_data_Enable_reg3_carry__1_i_1_n_0\
    );
\out_data_Enable_reg3_carry__1_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => in_data(27),
      I1 => filter_pattern(27),
      I2 => filter_pattern(29),
      I3 => in_data(29),
      I4 => filter_pattern(28),
      I5 => in_data(28),
      O => \out_data_Enable_reg3_carry__1_i_2_n_0\
    );
\out_data_Enable_reg3_carry__1_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => in_data(24),
      I1 => filter_pattern(24),
      I2 => filter_pattern(26),
      I3 => in_data(26),
      I4 => filter_pattern(25),
      I5 => in_data(25),
      O => \out_data_Enable_reg3_carry__1_i_3_n_0\
    );
out_data_Enable_reg3_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => in_data(9),
      I1 => filter_pattern(9),
      I2 => filter_pattern(11),
      I3 => in_data(11),
      I4 => filter_pattern(10),
      I5 => in_data(10),
      O => out_data_Enable_reg3_carry_i_1_n_0
    );
out_data_Enable_reg3_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => in_data(6),
      I1 => filter_pattern(6),
      I2 => filter_pattern(8),
      I3 => in_data(8),
      I4 => filter_pattern(7),
      I5 => in_data(7),
      O => out_data_Enable_reg3_carry_i_2_n_0
    );
out_data_Enable_reg3_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => in_data(3),
      I1 => filter_pattern(3),
      I2 => filter_pattern(5),
      I3 => in_data(5),
      I4 => filter_pattern(4),
      I5 => in_data(4),
      O => out_data_Enable_reg3_carry_i_3_n_0
    );
out_data_Enable_reg3_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => in_data(0),
      I1 => filter_pattern(0),
      I2 => filter_pattern(2),
      I3 => in_data(2),
      I4 => filter_pattern(1),
      I5 => in_data(1),
      O => out_data_Enable_reg3_carry_i_4_n_0
    );
out_data_Enable_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0808080808080800"
    )
        port map (
      I0 => through_filter,
      I1 => in_data_Enable,
      I2 => \out_data_Enable_reg3_carry__1_n_1\,
      I3 => out_data_Enable_reg_i_2_n_0,
      I4 => out_data_Enable_reg_i_3_n_0,
      I5 => out_data_Enable_reg_i_4_n_0,
      O => out_data_Enable_reg_i_1_n_0
    );
out_data_Enable_reg_i_10: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFE"
    )
        port map (
      I0 => in_data(19),
      I1 => in_data(20),
      I2 => in_data(16),
      I3 => in_data(17),
      I4 => in_data(18),
      O => out_data_Enable_reg_i_10_n_0
    );
out_data_Enable_reg_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => out_data_Enable_reg_i_5_n_0,
      I1 => out_data_Enable_reg_i_6_n_0,
      I2 => out_data_Enable_reg_i_7_n_0,
      I3 => out_data_Enable_reg_i_8_n_0,
      O => out_data_Enable_reg_i_2_n_0
    );
out_data_Enable_reg_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFE"
    )
        port map (
      I0 => in_data(31),
      I1 => in_data(30),
      I2 => in_data(29),
      I3 => in_data(28),
      I4 => out_data_Enable_reg_i_9_n_0,
      O => out_data_Enable_reg_i_3_n_0
    );
out_data_Enable_reg_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF7FFFFFFE"
    )
        port map (
      I0 => in_data(22),
      I1 => in_data(21),
      I2 => in_data(20),
      I3 => in_data(24),
      I4 => in_data(23),
      I5 => out_data_Enable_reg_i_10_n_0,
      O => out_data_Enable_reg_i_4_n_0
    );
out_data_Enable_reg_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFE"
    )
        port map (
      I0 => in_data(11),
      I1 => in_data(12),
      I2 => in_data(8),
      I3 => in_data(9),
      I4 => in_data(10),
      O => out_data_Enable_reg_i_5_n_0
    );
out_data_Enable_reg_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFE"
    )
        port map (
      I0 => in_data(15),
      I1 => in_data(16),
      I2 => in_data(12),
      I3 => in_data(13),
      I4 => in_data(14),
      O => out_data_Enable_reg_i_6_n_0
    );
out_data_Enable_reg_i_7: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFE"
    )
        port map (
      I0 => in_data(3),
      I1 => in_data(4),
      I2 => in_data(0),
      I3 => in_data(1),
      I4 => in_data(2),
      O => out_data_Enable_reg_i_7_n_0
    );
out_data_Enable_reg_i_8: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFE"
    )
        port map (
      I0 => in_data(7),
      I1 => in_data(8),
      I2 => in_data(4),
      I3 => in_data(5),
      I4 => in_data(6),
      O => out_data_Enable_reg_i_8_n_0
    );
out_data_Enable_reg_i_9: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFE"
    )
        port map (
      I0 => in_data(27),
      I1 => in_data(28),
      I2 => in_data(24),
      I3 => in_data(25),
      I4 => in_data(26),
      O => out_data_Enable_reg_i_9_n_0
    );
out_data_Enable_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => out_data_Enable_reg_i_1_n_0,
      Q => out_data_Enable,
      R => '0'
    );
\out_data_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(0),
      Q => out_data(0),
      R => '0'
    );
\out_data_reg_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(10),
      Q => out_data(10),
      R => '0'
    );
\out_data_reg_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(11),
      Q => out_data(11),
      R => '0'
    );
\out_data_reg_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(12),
      Q => out_data(12),
      R => '0'
    );
\out_data_reg_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(13),
      Q => out_data(13),
      R => '0'
    );
\out_data_reg_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(14),
      Q => out_data(14),
      R => '0'
    );
\out_data_reg_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(15),
      Q => out_data(15),
      R => '0'
    );
\out_data_reg_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(16),
      Q => out_data(16),
      R => '0'
    );
\out_data_reg_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(17),
      Q => out_data(17),
      R => '0'
    );
\out_data_reg_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(18),
      Q => out_data(18),
      R => '0'
    );
\out_data_reg_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(19),
      Q => out_data(19),
      R => '0'
    );
\out_data_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(1),
      Q => out_data(1),
      R => '0'
    );
\out_data_reg_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(20),
      Q => out_data(20),
      R => '0'
    );
\out_data_reg_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(21),
      Q => out_data(21),
      R => '0'
    );
\out_data_reg_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(22),
      Q => out_data(22),
      R => '0'
    );
\out_data_reg_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(23),
      Q => out_data(23),
      R => '0'
    );
\out_data_reg_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(24),
      Q => out_data(24),
      R => '0'
    );
\out_data_reg_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(25),
      Q => out_data(25),
      R => '0'
    );
\out_data_reg_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(26),
      Q => out_data(26),
      R => '0'
    );
\out_data_reg_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(27),
      Q => out_data(27),
      R => '0'
    );
\out_data_reg_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(28),
      Q => out_data(28),
      R => '0'
    );
\out_data_reg_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(29),
      Q => out_data(29),
      R => '0'
    );
\out_data_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(2),
      Q => out_data(2),
      R => '0'
    );
\out_data_reg_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(30),
      Q => out_data(30),
      R => '0'
    );
\out_data_reg_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(31),
      Q => out_data(31),
      R => '0'
    );
\out_data_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(3),
      Q => out_data(3),
      R => '0'
    );
\out_data_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(4),
      Q => out_data(4),
      R => '0'
    );
\out_data_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(5),
      Q => out_data(5),
      R => '0'
    );
\out_data_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(6),
      Q => out_data(6),
      R => '0'
    );
\out_data_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(7),
      Q => out_data(7),
      R => '0'
    );
\out_data_reg_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(8),
      Q => out_data(8),
      R => '0'
    );
\out_data_reg_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => in_data(9),
      Q => out_data(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    through_filter : in STD_LOGIC;
    filter_pattern : in STD_LOGIC_VECTOR ( 31 downto 0 );
    in_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    in_data_Enable : in STD_LOGIC;
    out_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    out_data_Enable : out STD_LOGIC;
    clk_in : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_PatterunFilter_0_0,PatterunFilter,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "PatterunFilter,Vivado 2017.4";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PatterunFilter
     port map (
      clk_in => clk_in,
      filter_pattern(31 downto 0) => filter_pattern(31 downto 0),
      in_data(31 downto 0) => in_data(31 downto 0),
      in_data_Enable => in_data_Enable,
      out_data(31 downto 0) => out_data(31 downto 0),
      out_data_Enable => out_data_Enable,
      through_filter => through_filter
    );
end STRUCTURE;
