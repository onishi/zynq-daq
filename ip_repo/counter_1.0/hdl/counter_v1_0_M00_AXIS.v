
`timescale 1 ns / 1 ps

	module counter_v1_0_M00_AXIS #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line

		// Width of S_AXIS address bus. The slave accepts the read and write addresses of width C_M_AXIS_TDATA_WIDTH.
		parameter integer C_M_AXIS_TDATA_WIDTH	= 32,
		// Start count is the number of clock cycles the master will wait before initiating/issuing any transaction.
		parameter integer C_M_START_COUNT	= 32
	)
	(
		// Users to add ports here
        input wire [31:0] INIT,
        input wire [31:0] UPPER,
        input wire [31:0] ITERATION,
        input wire [31:0] INTERVAL,
        output wire [7:0] LED,
        output wire [C_M_AXIS_TDATA_WIDTH-1:0] COUNT_MONITOR,
        output wire [31:0] ITERATION_MONITOR,
        output wire [31:0] INTERVAL_MONITOR,
        output wire [31:0] VALID_FLAG_MONITOR,
        
		// User ports ends
		// Do not modify the ports beyond this line

		// Global ports
		input wire  M_AXIS_ACLK,
		// 
		input wire  M_AXIS_ARESETN,
		// Master Stream Ports. TVALID indicates that the master is driving a valid transfer, A transfer takes place when both TVALID and TREADY are asserted. 
		output wire  M_AXIS_TVALID,
		// TDATA is the primary payload that is used to provide the data that is passing across the interface from the master.
		output wire [C_M_AXIS_TDATA_WIDTH-1 : 0] M_AXIS_TDATA,
		// TSTRB is the byte qualifier that indicates whether the content of the associated byte of TDATA is processed as a data byte or a position byte.
		output wire [(C_M_AXIS_TDATA_WIDTH/8)-1 : 0] M_AXIS_TSTRB,
		// TLAST indicates the boundary of a packet.
		output wire  M_AXIS_TLAST,
		// TREADY indicates that the slave can accept a transfer in the current cycle.
		input wire  M_AXIS_TREADY
	);
	// Total number of output data                                                 
	localparam NUMBER_OF_OUTPUT_WORDS = 8;                                               
	                                                                                     
	// function called clogb2 that returns an integer which has the                      
	// value of the ceiling of the log base 2.                                           
	function integer clogb2 (input integer bit_depth);                                   
	  begin                                                                              
	    for(clogb2=0; bit_depth>0; clogb2=clogb2+1)                                      
	      bit_depth = bit_depth >> 1;                                                    
	  end                                                                                
	endfunction                                                                          
	                                                                                     
	// WAIT_COUNT_BITS is the width of the wait counter.                                 
	localparam integer WAIT_COUNT_BITS = clogb2(C_M_START_COUNT-1);                      
	                                                                                     
	// bit_num gives the minimum number of bits needed to address 'depth' size of FIFO.  
	localparam bit_num  = clogb2(NUMBER_OF_OUTPUT_WORDS);                                
	                                                                                     
	// Define the states of state machine                                                
	// The control state machine oversees the writing of input streaming data to the FIFO,
	// and outputs the streaming data from the FIFO                                      
	parameter [1:0] IDLE = 2'b00,        // This is the initial/idle state               
	                                                                                     
	                INIT_COUNTER  = 2'b01, // This state initializes the counter, once   
	                                // the counter reaches C_M_START_COUNT count,        
	                                // the state machine changes state to SEND_STREAM     
	                SEND_STREAM   = 2'b10; // In this state the                          
	                                     // stream data is output through M_AXIS_TDATA   
	// State variable                                                                    
	reg [1:0] mst_exec_state;                                                            
	// Example design FIFO read pointer                                                  
	reg [bit_num-1:0] read_pointer;                                                      

	// AXI Stream internal signals
	//wait counter. The master waits for the user defined number of clock cycles before initiating a transfer.
	reg [WAIT_COUNT_BITS-1 : 0] 	count;
	//streaming data valid
	wire  	axis_tvalid;
	//streaming data valid delayed by one clock cycle
	reg  	axis_tvalid_delay;
	//Last of the streaming data 
	wire  	axis_tlast;
	//Last of the streaming data delayed by one clock cycle
	reg  	axis_tlast_delay;
	//FIFO implementation signals
	reg [C_M_AXIS_TDATA_WIDTH-1 : 0] 	stream_data_out;
	wire  	tx_en;
	//The master has issued all the streaming data stored in FIFO
	reg  	tx_done;


	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////
/*
	// I/O Connections assignments

	assign M_AXIS_TVALID	= axis_tvalid_delay;
	assign M_AXIS_TDATA	= stream_data_out;
	assign M_AXIS_TLAST	= axis_tlast_delay;
	assign M_AXIS_TSTRB	= {(C_M_AXIS_TDATA_WIDTH/8){1'b1}};
*/
	//////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
	// Control state machine implementation                             
	always @(posedge M_AXIS_ACLK)                                             
	begin                                                                     
	  if (!M_AXIS_ARESETN)                                                    
	  // Synchronous reset (active low)                                       
	    begin                                                                 
	      mst_exec_state <= IDLE;                                             
	      count    <= 0;                                                      
	    end                                                                   
	  else                                                                    
	    case (mst_exec_state)                                                 
	      IDLE:                                                               
	        // The slave starts accepting tdata when                          
	        // there tvalid is asserted to mark the                           
	        // presence of valid streaming data                               
	        //if ( count == 0 )                                                 
	        //  begin                                                           
	            mst_exec_state  <= INIT_COUNTER;                              
	        //  end                                                             
	        //else                                                              
	        //  begin                                                           
	        //    mst_exec_state  <= IDLE;                                      
	        //  end                                                             
	                                                                          
	      INIT_COUNTER:                                                       
	        // The slave starts accepting tdata when                          
	        // there tvalid is asserted to mark the                           
	        // presence of valid streaming data                               
	        if ( count == C_M_START_COUNT - 1 )                               
	          begin                                                           
	            mst_exec_state  <= SEND_STREAM;                               
	          end                                                             
	        else                                                              
	          begin                                                           
	            count <= count + 1;                                           
	            mst_exec_state  <= INIT_COUNTER;                              
	          end                                                             
	                                                                          
	      SEND_STREAM:                                                        
	        // The example design streaming master functionality starts       
	        // when the master drives output tdata from the FIFO and the slave
	        // has finished storing the S_AXIS_TDATA                          
	        if (tx_done)                                                      
	          begin                                                           
	            mst_exec_state <= IDLE;                                       
	          end                                                             
	        else                                                              
	          begin                                                           
	            mst_exec_state <= SEND_STREAM;                                
	          end                                                             
	    endcase                                                               
	end                                                                       


	//tvalid generation
	//axis_tvalid is asserted when the control state machine's state is SEND_STREAM and
	//number of output streaming data is less than the NUMBER_OF_OUTPUT_WORDS.
	assign axis_tvalid = ((mst_exec_state == SEND_STREAM) && (read_pointer < NUMBER_OF_OUTPUT_WORDS));
	                                                                                               
	// AXI tlast generation                                                                        
	// axis_tlast is asserted number of output streaming data is NUMBER_OF_OUTPUT_WORDS-1          
	// (0 to NUMBER_OF_OUTPUT_WORDS-1)                                                             
	assign axis_tlast = (read_pointer == NUMBER_OF_OUTPUT_WORDS-1);                                
	                                                                                               
	                                                                                               
	// Delay the axis_tvalid and axis_tlast signal by one clock cycle                              
	// to match the latency of M_AXIS_TDATA                                                        
	always @(posedge M_AXIS_ACLK)                                                                  
	begin                                                                                          
	  if (!M_AXIS_ARESETN)                                                                         
	    begin                                                                                      
	      axis_tvalid_delay <= 1'b0;                                                               
	      axis_tlast_delay <= 1'b0;                                                                
	    end                                                                                        
	  else                                                                                         
	    begin                                                                                      
	      axis_tvalid_delay <= axis_tvalid;                                                        
	      axis_tlast_delay <= axis_tlast;                                                          
	    end                                                                                        
	end                                                                                            


	//read_pointer pointer

	always@(posedge M_AXIS_ACLK)                                               
	begin                                                                            
	  if(!M_AXIS_ARESETN)                                                            
	    begin                                                                        
	      read_pointer <= 0;                                                         
	      tx_done <= 1'b0;                                                           
	    end                                                                          
	  else                                                                           
	    if (read_pointer <= NUMBER_OF_OUTPUT_WORDS-1)                                
	      begin                                                                      
	        if (tx_en)                                                               
	          // read pointer is incremented after every read from the FIFO          
	          // when FIFO read signal is enabled.                                   
	          begin                                                                  
	            read_pointer <= read_pointer + 1;                                    
	            tx_done <= 1'b0;                                                     
	          end                                                                    
	      end                                                                        
	    else if (read_pointer == NUMBER_OF_OUTPUT_WORDS)                             
	      begin                                                                      
	        // tx_done is asserted when NUMBER_OF_OUTPUT_WORDS numbers of streaming data
	        // has been out.                                                         
	        tx_done <= 1'b1;                                                         
	      end                                                                        
	end                                                                              


	//FIFO read enable generation 

	assign tx_en = M_AXIS_TREADY && axis_tvalid;   
	                                                     
	    // Streaming output data is read from FIFO       
	    always @( posedge M_AXIS_ACLK )                  
	    begin                                            
	      if(!M_AXIS_ARESETN)                            
	        begin                                        
	          stream_data_out <= 1;                      
	        end                                          
	      else if (tx_en)// && M_AXIS_TSTRB[byte_index]  
	        begin                                        
	          stream_data_out <= read_pointer + 32'b1;   
	        end                                          
	    end                                              

	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////
	// Add user logic here
     reg [C_M_AXIS_TDATA_WIDTH-1 : 0] M_AXIS_TDATA_reg;
     assign M_AXIS_TSTRB = 4'b1111;
     reg M_AXIS_TLAST_reg;
     reg M_AXIS_TVALID_reg;
     reg [C_M_AXIS_TDATA_WIDTH-1:0] COUNT;
     reg [31:0] ITERATION_COUNT;
     reg [31:0] RATE_COUNT;

     reg LATCH_TVALID;
     
     reg INIT_FLAG_0;
     reg INIT_FLAG_1;
     reg INIT_FLAG_2;
     reg INIT_FLAG_3;
    
     reg VALID_FLAG;
     
 //    reg CLOCK;
                   
    initial begin
         M_AXIS_TDATA_reg<=32'b0; 
         M_AXIS_TLAST_reg<=1'b0;
         M_AXIS_TVALID_reg <=1'b0;
         COUNT <= {C_M_AXIS_TDATA_WIDTH-1{1'b0}};    
         ITERATION_COUNT <= 32'b0; 
         RATE_COUNT <= 32'b0;          
         
         LATCH_TVALID<=1'b0;     
         
         INIT_FLAG_0<=1'b1;
         INIT_FLAG_1<=1'b1;
         INIT_FLAG_2<=1'b1;
         INIT_FLAG_3<=1'b1;

         VALID_FLAG<=1'b0;
         
 //        CLOCK<=1'b0;

     end
   
     
/*
always @(posedge M_AXIS_ACLK)begin
    M_AXIS_TDATA_reg <= {C_M_AXIS_TDATA_WIDTH-1{1'b0}};
    M_AXIS_TLAST_reg <= 1'b0;
    M_AXIS_TVALID_reg <=1'b0; 
    COUNT <= {C_M_AXIS_TDATA_WIDTH-1{1'b0}};
end          
 */


    always @(posedge M_AXIS_ACLK)begin 
       
    if(INIT[0]==1'b1)begin
        if(INIT_FLAG_0==1'b1)begin
            M_AXIS_TDATA_reg<=32'b0; 
            M_AXIS_TLAST_reg<=1'b0;
            M_AXIS_TVALID_reg <=1'b0;
            COUNT <= {C_M_AXIS_TDATA_WIDTH-1{1'b0}};
            ITERATION_COUNT <= 32'b0; 
            RATE_COUNT <= 32'b0;   
            INIT_FLAG_0<=1'b0;
 //           CLOCK<=1'b0;
        end
    end
    if(INIT[0]==1'b0)begin
        INIT_FLAG_0<=1'b1;
    end
    
    if(INIT[1]==1'b1)begin
        if(INIT_FLAG_1==1'b1)begin
            //M_AXIS_TVALID_reg <=1'b0;
            VALID_FLAG <=1'b0;
            INIT_FLAG_1<=1'b0;
        end
    end
    if(INIT[1]==1'b0)begin
        INIT_FLAG_1<=1'b1;
    end
    
    if(INIT[2]==1'b1)begin
        if(INIT_FLAG_2==1'b1)begin
            //M_AXIS_TVALID_reg <=1'b1;
            VALID_FLAG <=1'b1;
            INIT_FLAG_2<=1'b0;
        end
    end
    if(INIT[2]==1'b0)begin
        INIT_FLAG_2<=1'b1;
    end
   /*  
        if(INIT[0]==1'b1)begin
            M_AXIS_TDATA_reg<=32'b0; 
            M_AXIS_TLAST_reg<=1'b0;
            M_AXIS_TVALID_reg <=1'b0;
            COUNT <= {C_M_AXIS_TDATA_WIDTH-1{1'b0}};
            //M_AXIS_TVALID_reg <=1'b1;
        end
        
        if(INIT[1]==1'b1)begin
            M_AXIS_TVALID_reg <=1'b0;
        end
        
        if(INIT[2]==1'b1)begin
            M_AXIS_TVALID_reg <=1'b1;
        end
        */
        /*
        if(INIT[3]==1'b1)begin
            LATCH_TVALID<=1'b1;
        end
        */
    LATCH_TVALID<=INIT[3];
    
/*
    if(RATE_COUNT!=INTERVAL)begin
        RATE_COUNT <= RATE_COUNT+32'b1;
    end
    if(RATE_COUNT==INTERVAL)begin
        RATE_COUNT <= 32'b0;
        CLOCK<=~CLOCK;
    end

    if(VALID_FLAG)begin
        M_AXIS_TVALID_reg <=CLOCK;
    end  
    if(!VALID_FLAG)begin     
        M_AXIS_TVALID_reg <=1'b0;
        RATE_COUNT <= 32'b0;
        CLOCK<=1'b0;
    end 
*/

    if(VALID_FLAG)begin
    
        if(RATE_COUNT!=INTERVAL)begin
            RATE_COUNT <= RATE_COUNT+1'b1;
            M_AXIS_TVALID_reg <=1'b0;
        end
        if(RATE_COUNT==INTERVAL)begin
            M_AXIS_TVALID_reg <=1'b1;
            RATE_COUNT <= 32'b0;
        end
    end  
    if(!VALID_FLAG)begin     
        M_AXIS_TVALID_reg <=1'b0;
        RATE_COUNT <= 32'b0;
    end    

 //    M_AXIS_TVALID_reg <=1'b0;
         if(!M_AXIS_ARESETN)begin
             M_AXIS_TDATA_reg <= {C_M_AXIS_TDATA_WIDTH-1{1'b0}};
             M_AXIS_TLAST_reg <= 1'b0;
             M_AXIS_TVALID_reg <= 1'b0;
             COUNT <= {C_M_AXIS_TDATA_WIDTH-1{1'b0}};
             ITERATION_COUNT <= 32'b0; 
             RATE_COUNT <= 32'b0;   
             INIT_FLAG_0<=1'b0;
  //           CLOCK<=1'b0;
         end
         else begin

            //M_AXIS_TVALID_reg <=1'b0;
            
            //if(M_AXIS_TREADY)begin
            if(M_AXIS_TREADY&M_AXIS_TVALID)begin

                //M_AXIS_TVALID_reg <=1'b1;
                M_AXIS_TDATA_reg<=M_AXIS_TDATA_reg+1'b1;
                
 ///////////////////////////////dummy data/////////////////////////////////////
   //              M_AXIS_TDATA_reg<=32'b10101010101010101010101010101010;
 //               if(M_AXIS_TDATA_reg==32'd300002)begin
 //                   M_AXIS_TDATA_reg<=32'd12341234;     
 //               end
 //               if(M_AXIS_TDATA_reg==32'd12341234)begin
 //                   M_AXIS_TDATA_reg<=32'd300004;     
 //               end
 //               if(M_AXIS_TDATA_reg==32'd600023)begin
 //                   M_AXIS_TDATA_reg<=32'd10101010;     
 //               end
 //               if(M_AXIS_TDATA_reg==32'd10101010)begin
 //                   M_AXIS_TDATA_reg<=32'd600025;     
 //              end
///////////////////////////////////////////////////////////////////////                
                
                 
                //M_AXIS_TDATA_reg<='hff&(M_AXIS_TDATA_reg+1'b1);   ///cut off            
                //M_AXIS_TLAST_reg <= M_AXIS_TDATA_reg =='d9;   ///for Debug
                 
                COUNT<=COUNT+1'b1;
                 
                if(COUNT==(UPPER-32'h2))begin
                    //if(RATE_COUNT==INTERVAL)begin
                        M_AXIS_TLAST_reg<=1'b1;
                        //COUNT<={C_M_AXIS_TDATA_WIDTH-1{1'b0}};
                        ITERATION_COUNT <= ITERATION_COUNT+1'b1;
                    //end
                end
                
                 
                //else begin 
                //    M_AXIS_TLAST_reg<=1'b0;
                //end
            end
/*
            if(!M_AXIS_TREADY)begin
                 M_AXIS_TLAST_reg<=1'b0;
            end
*/
            //if(M_AXIS_TLAST_reg==1'b1)begin
            if(M_AXIS_TLAST_reg&M_AXIS_TVALID)begin
                M_AXIS_TLAST_reg<=1'b0;
                COUNT<={C_M_AXIS_TDATA_WIDTH-1{1'b0}};
                if(!LATCH_TVALID)begin              
                    M_AXIS_TVALID_reg<=1'b0;        
                end   
                //if(ITERATION_COUNT==(ITERATION-32'h1))begin
                if(ITERATION_COUNT==ITERATION)begin
                    M_AXIS_TDATA_reg<=32'b0; 
                    M_AXIS_TLAST_reg<=1'b0;
                    M_AXIS_TVALID_reg <=1'b0;
                    VALID_FLAG=1'b0;
                    COUNT <= {C_M_AXIS_TDATA_WIDTH-1{1'b0}};
                    ITERATION_COUNT <= 32'b0;
                end                                      
            end
         /*
            if(ITERATION_COUNT==ITERATION)begin
                M_AXIS_TDATA_reg<=32'b0; 
                M_AXIS_TLAST_reg<=1'b0;
                M_AXIS_TVALID_reg <=1'b0;
                COUNT <= {C_M_AXIS_TDATA_WIDTH-1{1'b0}};
                ITERATION_COUNT <= 1'b0;
            end
         */
         end
     end

      
     assign M_AXIS_TDATA = M_AXIS_TDATA_reg;
     assign M_AXIS_TLAST = M_AXIS_TLAST_reg;
     assign M_AXIS_TVALID = M_AXIS_TVALID_reg;
    
 
    assign LED = M_AXIS_TDATA_reg;
    assign COUNT_MONITOR = COUNT;
    assign ITERATION_MONITOR = ITERATION_COUNT;
    assign INTERVAL_MONITOR = RATE_COUNT;
    assign VALID_FLAG_MONITOR = VALID_FLAG;
	// User logic ends
	//////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    
	endmodule
