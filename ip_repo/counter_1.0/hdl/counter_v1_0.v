
`timescale 1 ns / 1 ps

	module counter_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Master Bus Interface M00_AXIS
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M00_AXIS_START_COUNT	= 32
	)
	(
		// Users to add ports here
        input wire [31:0] INIT,
        input wire [31:0] UPPER,
        input wire [31:0] ITERATION,
        input wire [31:0] INTERVAL,
        output wire [7:0] LED,
        output wire [C_M00_AXIS_TDATA_WIDTH-1:0] COUNT_MONITOR,
        output wire [31:0] ITERATION_MONITOR,
        output wire [31:0] INTERVAL_MONITOR,
        output wire [31:0] VALID_FLAG_MONITOR,
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Master Bus Interface M00_AXIS
		input wire  m00_axis_aclk,
		input wire  m00_axis_aresetn,
		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready
	);
// Instantiation of Axi Bus Interface M00_AXIS
	counter_v1_0_M00_AXIS # ( 
		.C_M_AXIS_TDATA_WIDTH(C_M00_AXIS_TDATA_WIDTH),
		.C_M_START_COUNT(C_M00_AXIS_START_COUNT)
	) counter_v1_0_M00_AXIS_inst (
		
        .INIT(INIT),
        .UPPER(UPPER),
        .ITERATION(ITERATION),
        .INTERVAL(INTERVAL),
        .LED(LED),
        .COUNT_MONITOR(COUNT_MONITOR),
        .ITERATION_MONITOR(ITERATION_MONITOR),
	    .INTERVAL_MONITOR(INTERVAL_MONITOR),
	    .VALID_FLAG_MONITOR(VALID_FLAG_MONITOR),
	   
		.M_AXIS_ACLK(m00_axis_aclk),
		.M_AXIS_ARESETN(m00_axis_aresetn),
		.M_AXIS_TVALID(m00_axis_tvalid),
		.M_AXIS_TDATA(m00_axis_tdata),
		.M_AXIS_TSTRB(m00_axis_tstrb),
		.M_AXIS_TLAST(m00_axis_tlast),
		.M_AXIS_TREADY(m00_axis_tready)
	);

	// Add user logic here

	// User logic ends

	endmodule
