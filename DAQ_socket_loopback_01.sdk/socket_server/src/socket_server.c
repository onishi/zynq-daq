
/*
 *  TCP server
 */

#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdbool.h>
#include <sys/time.h>
#include <signal.h>
#include <math.h>



/* number of client request */
#define MAXPENDING 5
//#define BUF_ZYNQDAQ 104857600
//#define BUF_ZYNQDAQ 32768
#define BUF_ZYNQDAQ 131072


volatile sig_atomic_t loop_flag = 1;

void handler(int signum)
{
	loop_flag = 0;
}


/*
 *  servrt information
 */
struct server_info {
  unsigned short sv_port;     /* port Nu. on server */

  int sd;                     /* socket descriptor */
  struct sockaddr_in sv_addr; /* structure of address on server */
};
typedef struct server_info sv_info_t;

/*!
 * @brief      return back the message to client
 * @param[in]  clnt_sd : socket descriptor of client
 * @param[out] buff    : recieved message
 * @param[out] errmsg  : container of error message
 * @return     return 0 if succeed, otherwise return -1.
 */
static int
tcp_echo_process(int clnt_sd, char *buff, char *errmsg)
{
//  int rc = 0;
  int recv_msglen = 0;

  /* recieve a message from client */
//  recv_msglen = recv(clnt_sd, buff, BUF_ZYNQDAQ, 0);
  recv_msglen = recv(clnt_sd, buff, BUF_ZYNQDAQ, MSG_WAITALL);
  if(recv_msglen < 0){
    sprintf(errmsg, "(line:%d) %s", __LINE__, strerror(errno));
    return(-1);
  }
/*
  while (recv_msglen > 0){
    rc = send(clnt_sd, buff, recv_msglen, 0);
    if(rc != recv_msglen){
      sprintf(errmsg, "(line:%d) %s", __LINE__, strerror(errno));
      return(-1);
    }
*/

    /* confirm which additional received data exist or not */
  /*
    recv_msglen = recv(clnt_sd, buff, BUF_ZYNQDAQ, 0);
    if(recv_msglen < 0){
      sprintf(errmsg, "(line:%d) %s", __LINE__, strerror(errno));
      return(-1);
    }
  }
*/
 // fprintf(stdout, "%d byte\n",recv_msglen);               /////////////////////////DEBUG
  return(recv_msglen);
}

/*!
 * @brief      receive a TCP message
 * @param[in]  info   : connection information of client
 * @param[out] errmsg : container of error message
 * @return     return 0 if succeed, otherwise return -1.
 */
static int
tcp_echo_server(sv_info_t *info, char *errmsg,FILE *fd,unsigned long total_accept)
{
  unsigned long result=0;

  int rc = 0;
  int exit_flag = 1;
  char buff[BUF_ZYNQDAQ];
  struct sockaddr_in cl_addr;
  int clnt_sd = 0;
  int clnt_len = sizeof(cl_addr);

  unsigned long loopNo = 0;
  long totaltime=0;
 // long recievetime=0;
  long monitortime=0;
  unsigned long lap_accept=0;
  struct timeval Probe1,Probe2;
  time_t Probe_diff;


	clnt_sd = accept(info->sd, (struct sockaddr *)&cl_addr, &clnt_len);

	gettimeofday(&Probe1,NULL);
 // while(exit_flag){

	while(loop_flag){

  		loopNo++;
     /* accept a connection from client */
 //		clnt_sd = accept(info->sd, (struct sockaddr *)&cl_addr, &clnt_len);
  		if(clnt_sd < 0){
  			sprintf(errmsg, "(line:%d) %s", __LINE__, strerror(errno));
  			return(-1);
  		}

    /* work as echo server */
  		rc = tcp_echo_process(clnt_sd, buff, errmsg);
  		if(rc == -1) return(-1);

 // 		if(rc!=0){
//		if(rc!=BUF_ZYNQDAQ){
//		if(1){
//  			fprintf(stdout, "[client: %s]%s\n", inet_ntoa(cl_addr.sin_addr),buff);
//  			fprintf(stdout, "%d byte\n",rc);               /////////////////////////DEBUG
//  		}

  		/////////////////write to file/////////////////////
  		result=fwrite(buff,sizeof(char),rc,fd);
  		if(result!=rc){printf("return of fwrite is %lu\n",result);}


 //	if(1){printf("return of fwrite is %lu\n",result);}      ///for debug

  		/* confirm terminate order from client*/
  		if(strcmp(buff, "terminate") == 0){
  			exit_flag = 0;
  		}
  		//total_accept=total_accept+result;
  		total_accept=total_accept+rc;
  		lap_accept=lap_accept+rc;
		gettimeofday(&Probe2,NULL);       //////Probe
		Probe_diff = (Probe2.tv_sec-Probe1.tv_sec)*1.0E6+(Probe2.tv_usec-Probe1.tv_usec);
		totaltime=totaltime+Probe_diff;
	//	recievetime=Probe_diff;
		monitortime=monitortime+Probe_diff;
		gettimeofday(&Probe1,NULL);

  		if(monitortime>=pow(10.0,7.0)){
//			printf("totaltime:%luus\ttotalrecieve:%luByte\ndatarate:%fBMB/s\n",totaltime,total_accept,((1.0*total_accept)/totaltime*pow(10.0,6.0)));
//			printf("totaltime:%luus\ttotalrecieve:%luByte\ndatarate:%fBMB/s\n",totaltime,total_accept,(1.0*total_accept)/totaltime);
			printf("lap time:%luus\tlap recieve:%luByte\ndatarate:%fBMB/s\n",monitortime,lap_accept,(1.0*lap_accept)/monitortime);
			monitortime=0;
			lap_accept=0;
  		}
  	}
	printf("totaltime:%luus\ttotalrecieve:%luByte\ndatarate:%fBMB/s\n",totaltime,total_accept,(1.0*total_accept)/totaltime);
  return( 0 );
}

/*!
 * @brief      initialize a socket
 * @param[in]  info   : connection information of client
 * @param[out] errmsg : container of error message
 * @return     return 0 if succeed, otherwise return -1.
 */
static int
socket_initialize(sv_info_t *info, char *errmsg)
{
  int rc = 0;

  /* generate a socket : select TCP*/
  info->sd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  if(info->sd < 0){
    sprintf(errmsg, "(line:%d) %s", __LINE__, strerror(errno));
    return(-1);
  }

  /* generate address structure of server*/
  info->sv_addr.sin_family = AF_INET;
  info->sv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  info->sv_addr.sin_port = htons(info->sv_port);

  /* bind socket to local address and port No */
  rc = bind(info->sd, (struct sockaddr *)&(info->sv_addr),sizeof(info->sv_addr));
  if(rc != 0){
    sprintf(errmsg, "(line:%d) %s", __LINE__, strerror(errno));
    return(-1);
  }

  /* make socket stand-by*/
  rc = listen(info->sd, MAXPENDING);
  if(rc != 0){
    sprintf(errmsg, "(line:%d) %s", __LINE__, strerror(errno));
    return(-1);
  }

  return(0);
}

/*!
 * @brief      terminate a socket
 * @param[in]  info   : connection information of client
 * @return     return 0 if succeed, otherwise return -1.
 */
static void
socket_finalize(sv_info_t *info)
{
  /* close a connection */
  shutdown(info->sd, SHUT_RDWR);

  /* discard a socket */
//  if(info->sd != 0) close(info->sd);
  close(info->sd);

  return;
}

/*!
 * @brief      execute TCP server
 * @param[in]  info   : connection information of client
 * @param[out] errmsg : container of error message
 * @return     return 0 if succeed, otherwise return -1.
 */
static int
tcp_server(sv_info_t *info, char *errmsg,FILE *fd, unsigned long total_accept)
{
  int rc = 0;

  /* initialize a socket */
  rc = socket_initialize(info, errmsg);
  if(rc != 0) return(-1);

  /* receive a strings */
  rc = tcp_echo_server(info, errmsg,fd,total_accept);

  /* terminate a socket */
  socket_finalize(info);

  return(rc);
}

/*!
 * @brief      initialize a socket.  bind socket to local address and port No.
 * @param[in]  argc   Number of argument in command line
 * @param[in]  argv   argument in command line
 * @param[out] info   server information
 * @param[out] errmsg : container of error message
 * @return     return 0 if succeed, otherwise return -1.
 */
static int
initialize(int argc, char *argv[], sv_info_t *info, char *errmsg)
{
  if(argc != 2){
    sprintf(errmsg, "Usage: %s <port>\n", argv[0]);
    return(-1);
  }

  memset(info, 0, sizeof(sv_info_t));
  info->sv_port = atoi(argv[1]);

  return(0);
}

/*!
 * @brief   main routine
 * @return  return 0 if succeed, otherwise return -1.
 */
int
main(int argc, char *argv[])
{
  unsigned long total_accept=0;

  FILE *fd;
  char filename[200];
  sprintf(filename,"../../data/data.dat");
//	sprintf(filename,"./fwrite.dat");
  fd=fopen(filename,"wb");
  if(fd ==NULL){
	printf("failure opening file");
  }

  signal(SIGINT,handler); 	//"Ctrl+C" or "kill -9 ProccessNumber"

  int rc = 0;
  sv_info_t info = {0};
  char errmsg[256];

  rc = initialize(argc, argv, &info, errmsg);
  if(rc != 0){
    fprintf(stderr, "Error: %s\n", errmsg);
    return(-1);
  }

  rc = tcp_server(&info, errmsg,fd,total_accept);
  if(rc != 0){
    fprintf(stderr, "Error: %s\n", errmsg);
    return(-1);
  }
  fprintf(stdout, "end of server\n");
  return(0);
}
