#set_property PACKAGE_PIN Y11 [get_ports clk_out1]
#set_property IOSTANDARD LVCMOS33 [get_ports clk_out1]

#set_property PACKAGE_PIN N19 [get_ports clk_out1]
#set_property IOSTANDARD LVCMOS33 [get_ports clk_out1]


#set_property PACKAGE_PIN N19 [get_ports clk_to_pins_p]
#set_property IOSTANDARD LVDS_25 [get_ports clk_to_pins_p]
#set_property PACKAGE_PIN N20 [get_ports clk_to_pins_n]
#set_property IOSTANDARD LVDS_25 [get_ports clk_to_pins_n]


set_property PACKAGE_PIN P16 [get_ports reset_rtl]
#set_property IOSTANDARD LVCMOS33 [get_ports reset_rtl]
set_property IOSTANDARD LVCMOS25 [get_ports reset_rtl]


#set_property PACKAGE_PIN W6 [get_ports diff_clk_to_pins_0_clk_p]

#set_property PACKAGE_PIN N19 [get_ports dout_0]
#set_property PACKAGE_PIN N20 [get_ports dout_1]
#set_property PACKAGE_PIN Y10 [get_ports dout_0]
#set_property PACKAGE_PIN Y11 [get_ports dout_1]
#set_property IOSTANDARD LVCMOS33 [get_ports dout_1]


#set_property PACKAGE_PIN J18 [get_ports {clkout_0}]
#set_property IOSTANDARD LVCMOS25 [get_ports {clkout_0}]

set_property PACKAGE_PIN J18 [get_ports clk_to_pins_p_0]
set_property IOSTANDARD LVDS_25 [get_ports clk_to_pins_p_0]
#set_property IOSTANDARD LVDS_33 [get_ports clk_to_pins_p_0]
set_property PACKAGE_PIN K18 [get_ports clk_to_pins_n_0]
set_property IOSTANDARD LVDS_25 [get_ports clk_to_pins_n_0]
#set_property IOSTANDARD LVDS_33 [get_ports clk_to_pins_n_0]

#set_property PACKAGE_PIN L21 [get_ports clk_to_pins_p_0]
#set_property IOSTANDARD LVDS_25 [get_ports clk_to_pins_p_0]
#set_property PACKAGE_PIN L22 [get_ports clk_to_pins_n_0]
#set_property IOSTANDARD LVDS_25 [get_ports clk_to_pins_n_0]

#set_property IOSTANDARD LVDS_25 [get_ports clk_to_pins_p_0]
#set_property PACKAGE_PIN R21 [get_ports clk_to_pins_n_0]
#set_property PACKAGE_PIN R20 [get_ports clk_to_pins_p_0]
#set_property IOSTANDARD LVDS_25 [get_ports clk_to_pins_n_0]

set_property PACKAGE_PIN R20 [get_ports data_in_from_pins_p_0]
set_property IOSTANDARD LVDS_25 [get_ports data_in_from_pins_p_0]
#set_property IOSTANDARD LVDS_33 [get_ports data_in_from_pins_p_0]
set_property PACKAGE_PIN R21 [get_ports data_in_from_pins_n_0]
set_property IOSTANDARD LVDS_25 [get_ports data_in_from_pins_n_0]
#set_property IOSTANDARD LVDS_33 [get_ports data_in_from_pins_n_0]



#set_property PACKAGE_PIN Y11 [get_ports data_in_to_device_0]
#set_property IOSTANDARD LVCMOS25 [get_ports data_in_to_device_0]

#set_property PACKAGE_PIN R20 [get_ports input_data01]
#set_property IOSTANDARD LVCMOS25 [get_ports input_data01]
#set_property PACKAGE_PIN Y11 [get_ports output_data01]
#set_property IOSTANDARD LVCMOS33 [get_ports output_data01]
