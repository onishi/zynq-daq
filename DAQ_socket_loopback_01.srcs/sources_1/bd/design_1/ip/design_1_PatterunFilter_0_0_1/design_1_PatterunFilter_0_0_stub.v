// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
// Date        : Wed Apr 17 14:31:25 2019
// Host        : fpga01 running 64-bit unknown
// Command     : write_verilog -force -mode synth_stub
//               /mnt/HDD1/onishi/Vivado_Project/work/DAQ_socket_loopback_01/DAQ_socket_loopback_01.srcs/sources_1/bd/design_1/ip/design_1_PatterunFilter_0_0_1/design_1_PatterunFilter_0_0_stub.v
// Design      : design_1_PatterunFilter_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "PatterunFilter,Vivado 2017.4" *)
module design_1_PatterunFilter_0_0(through_filter, filter_pattern, in_data, 
  in_data_Enable, out_data, out_data_Enable, clk_in)
/* synthesis syn_black_box black_box_pad_pin="through_filter,filter_pattern[31:0],in_data[31:0],in_data_Enable,out_data[31:0],out_data_Enable,clk_in" */;
  input through_filter;
  input [31:0]filter_pattern;
  input [31:0]in_data;
  input in_data_Enable;
  output [31:0]out_data;
  output out_data_Enable;
  input clk_in;
endmodule
