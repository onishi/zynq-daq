-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
-- Date        : Fri Dec 14 17:50:04 2018
-- Host        : fpga01 running 64-bit unknown
-- Command     : write_vhdl -force -mode funcsim
--               /mnt/HDD1/onishi/Vivado_Project/work/DAQ_byDMA_FEI4/DAQ_byDMA_FEI4.srcs/sources_1/bd/design_1/ip/design_1_SeriPara_0_0/design_1_SeriPara_0_0_sim_netlist.vhdl
-- Design      : design_1_SeriPara_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_SeriPara_0_0_SeriPara is
  port (
    ParallelSignal : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ParallelEnable : out STD_LOGIC;
    clk_in : in STD_LOGIC;
    SerialSignal : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_SeriPara_0_0_SeriPara : entity is "SeriPara";
end design_1_SeriPara_0_0_SeriPara;

architecture STRUCTURE of design_1_SeriPara_0_0_SeriPara is
  signal Counter : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal \Counter_reg_n_0_[0]\ : STD_LOGIC;
  signal \Counter_reg_n_0_[1]\ : STD_LOGIC;
  signal \Counter_reg_n_0_[2]\ : STD_LOGIC;
  signal \Counter_reg_n_0_[3]\ : STD_LOGIC;
  signal \Counter_reg_n_0_[4]\ : STD_LOGIC;
  signal \Counter_reg_n_0_[5]\ : STD_LOGIC;
  signal \^parallelenable\ : STD_LOGIC;
  signal ParallelEnable_reg_i_1_n_0 : STD_LOGIC;
  signal ParallelEnable_reg_i_2_n_0 : STD_LOGIC;
  signal ParallelEnable_reg_i_3_n_0 : STD_LOGIC;
  signal \^parallelsignal\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal ParallelSignal_reg : STD_LOGIC_VECTOR ( 31 to 31 );
  signal \ParallelSignal_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \ParallelSignal_reg[31]_i_1_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \Counter[0]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \Counter[1]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \Counter[2]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \Counter[3]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \Counter[4]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of ParallelEnable_reg_i_3 : label is "soft_lutpair0";
begin
  ParallelEnable <= \^parallelenable\;
  ParallelSignal(31 downto 0) <= \^parallelsignal\(31 downto 0);
\Counter[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \Counter_reg_n_0_[0]\,
      O => Counter(0)
    );
\Counter[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \Counter_reg_n_0_[1]\,
      I1 => \Counter_reg_n_0_[0]\,
      O => Counter(1)
    );
\Counter[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \Counter_reg_n_0_[2]\,
      I1 => \Counter_reg_n_0_[1]\,
      I2 => \Counter_reg_n_0_[0]\,
      O => Counter(2)
    );
\Counter[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \Counter_reg_n_0_[3]\,
      I1 => \Counter_reg_n_0_[1]\,
      I2 => \Counter_reg_n_0_[0]\,
      I3 => \Counter_reg_n_0_[2]\,
      O => Counter(3)
    );
\Counter[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \Counter_reg_n_0_[4]\,
      I1 => \Counter_reg_n_0_[2]\,
      I2 => \Counter_reg_n_0_[0]\,
      I3 => \Counter_reg_n_0_[1]\,
      I4 => \Counter_reg_n_0_[3]\,
      O => Counter(4)
    );
\Counter[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAA8"
    )
        port map (
      I0 => \Counter_reg_n_0_[5]\,
      I1 => \Counter_reg_n_0_[3]\,
      I2 => \Counter_reg_n_0_[1]\,
      I3 => \Counter_reg_n_0_[0]\,
      I4 => \Counter_reg_n_0_[2]\,
      I5 => \Counter_reg_n_0_[4]\,
      O => Counter(5)
    );
\Counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => Counter(0),
      Q => \Counter_reg_n_0_[0]\,
      R => '0'
    );
\Counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => Counter(1),
      Q => \Counter_reg_n_0_[1]\,
      R => '0'
    );
\Counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => Counter(2),
      Q => \Counter_reg_n_0_[2]\,
      R => '0'
    );
\Counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => Counter(3),
      Q => \Counter_reg_n_0_[3]\,
      R => '0'
    );
\Counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => Counter(4),
      Q => \Counter_reg_n_0_[4]\,
      R => '0'
    );
\Counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => Counter(5),
      Q => \Counter_reg_n_0_[5]\,
      R => '0'
    );
ParallelEnable_reg_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88A8"
    )
        port map (
      I0 => ParallelEnable_reg_i_2_n_0,
      I1 => \^parallelenable\,
      I2 => ParallelEnable_reg_i_3_n_0,
      I3 => \Counter_reg_n_0_[5]\,
      O => ParallelEnable_reg_i_1_n_0
    );
ParallelEnable_reg_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFFFFFF"
    )
        port map (
      I0 => \Counter_reg_n_0_[4]\,
      I1 => \Counter_reg_n_0_[2]\,
      I2 => \Counter_reg_n_0_[0]\,
      I3 => \Counter_reg_n_0_[1]\,
      I4 => \Counter_reg_n_0_[3]\,
      I5 => \Counter_reg_n_0_[5]\,
      O => ParallelEnable_reg_i_2_n_0
    );
ParallelEnable_reg_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \Counter_reg_n_0_[3]\,
      I1 => \Counter_reg_n_0_[1]\,
      I2 => \Counter_reg_n_0_[0]\,
      I3 => \Counter_reg_n_0_[2]\,
      I4 => \Counter_reg_n_0_[4]\,
      O => ParallelEnable_reg_i_3_n_0
    );
ParallelEnable_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => ParallelEnable_reg_i_1_n_0,
      Q => \^parallelenable\,
      R => '0'
    );
\ParallelSignal_reg[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => SerialSignal,
      I1 => ParallelSignal_reg(31),
      I2 => \^parallelsignal\(0),
      O => \ParallelSignal_reg[0]_i_1_n_0\
    );
\ParallelSignal_reg[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => \Counter_reg_n_0_[5]\,
      I1 => \Counter_reg_n_0_[3]\,
      I2 => \Counter_reg_n_0_[1]\,
      I3 => \Counter_reg_n_0_[0]\,
      I4 => \Counter_reg_n_0_[2]\,
      I5 => \Counter_reg_n_0_[4]\,
      O => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \Counter_reg_n_0_[4]\,
      I1 => \Counter_reg_n_0_[2]\,
      I2 => \Counter_reg_n_0_[0]\,
      I3 => \Counter_reg_n_0_[1]\,
      I4 => \Counter_reg_n_0_[3]\,
      I5 => \Counter_reg_n_0_[5]\,
      O => ParallelSignal_reg(31)
    );
\ParallelSignal_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \ParallelSignal_reg[0]_i_1_n_0\,
      Q => \^parallelsignal\(0),
      R => '0'
    );
\ParallelSignal_reg_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(9),
      Q => \^parallelsignal\(10),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(10),
      Q => \^parallelsignal\(11),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(11),
      Q => \^parallelsignal\(12),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(12),
      Q => \^parallelsignal\(13),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(13),
      Q => \^parallelsignal\(14),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(14),
      Q => \^parallelsignal\(15),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(15),
      Q => \^parallelsignal\(16),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(16),
      Q => \^parallelsignal\(17),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(17),
      Q => \^parallelsignal\(18),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(18),
      Q => \^parallelsignal\(19),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(0),
      Q => \^parallelsignal\(1),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(19),
      Q => \^parallelsignal\(20),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(20),
      Q => \^parallelsignal\(21),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(21),
      Q => \^parallelsignal\(22),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(22),
      Q => \^parallelsignal\(23),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(23),
      Q => \^parallelsignal\(24),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(24),
      Q => \^parallelsignal\(25),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(25),
      Q => \^parallelsignal\(26),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(26),
      Q => \^parallelsignal\(27),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(27),
      Q => \^parallelsignal\(28),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(28),
      Q => \^parallelsignal\(29),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(1),
      Q => \^parallelsignal\(2),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(29),
      Q => \^parallelsignal\(30),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(30),
      Q => \^parallelsignal\(31),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(2),
      Q => \^parallelsignal\(3),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(3),
      Q => \^parallelsignal\(4),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(4),
      Q => \^parallelsignal\(5),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(5),
      Q => \^parallelsignal\(6),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(6),
      Q => \^parallelsignal\(7),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(7),
      Q => \^parallelsignal\(8),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
\ParallelSignal_reg_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => ParallelSignal_reg(31),
      D => \^parallelsignal\(8),
      Q => \^parallelsignal\(9),
      R => \ParallelSignal_reg[31]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_SeriPara_0_0 is
  port (
    clk_in : in STD_LOGIC;
    SerialSignal : in STD_LOGIC;
    ParallelSignal : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ParallelEnable : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_SeriPara_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_SeriPara_0_0 : entity is "design_1_SeriPara_0_0,SeriPara,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_SeriPara_0_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_SeriPara_0_0 : entity is "SeriPara,Vivado 2017.4";
end design_1_SeriPara_0_0;

architecture STRUCTURE of design_1_SeriPara_0_0 is
begin
inst: entity work.design_1_SeriPara_0_0_SeriPara
     port map (
      ParallelEnable => ParallelEnable,
      ParallelSignal(31 downto 0) => ParallelSignal(31 downto 0),
      SerialSignal => SerialSignal,
      clk_in => clk_in
    );
end STRUCTURE;
