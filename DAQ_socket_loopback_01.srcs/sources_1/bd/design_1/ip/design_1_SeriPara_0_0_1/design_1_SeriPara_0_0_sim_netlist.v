// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
// Date        : Wed Apr 17 14:31:29 2019
// Host        : fpga01 running 64-bit unknown
// Command     : write_verilog -force -mode funcsim
//               /mnt/HDD1/onishi/Vivado_Project/work/DAQ_socket_loopback_01/DAQ_socket_loopback_01.srcs/sources_1/bd/design_1/ip/design_1_SeriPara_0_0_1/design_1_SeriPara_0_0_sim_netlist.v
// Design      : design_1_SeriPara_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_SeriPara_0_0,SeriPara,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "SeriPara,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module design_1_SeriPara_0_0
   (clk_in,
    SerialSignal,
    ParallelSignal,
    ParallelEnable);
  input clk_in;
  input SerialSignal;
  output [31:0]ParallelSignal;
  output ParallelEnable;

  wire ParallelEnable;
  wire [31:0]ParallelSignal;
  wire SerialSignal;
  wire clk_in;

  design_1_SeriPara_0_0_SeriPara inst
       (.ParallelEnable(ParallelEnable),
        .ParallelSignal(ParallelSignal),
        .SerialSignal(SerialSignal),
        .clk_in(clk_in));
endmodule

(* ORIG_REF_NAME = "SeriPara" *) 
module design_1_SeriPara_0_0_SeriPara
   (ParallelSignal,
    ParallelEnable,
    clk_in,
    SerialSignal);
  output [31:0]ParallelSignal;
  output ParallelEnable;
  input clk_in;
  input SerialSignal;

  wire [5:0]Counter;
  wire \Counter_reg_n_0_[0] ;
  wire \Counter_reg_n_0_[1] ;
  wire \Counter_reg_n_0_[2] ;
  wire \Counter_reg_n_0_[3] ;
  wire \Counter_reg_n_0_[4] ;
  wire \Counter_reg_n_0_[5] ;
  wire ParallelEnable;
  wire ParallelEnable_reg_i_1_n_0;
  wire ParallelEnable_reg_i_2_n_0;
  wire ParallelEnable_reg_i_3_n_0;
  wire [31:0]ParallelSignal;
  wire [31:31]ParallelSignal_reg;
  wire \ParallelSignal_reg[0]_i_1_n_0 ;
  wire \ParallelSignal_reg[31]_i_1_n_0 ;
  wire SerialSignal;
  wire clk_in;

  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \Counter[0]_i_1 
       (.I0(\Counter_reg_n_0_[0] ),
        .O(Counter[0]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \Counter[1]_i_1 
       (.I0(\Counter_reg_n_0_[1] ),
        .I1(\Counter_reg_n_0_[0] ),
        .O(Counter[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \Counter[2]_i_1 
       (.I0(\Counter_reg_n_0_[2] ),
        .I1(\Counter_reg_n_0_[1] ),
        .I2(\Counter_reg_n_0_[0] ),
        .O(Counter[2]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \Counter[3]_i_1 
       (.I0(\Counter_reg_n_0_[3] ),
        .I1(\Counter_reg_n_0_[1] ),
        .I2(\Counter_reg_n_0_[0] ),
        .I3(\Counter_reg_n_0_[2] ),
        .O(Counter[3]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \Counter[4]_i_1 
       (.I0(\Counter_reg_n_0_[4] ),
        .I1(\Counter_reg_n_0_[2] ),
        .I2(\Counter_reg_n_0_[0] ),
        .I3(\Counter_reg_n_0_[1] ),
        .I4(\Counter_reg_n_0_[3] ),
        .O(Counter[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAA8)) 
    \Counter[5]_i_1 
       (.I0(\Counter_reg_n_0_[5] ),
        .I1(\Counter_reg_n_0_[3] ),
        .I2(\Counter_reg_n_0_[1] ),
        .I3(\Counter_reg_n_0_[0] ),
        .I4(\Counter_reg_n_0_[2] ),
        .I5(\Counter_reg_n_0_[4] ),
        .O(Counter[5]));
  FDRE #(
    .INIT(1'b0)) 
    \Counter_reg[0] 
       (.C(clk_in),
        .CE(1'b1),
        .D(Counter[0]),
        .Q(\Counter_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \Counter_reg[1] 
       (.C(clk_in),
        .CE(1'b1),
        .D(Counter[1]),
        .Q(\Counter_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \Counter_reg[2] 
       (.C(clk_in),
        .CE(1'b1),
        .D(Counter[2]),
        .Q(\Counter_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \Counter_reg[3] 
       (.C(clk_in),
        .CE(1'b1),
        .D(Counter[3]),
        .Q(\Counter_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \Counter_reg[4] 
       (.C(clk_in),
        .CE(1'b1),
        .D(Counter[4]),
        .Q(\Counter_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \Counter_reg[5] 
       (.C(clk_in),
        .CE(1'b1),
        .D(Counter[5]),
        .Q(\Counter_reg_n_0_[5] ),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h88A8)) 
    ParallelEnable_reg_i_1
       (.I0(ParallelEnable_reg_i_2_n_0),
        .I1(ParallelEnable),
        .I2(ParallelEnable_reg_i_3_n_0),
        .I3(\Counter_reg_n_0_[5] ),
        .O(ParallelEnable_reg_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    ParallelEnable_reg_i_2
       (.I0(\Counter_reg_n_0_[4] ),
        .I1(\Counter_reg_n_0_[2] ),
        .I2(\Counter_reg_n_0_[0] ),
        .I3(\Counter_reg_n_0_[1] ),
        .I4(\Counter_reg_n_0_[3] ),
        .I5(\Counter_reg_n_0_[5] ),
        .O(ParallelEnable_reg_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    ParallelEnable_reg_i_3
       (.I0(\Counter_reg_n_0_[3] ),
        .I1(\Counter_reg_n_0_[1] ),
        .I2(\Counter_reg_n_0_[0] ),
        .I3(\Counter_reg_n_0_[2] ),
        .I4(\Counter_reg_n_0_[4] ),
        .O(ParallelEnable_reg_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    ParallelEnable_reg_reg
       (.C(clk_in),
        .CE(1'b1),
        .D(ParallelEnable_reg_i_1_n_0),
        .Q(ParallelEnable),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hB8)) 
    \ParallelSignal_reg[0]_i_1 
       (.I0(SerialSignal),
        .I1(ParallelSignal_reg),
        .I2(ParallelSignal[0]),
        .O(\ParallelSignal_reg[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \ParallelSignal_reg[31]_i_1 
       (.I0(\Counter_reg_n_0_[5] ),
        .I1(\Counter_reg_n_0_[3] ),
        .I2(\Counter_reg_n_0_[1] ),
        .I3(\Counter_reg_n_0_[0] ),
        .I4(\Counter_reg_n_0_[2] ),
        .I5(\Counter_reg_n_0_[4] ),
        .O(\ParallelSignal_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \ParallelSignal_reg[31]_i_2 
       (.I0(\Counter_reg_n_0_[4] ),
        .I1(\Counter_reg_n_0_[2] ),
        .I2(\Counter_reg_n_0_[0] ),
        .I3(\Counter_reg_n_0_[1] ),
        .I4(\Counter_reg_n_0_[3] ),
        .I5(\Counter_reg_n_0_[5] ),
        .O(ParallelSignal_reg));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[0] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\ParallelSignal_reg[0]_i_1_n_0 ),
        .Q(ParallelSignal[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[10] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[9]),
        .Q(ParallelSignal[10]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[11] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[10]),
        .Q(ParallelSignal[11]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[12] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[11]),
        .Q(ParallelSignal[12]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[13] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[12]),
        .Q(ParallelSignal[13]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[14] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[13]),
        .Q(ParallelSignal[14]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[15] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[14]),
        .Q(ParallelSignal[15]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[16] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[15]),
        .Q(ParallelSignal[16]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[17] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[16]),
        .Q(ParallelSignal[17]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[18] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[17]),
        .Q(ParallelSignal[18]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[19] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[18]),
        .Q(ParallelSignal[19]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[1] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[0]),
        .Q(ParallelSignal[1]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[20] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[19]),
        .Q(ParallelSignal[20]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[21] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[20]),
        .Q(ParallelSignal[21]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[22] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[21]),
        .Q(ParallelSignal[22]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[23] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[22]),
        .Q(ParallelSignal[23]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[24] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[23]),
        .Q(ParallelSignal[24]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[25] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[24]),
        .Q(ParallelSignal[25]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[26] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[25]),
        .Q(ParallelSignal[26]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[27] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[26]),
        .Q(ParallelSignal[27]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[28] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[27]),
        .Q(ParallelSignal[28]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[29] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[28]),
        .Q(ParallelSignal[29]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[2] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[1]),
        .Q(ParallelSignal[2]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[30] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[29]),
        .Q(ParallelSignal[30]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[31] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[30]),
        .Q(ParallelSignal[31]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[3] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[2]),
        .Q(ParallelSignal[3]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[4] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[3]),
        .Q(ParallelSignal[4]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[5] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[4]),
        .Q(ParallelSignal[5]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[6] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[5]),
        .Q(ParallelSignal[6]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[7] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[6]),
        .Q(ParallelSignal[7]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[8] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[7]),
        .Q(ParallelSignal[8]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ParallelSignal_reg_reg[9] 
       (.C(clk_in),
        .CE(ParallelSignal_reg),
        .D(ParallelSignal[8]),
        .Q(ParallelSignal[9]),
        .R(\ParallelSignal_reg[31]_i_1_n_0 ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
