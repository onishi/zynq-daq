-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
-- Date        : Wed Apr 17 14:31:29 2019
-- Host        : fpga01 running 64-bit unknown
-- Command     : write_vhdl -force -mode synth_stub
--               /mnt/HDD1/onishi/Vivado_Project/work/DAQ_socket_loopback_01/DAQ_socket_loopback_01.srcs/sources_1/bd/design_1/ip/design_1_SeriPara_0_0_1/design_1_SeriPara_0_0_stub.vhdl
-- Design      : design_1_SeriPara_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_SeriPara_0_0 is
  Port ( 
    clk_in : in STD_LOGIC;
    SerialSignal : in STD_LOGIC;
    ParallelSignal : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ParallelEnable : out STD_LOGIC
  );

end design_1_SeriPara_0_0;

architecture stub of design_1_SeriPara_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk_in,SerialSignal,ParallelSignal[31:0],ParallelEnable";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "SeriPara,Vivado 2017.4";
begin
end;
