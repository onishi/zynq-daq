-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
-- Date        : Thu Dec  6 19:27:30 2018
-- Host        : fpga01 running 64-bit unknown
-- Command     : write_vhdl -force -mode synth_stub
--               /mnt/HDD1/onishi/Vivado_Project/work/DAQ_byDMA_FEI4/DAQ_byDMA_FEI4.srcs/sources_1/bd/design_1/ip/design_1_AXIStream_Converter_0_0/design_1_AXIStream_Converter_0_0_stub.vhdl
-- Design      : design_1_AXIStream_Converter_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_AXIStream_Converter_0_0 is
  Port ( 
    INIT : in STD_LOGIC;
    empty : in STD_LOGIC;
    almost_full : in STD_LOGIC;
    addTLAST : in STD_LOGIC;
    input_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    NUM_of_packet : in STD_LOGIC_VECTOR ( 31 downto 0 );
    rd_en : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC
  );

end design_1_AXIStream_Converter_0_0;

architecture stub of design_1_AXIStream_Converter_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "INIT,empty,almost_full,addTLAST,input_data[31:0],NUM_of_packet[31:0],rd_en,m00_axis_tdata[31:0],m00_axis_tstrb[3:0],m00_axis_tlast,m00_axis_tvalid,m00_axis_tready,m00_axis_aclk,m00_axis_aresetn";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "AXIStream_Converter_v1_0,Vivado 2017.4";
begin
end;
