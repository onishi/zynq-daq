// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
// Date        : Thu Dec  6 19:27:30 2018
// Host        : fpga01 running 64-bit unknown
// Command     : write_verilog -force -mode funcsim
//               /mnt/HDD1/onishi/Vivado_Project/work/DAQ_byDMA_FEI4/DAQ_byDMA_FEI4.srcs/sources_1/bd/design_1/ip/design_1_AXIStream_Converter_0_0/design_1_AXIStream_Converter_0_0_sim_netlist.v
// Design      : design_1_AXIStream_Converter_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_AXIStream_Converter_0_0,AXIStream_Converter_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "AXIStream_Converter_v1_0,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module design_1_AXIStream_Converter_0_0
   (INIT,
    empty,
    almost_full,
    addTLAST,
    input_data,
    NUM_of_packet,
    rd_en,
    m00_axis_tdata,
    m00_axis_tstrb,
    m00_axis_tlast,
    m00_axis_tvalid,
    m00_axis_tready,
    m00_axis_aclk,
    m00_axis_aresetn);
  input INIT;
  input empty;
  input almost_full;
  input addTLAST;
  input [31:0]input_data;
  input [31:0]NUM_of_packet;
  output rd_en;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TDATA" *) output [31:0]m00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB" *) output [3:0]m00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TLAST" *) output m00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TVALID" *) output m00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef" *) input m00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 M00_AXIS_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS_CLK, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0" *) input m00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 M00_AXIS_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS_RST, POLARITY ACTIVE_LOW" *) input m00_axis_aresetn;

  wire \<const1> ;
  wire INIT;
  wire [31:0]NUM_of_packet;
  wire addTLAST;
  wire almost_full;
  wire empty;
  wire [31:0]input_data;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tlast;
  wire m00_axis_tready;
  wire m00_axis_tvalid;
  wire rd_en;

  assign m00_axis_tstrb[3] = \<const1> ;
  assign m00_axis_tstrb[2] = \<const1> ;
  assign m00_axis_tstrb[1] = \<const1> ;
  assign m00_axis_tstrb[0] = \<const1> ;
  VCC VCC
       (.P(\<const1> ));
  design_1_AXIStream_Converter_0_0_AXIStream_Converter_v1_0 inst
       (.INIT(INIT),
        .NUM_of_packet(NUM_of_packet),
        .addTLAST(addTLAST),
        .almost_full(almost_full),
        .empty(empty),
        .input_data(input_data),
        .m00_axis_aclk(m00_axis_aclk),
        .m00_axis_aresetn(m00_axis_aresetn),
        .m00_axis_tdata(m00_axis_tdata),
        .m00_axis_tlast(m00_axis_tlast),
        .m00_axis_tready(m00_axis_tready),
        .m00_axis_tvalid(m00_axis_tvalid),
        .rd_en(rd_en));
endmodule

(* ORIG_REF_NAME = "AXIStream_Converter_v1_0" *) 
module design_1_AXIStream_Converter_0_0_AXIStream_Converter_v1_0
   (m00_axis_tvalid,
    m00_axis_tlast,
    m00_axis_tdata,
    rd_en,
    m00_axis_tready,
    INIT,
    m00_axis_aresetn,
    almost_full,
    m00_axis_aclk,
    input_data,
    addTLAST,
    NUM_of_packet,
    empty);
  output m00_axis_tvalid;
  output m00_axis_tlast;
  output [31:0]m00_axis_tdata;
  output rd_en;
  input m00_axis_tready;
  input INIT;
  input m00_axis_aresetn;
  input almost_full;
  input m00_axis_aclk;
  input [31:0]input_data;
  input addTLAST;
  input [31:0]NUM_of_packet;
  input empty;

  wire INIT;
  wire [31:0]NUM_of_packet;
  wire addTLAST;
  wire almost_full;
  wire empty;
  wire [31:0]input_data;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tlast;
  wire m00_axis_tready;
  wire m00_axis_tvalid;
  wire rd_en;

  design_1_AXIStream_Converter_0_0_AXIStream_Converter_v1_0_M00_AXIS AXIStream_Converter_v1_0_M00_AXIS_inst
       (.INIT(INIT),
        .NUM_of_packet(NUM_of_packet),
        .addTLAST(addTLAST),
        .almost_full(almost_full),
        .empty(empty),
        .input_data(input_data),
        .m00_axis_aclk(m00_axis_aclk),
        .m00_axis_aresetn(m00_axis_aresetn),
        .m00_axis_tdata(m00_axis_tdata),
        .m00_axis_tlast(m00_axis_tlast),
        .m00_axis_tready(m00_axis_tready),
        .m00_axis_tvalid(m00_axis_tvalid),
        .rd_en(rd_en));
endmodule

(* ORIG_REF_NAME = "AXIStream_Converter_v1_0_M00_AXIS" *) 
module design_1_AXIStream_Converter_0_0_AXIStream_Converter_v1_0_M00_AXIS
   (m00_axis_tvalid,
    m00_axis_tlast,
    m00_axis_tdata,
    rd_en,
    m00_axis_tready,
    INIT,
    m00_axis_aresetn,
    almost_full,
    m00_axis_aclk,
    input_data,
    addTLAST,
    NUM_of_packet,
    empty);
  output m00_axis_tvalid;
  output m00_axis_tlast;
  output [31:0]m00_axis_tdata;
  output rd_en;
  input m00_axis_tready;
  input INIT;
  input m00_axis_aresetn;
  input almost_full;
  input m00_axis_aclk;
  input [31:0]input_data;
  input addTLAST;
  input [31:0]NUM_of_packet;
  input empty;

  wire [31:0]COUNT;
  wire [31:1]COUNT0;
  wire COUNT0_carry__0_n_0;
  wire COUNT0_carry__0_n_1;
  wire COUNT0_carry__0_n_2;
  wire COUNT0_carry__0_n_3;
  wire COUNT0_carry__1_n_0;
  wire COUNT0_carry__1_n_1;
  wire COUNT0_carry__1_n_2;
  wire COUNT0_carry__1_n_3;
  wire COUNT0_carry__2_n_0;
  wire COUNT0_carry__2_n_1;
  wire COUNT0_carry__2_n_2;
  wire COUNT0_carry__2_n_3;
  wire COUNT0_carry__3_n_0;
  wire COUNT0_carry__3_n_1;
  wire COUNT0_carry__3_n_2;
  wire COUNT0_carry__3_n_3;
  wire COUNT0_carry__4_n_0;
  wire COUNT0_carry__4_n_1;
  wire COUNT0_carry__4_n_2;
  wire COUNT0_carry__4_n_3;
  wire COUNT0_carry__5_n_0;
  wire COUNT0_carry__5_n_1;
  wire COUNT0_carry__5_n_2;
  wire COUNT0_carry__5_n_3;
  wire COUNT0_carry__6_n_2;
  wire COUNT0_carry__6_n_3;
  wire COUNT0_carry_n_0;
  wire COUNT0_carry_n_1;
  wire COUNT0_carry_n_2;
  wire COUNT0_carry_n_3;
  wire COUNT2;
  wire COUNT2_carry__0_i_1_n_0;
  wire COUNT2_carry__0_i_2_n_0;
  wire COUNT2_carry__0_i_3_n_0;
  wire COUNT2_carry__0_i_4_n_0;
  wire COUNT2_carry__0_n_0;
  wire COUNT2_carry__0_n_1;
  wire COUNT2_carry__0_n_2;
  wire COUNT2_carry__0_n_3;
  wire COUNT2_carry__1_i_1_n_0;
  wire COUNT2_carry__1_i_2_n_0;
  wire COUNT2_carry__1_i_3_n_0;
  wire COUNT2_carry__1_n_2;
  wire COUNT2_carry__1_n_3;
  wire COUNT2_carry_i_1_n_0;
  wire COUNT2_carry_i_2_n_0;
  wire COUNT2_carry_i_3_n_0;
  wire COUNT2_carry_i_4_n_0;
  wire COUNT2_carry_n_0;
  wire COUNT2_carry_n_1;
  wire COUNT2_carry_n_2;
  wire COUNT2_carry_n_3;
  wire [31:1]COUNT3;
  wire COUNT3_carry__0_i_1_n_0;
  wire COUNT3_carry__0_i_2_n_0;
  wire COUNT3_carry__0_i_3_n_0;
  wire COUNT3_carry__0_i_4_n_0;
  wire COUNT3_carry__0_n_0;
  wire COUNT3_carry__0_n_1;
  wire COUNT3_carry__0_n_2;
  wire COUNT3_carry__0_n_3;
  wire COUNT3_carry__1_i_1_n_0;
  wire COUNT3_carry__1_i_2_n_0;
  wire COUNT3_carry__1_i_3_n_0;
  wire COUNT3_carry__1_i_4_n_0;
  wire COUNT3_carry__1_n_0;
  wire COUNT3_carry__1_n_1;
  wire COUNT3_carry__1_n_2;
  wire COUNT3_carry__1_n_3;
  wire COUNT3_carry__2_i_1_n_0;
  wire COUNT3_carry__2_i_2_n_0;
  wire COUNT3_carry__2_i_3_n_0;
  wire COUNT3_carry__2_i_4_n_0;
  wire COUNT3_carry__2_n_0;
  wire COUNT3_carry__2_n_1;
  wire COUNT3_carry__2_n_2;
  wire COUNT3_carry__2_n_3;
  wire COUNT3_carry__3_i_1_n_0;
  wire COUNT3_carry__3_i_2_n_0;
  wire COUNT3_carry__3_i_3_n_0;
  wire COUNT3_carry__3_i_4_n_0;
  wire COUNT3_carry__3_n_0;
  wire COUNT3_carry__3_n_1;
  wire COUNT3_carry__3_n_2;
  wire COUNT3_carry__3_n_3;
  wire COUNT3_carry__4_i_1_n_0;
  wire COUNT3_carry__4_i_2_n_0;
  wire COUNT3_carry__4_i_3_n_0;
  wire COUNT3_carry__4_i_4_n_0;
  wire COUNT3_carry__4_n_0;
  wire COUNT3_carry__4_n_1;
  wire COUNT3_carry__4_n_2;
  wire COUNT3_carry__4_n_3;
  wire COUNT3_carry__5_i_1_n_0;
  wire COUNT3_carry__5_i_2_n_0;
  wire COUNT3_carry__5_i_3_n_0;
  wire COUNT3_carry__5_i_4_n_0;
  wire COUNT3_carry__5_n_0;
  wire COUNT3_carry__5_n_1;
  wire COUNT3_carry__5_n_2;
  wire COUNT3_carry__5_n_3;
  wire COUNT3_carry__6_i_1_n_0;
  wire COUNT3_carry__6_i_2_n_0;
  wire COUNT3_carry__6_i_3_n_0;
  wire COUNT3_carry__6_n_2;
  wire COUNT3_carry__6_n_3;
  wire COUNT3_carry_i_1_n_0;
  wire COUNT3_carry_i_2_n_0;
  wire COUNT3_carry_i_3_n_0;
  wire COUNT3_carry_i_4_n_0;
  wire COUNT3_carry_n_0;
  wire COUNT3_carry_n_1;
  wire COUNT3_carry_n_2;
  wire COUNT3_carry_n_3;
  wire \COUNT[31]_i_2_n_0 ;
  wire \COUNT[31]_i_4_n_0 ;
  wire COUNT_0;
  wire INIT;
  wire [26:0]M_AXIS_TDATA_reg;
  wire \M_AXIS_TDATA_reg[14]_i_1_n_0 ;
  wire \M_AXIS_TDATA_reg[16]_i_2_n_0 ;
  wire \M_AXIS_TDATA_reg[16]_i_3_n_0 ;
  wire \M_AXIS_TDATA_reg[16]_i_4_n_0 ;
  wire \M_AXIS_TDATA_reg[16]_i_5_n_0 ;
  wire \M_AXIS_TDATA_reg[17]_i_2_n_0 ;
  wire \M_AXIS_TDATA_reg[18]_i_2_n_0 ;
  wire \M_AXIS_TDATA_reg[18]_i_3_n_0 ;
  wire \M_AXIS_TDATA_reg[18]_i_4_n_0 ;
  wire \M_AXIS_TDATA_reg[21]_i_1_n_0 ;
  wire \M_AXIS_TDATA_reg[22]_i_1_n_0 ;
  wire \M_AXIS_TDATA_reg[24]_i_2_n_0 ;
  wire \M_AXIS_TDATA_reg[24]_i_3_n_0 ;
  wire \M_AXIS_TDATA_reg[25]_i_2_n_0 ;
  wire \M_AXIS_TDATA_reg[26]_i_2_n_0 ;
  wire \M_AXIS_TDATA_reg[27]_i_1_n_0 ;
  wire \M_AXIS_TDATA_reg[29]_i_1_n_0 ;
  wire \M_AXIS_TDATA_reg[30]_i_1_n_0 ;
  wire \M_AXIS_TDATA_reg[30]_i_2_n_0 ;
  wire \M_AXIS_TDATA_reg[30]_i_4_n_0 ;
  wire \M_AXIS_TDATA_reg[31]_i_1_n_0 ;
  wire \M_AXIS_TDATA_reg[31]_i_2_n_0 ;
  wire \M_AXIS_TDATA_reg[31]_i_3_n_0 ;
  wire \M_AXIS_TDATA_reg[31]_i_4_n_0 ;
  wire \M_AXIS_TDATA_reg[31]_i_5_n_0 ;
  wire \M_AXIS_TDATA_reg[5]_i_1_n_0 ;
  wire \M_AXIS_TDATA_reg[6]_i_1_n_0 ;
  wire \M_AXIS_TDATA_reg[8]_i_2_n_0 ;
  wire M_AXIS_TLAST_reg;
  wire M_AXIS_TLAST_reg_i_2_n_0;
  wire M_AXIS_TVALID_reg11_out;
  wire M_AXIS_TVALID_reg_i_1_n_0;
  wire [31:0]NUM_of_packet;
  wire [2:0]TLAST_COUNT;
  wire \TLAST_COUNT[0]_i_1_n_0 ;
  wire \TLAST_COUNT[0]_i_2_n_0 ;
  wire \TLAST_COUNT[0]_i_3_n_0 ;
  wire \TLAST_COUNT[0]_i_4_n_0 ;
  wire \TLAST_COUNT[0]_i_5_n_0 ;
  wire \TLAST_COUNT[0]_i_6_n_0 ;
  wire \TLAST_COUNT[0]_i_7_n_0 ;
  wire \TLAST_COUNT[1]_i_1_n_0 ;
  wire \TLAST_COUNT[2]_i_1_n_0 ;
  wire \TLAST_COUNT[2]_i_2_n_0 ;
  wire \TLAST_COUNT[2]_i_3_n_0 ;
  wire \TLAST_COUNT[2]_i_4_n_0 ;
  wire TLAST_FLAG_i_1_n_0;
  wire TLAST_FLAG_reg_n_0;
  wire addTLAST;
  wire addTLAST_VETO;
  wire addTLAST_VETO_i_1_n_0;
  wire addTLAST_VETO_reg_n_0;
  wire almost_full;
  wire empty;
  wire input_DATA_VALID_FLAG;
  wire input_DATA_VALID_FLAG_i_1_n_0;
  wire [31:0]input_data;
  wire [31:0]input_data_reg;
  wire \input_data_reg[31]_i_1_n_0 ;
  wire \input_data_reg[31]_i_2_n_0 ;
  wire input_data_reg_VALID_FLAG_i_1_n_0;
  wire input_data_reg_VALID_FLAG_i_2_n_0;
  wire input_data_reg_VALID_FLAG_i_3_n_0;
  wire input_data_reg_VALID_FLAG_i_4_n_0;
  wire input_data_reg_VALID_FLAG_i_5_n_0;
  wire input_data_reg_VALID_FLAG_reg_n_0;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tlast;
  wire m00_axis_tready;
  wire m00_axis_tvalid;
  wire [31:0]p_1_in;
  wire rd_en;
  wire rd_en_reg_i_1_n_0;
  wire rd_en_reg_i_2_n_0;
  wire rd_en_reg_i_3_n_0;
  wire rd_en_reg_i_4_n_0;
  wire [3:2]NLW_COUNT0_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_COUNT0_carry__6_O_UNCONNECTED;
  wire [3:0]NLW_COUNT2_carry_O_UNCONNECTED;
  wire [3:0]NLW_COUNT2_carry__0_O_UNCONNECTED;
  wire [3:3]NLW_COUNT2_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_COUNT2_carry__1_O_UNCONNECTED;
  wire [3:2]NLW_COUNT3_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_COUNT3_carry__6_O_UNCONNECTED;

  CARRY4 COUNT0_carry
       (.CI(1'b0),
        .CO({COUNT0_carry_n_0,COUNT0_carry_n_1,COUNT0_carry_n_2,COUNT0_carry_n_3}),
        .CYINIT(COUNT[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(COUNT0[4:1]),
        .S(COUNT[4:1]));
  CARRY4 COUNT0_carry__0
       (.CI(COUNT0_carry_n_0),
        .CO({COUNT0_carry__0_n_0,COUNT0_carry__0_n_1,COUNT0_carry__0_n_2,COUNT0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(COUNT0[8:5]),
        .S(COUNT[8:5]));
  CARRY4 COUNT0_carry__1
       (.CI(COUNT0_carry__0_n_0),
        .CO({COUNT0_carry__1_n_0,COUNT0_carry__1_n_1,COUNT0_carry__1_n_2,COUNT0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(COUNT0[12:9]),
        .S(COUNT[12:9]));
  CARRY4 COUNT0_carry__2
       (.CI(COUNT0_carry__1_n_0),
        .CO({COUNT0_carry__2_n_0,COUNT0_carry__2_n_1,COUNT0_carry__2_n_2,COUNT0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(COUNT0[16:13]),
        .S(COUNT[16:13]));
  CARRY4 COUNT0_carry__3
       (.CI(COUNT0_carry__2_n_0),
        .CO({COUNT0_carry__3_n_0,COUNT0_carry__3_n_1,COUNT0_carry__3_n_2,COUNT0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(COUNT0[20:17]),
        .S(COUNT[20:17]));
  CARRY4 COUNT0_carry__4
       (.CI(COUNT0_carry__3_n_0),
        .CO({COUNT0_carry__4_n_0,COUNT0_carry__4_n_1,COUNT0_carry__4_n_2,COUNT0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(COUNT0[24:21]),
        .S(COUNT[24:21]));
  CARRY4 COUNT0_carry__5
       (.CI(COUNT0_carry__4_n_0),
        .CO({COUNT0_carry__5_n_0,COUNT0_carry__5_n_1,COUNT0_carry__5_n_2,COUNT0_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(COUNT0[28:25]),
        .S(COUNT[28:25]));
  CARRY4 COUNT0_carry__6
       (.CI(COUNT0_carry__5_n_0),
        .CO({NLW_COUNT0_carry__6_CO_UNCONNECTED[3:2],COUNT0_carry__6_n_2,COUNT0_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_COUNT0_carry__6_O_UNCONNECTED[3],COUNT0[31:29]}),
        .S({1'b0,COUNT[31:29]}));
  CARRY4 COUNT2_carry
       (.CI(1'b0),
        .CO({COUNT2_carry_n_0,COUNT2_carry_n_1,COUNT2_carry_n_2,COUNT2_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_COUNT2_carry_O_UNCONNECTED[3:0]),
        .S({COUNT2_carry_i_1_n_0,COUNT2_carry_i_2_n_0,COUNT2_carry_i_3_n_0,COUNT2_carry_i_4_n_0}));
  CARRY4 COUNT2_carry__0
       (.CI(COUNT2_carry_n_0),
        .CO({COUNT2_carry__0_n_0,COUNT2_carry__0_n_1,COUNT2_carry__0_n_2,COUNT2_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_COUNT2_carry__0_O_UNCONNECTED[3:0]),
        .S({COUNT2_carry__0_i_1_n_0,COUNT2_carry__0_i_2_n_0,COUNT2_carry__0_i_3_n_0,COUNT2_carry__0_i_4_n_0}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    COUNT2_carry__0_i_1
       (.I0(COUNT3[23]),
        .I1(COUNT[23]),
        .I2(COUNT3[22]),
        .I3(COUNT[22]),
        .I4(COUNT[21]),
        .I5(COUNT3[21]),
        .O(COUNT2_carry__0_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    COUNT2_carry__0_i_2
       (.I0(COUNT3[20]),
        .I1(COUNT[20]),
        .I2(COUNT3[19]),
        .I3(COUNT[19]),
        .I4(COUNT[18]),
        .I5(COUNT3[18]),
        .O(COUNT2_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    COUNT2_carry__0_i_3
       (.I0(COUNT3[17]),
        .I1(COUNT[17]),
        .I2(COUNT3[16]),
        .I3(COUNT[16]),
        .I4(COUNT[15]),
        .I5(COUNT3[15]),
        .O(COUNT2_carry__0_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    COUNT2_carry__0_i_4
       (.I0(COUNT3[14]),
        .I1(COUNT[14]),
        .I2(COUNT3[13]),
        .I3(COUNT[13]),
        .I4(COUNT[12]),
        .I5(COUNT3[12]),
        .O(COUNT2_carry__0_i_4_n_0));
  CARRY4 COUNT2_carry__1
       (.CI(COUNT2_carry__0_n_0),
        .CO({NLW_COUNT2_carry__1_CO_UNCONNECTED[3],COUNT2,COUNT2_carry__1_n_2,COUNT2_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_COUNT2_carry__1_O_UNCONNECTED[3:0]),
        .S({1'b0,COUNT2_carry__1_i_1_n_0,COUNT2_carry__1_i_2_n_0,COUNT2_carry__1_i_3_n_0}));
  LUT4 #(
    .INIT(16'h9009)) 
    COUNT2_carry__1_i_1
       (.I0(COUNT3[31]),
        .I1(COUNT[31]),
        .I2(COUNT3[30]),
        .I3(COUNT[30]),
        .O(COUNT2_carry__1_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    COUNT2_carry__1_i_2
       (.I0(COUNT3[29]),
        .I1(COUNT[29]),
        .I2(COUNT3[28]),
        .I3(COUNT[28]),
        .I4(COUNT[27]),
        .I5(COUNT3[27]),
        .O(COUNT2_carry__1_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    COUNT2_carry__1_i_3
       (.I0(COUNT3[26]),
        .I1(COUNT[26]),
        .I2(COUNT3[25]),
        .I3(COUNT[25]),
        .I4(COUNT[24]),
        .I5(COUNT3[24]),
        .O(COUNT2_carry__1_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    COUNT2_carry_i_1
       (.I0(COUNT3[11]),
        .I1(COUNT[11]),
        .I2(COUNT3[10]),
        .I3(COUNT[10]),
        .I4(COUNT[9]),
        .I5(COUNT3[9]),
        .O(COUNT2_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    COUNT2_carry_i_2
       (.I0(COUNT3[8]),
        .I1(COUNT[8]),
        .I2(COUNT3[7]),
        .I3(COUNT[7]),
        .I4(COUNT[6]),
        .I5(COUNT3[6]),
        .O(COUNT2_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    COUNT2_carry_i_3
       (.I0(COUNT3[5]),
        .I1(COUNT[5]),
        .I2(COUNT3[4]),
        .I3(COUNT[4]),
        .I4(COUNT[3]),
        .I5(COUNT3[3]),
        .O(COUNT2_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h6006000000006006)) 
    COUNT2_carry_i_4
       (.I0(COUNT[0]),
        .I1(NUM_of_packet[0]),
        .I2(COUNT3[2]),
        .I3(COUNT[2]),
        .I4(COUNT[1]),
        .I5(COUNT3[1]),
        .O(COUNT2_carry_i_4_n_0));
  CARRY4 COUNT3_carry
       (.CI(1'b0),
        .CO({COUNT3_carry_n_0,COUNT3_carry_n_1,COUNT3_carry_n_2,COUNT3_carry_n_3}),
        .CYINIT(NUM_of_packet[0]),
        .DI(NUM_of_packet[4:1]),
        .O(COUNT3[4:1]),
        .S({COUNT3_carry_i_1_n_0,COUNT3_carry_i_2_n_0,COUNT3_carry_i_3_n_0,COUNT3_carry_i_4_n_0}));
  CARRY4 COUNT3_carry__0
       (.CI(COUNT3_carry_n_0),
        .CO({COUNT3_carry__0_n_0,COUNT3_carry__0_n_1,COUNT3_carry__0_n_2,COUNT3_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(NUM_of_packet[8:5]),
        .O(COUNT3[8:5]),
        .S({COUNT3_carry__0_i_1_n_0,COUNT3_carry__0_i_2_n_0,COUNT3_carry__0_i_3_n_0,COUNT3_carry__0_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__0_i_1
       (.I0(NUM_of_packet[8]),
        .O(COUNT3_carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__0_i_2
       (.I0(NUM_of_packet[7]),
        .O(COUNT3_carry__0_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__0_i_3
       (.I0(NUM_of_packet[6]),
        .O(COUNT3_carry__0_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__0_i_4
       (.I0(NUM_of_packet[5]),
        .O(COUNT3_carry__0_i_4_n_0));
  CARRY4 COUNT3_carry__1
       (.CI(COUNT3_carry__0_n_0),
        .CO({COUNT3_carry__1_n_0,COUNT3_carry__1_n_1,COUNT3_carry__1_n_2,COUNT3_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(NUM_of_packet[12:9]),
        .O(COUNT3[12:9]),
        .S({COUNT3_carry__1_i_1_n_0,COUNT3_carry__1_i_2_n_0,COUNT3_carry__1_i_3_n_0,COUNT3_carry__1_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__1_i_1
       (.I0(NUM_of_packet[12]),
        .O(COUNT3_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__1_i_2
       (.I0(NUM_of_packet[11]),
        .O(COUNT3_carry__1_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__1_i_3
       (.I0(NUM_of_packet[10]),
        .O(COUNT3_carry__1_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__1_i_4
       (.I0(NUM_of_packet[9]),
        .O(COUNT3_carry__1_i_4_n_0));
  CARRY4 COUNT3_carry__2
       (.CI(COUNT3_carry__1_n_0),
        .CO({COUNT3_carry__2_n_0,COUNT3_carry__2_n_1,COUNT3_carry__2_n_2,COUNT3_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(NUM_of_packet[16:13]),
        .O(COUNT3[16:13]),
        .S({COUNT3_carry__2_i_1_n_0,COUNT3_carry__2_i_2_n_0,COUNT3_carry__2_i_3_n_0,COUNT3_carry__2_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__2_i_1
       (.I0(NUM_of_packet[16]),
        .O(COUNT3_carry__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__2_i_2
       (.I0(NUM_of_packet[15]),
        .O(COUNT3_carry__2_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__2_i_3
       (.I0(NUM_of_packet[14]),
        .O(COUNT3_carry__2_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__2_i_4
       (.I0(NUM_of_packet[13]),
        .O(COUNT3_carry__2_i_4_n_0));
  CARRY4 COUNT3_carry__3
       (.CI(COUNT3_carry__2_n_0),
        .CO({COUNT3_carry__3_n_0,COUNT3_carry__3_n_1,COUNT3_carry__3_n_2,COUNT3_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(NUM_of_packet[20:17]),
        .O(COUNT3[20:17]),
        .S({COUNT3_carry__3_i_1_n_0,COUNT3_carry__3_i_2_n_0,COUNT3_carry__3_i_3_n_0,COUNT3_carry__3_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__3_i_1
       (.I0(NUM_of_packet[20]),
        .O(COUNT3_carry__3_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__3_i_2
       (.I0(NUM_of_packet[19]),
        .O(COUNT3_carry__3_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__3_i_3
       (.I0(NUM_of_packet[18]),
        .O(COUNT3_carry__3_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__3_i_4
       (.I0(NUM_of_packet[17]),
        .O(COUNT3_carry__3_i_4_n_0));
  CARRY4 COUNT3_carry__4
       (.CI(COUNT3_carry__3_n_0),
        .CO({COUNT3_carry__4_n_0,COUNT3_carry__4_n_1,COUNT3_carry__4_n_2,COUNT3_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI(NUM_of_packet[24:21]),
        .O(COUNT3[24:21]),
        .S({COUNT3_carry__4_i_1_n_0,COUNT3_carry__4_i_2_n_0,COUNT3_carry__4_i_3_n_0,COUNT3_carry__4_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__4_i_1
       (.I0(NUM_of_packet[24]),
        .O(COUNT3_carry__4_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__4_i_2
       (.I0(NUM_of_packet[23]),
        .O(COUNT3_carry__4_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__4_i_3
       (.I0(NUM_of_packet[22]),
        .O(COUNT3_carry__4_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__4_i_4
       (.I0(NUM_of_packet[21]),
        .O(COUNT3_carry__4_i_4_n_0));
  CARRY4 COUNT3_carry__5
       (.CI(COUNT3_carry__4_n_0),
        .CO({COUNT3_carry__5_n_0,COUNT3_carry__5_n_1,COUNT3_carry__5_n_2,COUNT3_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI(NUM_of_packet[28:25]),
        .O(COUNT3[28:25]),
        .S({COUNT3_carry__5_i_1_n_0,COUNT3_carry__5_i_2_n_0,COUNT3_carry__5_i_3_n_0,COUNT3_carry__5_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__5_i_1
       (.I0(NUM_of_packet[28]),
        .O(COUNT3_carry__5_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__5_i_2
       (.I0(NUM_of_packet[27]),
        .O(COUNT3_carry__5_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__5_i_3
       (.I0(NUM_of_packet[26]),
        .O(COUNT3_carry__5_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__5_i_4
       (.I0(NUM_of_packet[25]),
        .O(COUNT3_carry__5_i_4_n_0));
  CARRY4 COUNT3_carry__6
       (.CI(COUNT3_carry__5_n_0),
        .CO({NLW_COUNT3_carry__6_CO_UNCONNECTED[3:2],COUNT3_carry__6_n_2,COUNT3_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,NUM_of_packet[30:29]}),
        .O({NLW_COUNT3_carry__6_O_UNCONNECTED[3],COUNT3[31:29]}),
        .S({1'b0,COUNT3_carry__6_i_1_n_0,COUNT3_carry__6_i_2_n_0,COUNT3_carry__6_i_3_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__6_i_1
       (.I0(NUM_of_packet[31]),
        .O(COUNT3_carry__6_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__6_i_2
       (.I0(NUM_of_packet[30]),
        .O(COUNT3_carry__6_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry__6_i_3
       (.I0(NUM_of_packet[29]),
        .O(COUNT3_carry__6_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry_i_1
       (.I0(NUM_of_packet[4]),
        .O(COUNT3_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry_i_2
       (.I0(NUM_of_packet[3]),
        .O(COUNT3_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry_i_3
       (.I0(NUM_of_packet[2]),
        .O(COUNT3_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    COUNT3_carry_i_4
       (.I0(NUM_of_packet[1]),
        .O(COUNT3_carry_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \COUNT[0]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT[0]),
        .O(p_1_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[10]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[10]),
        .O(p_1_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[11]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[11]),
        .O(p_1_in[11]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[12]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[12]),
        .O(p_1_in[12]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[13]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[13]),
        .O(p_1_in[13]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[14]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[14]),
        .O(p_1_in[14]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[15]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[15]),
        .O(p_1_in[15]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[16]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[16]),
        .O(p_1_in[16]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[17]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[17]),
        .O(p_1_in[17]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[18]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[18]),
        .O(p_1_in[18]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[19]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[19]),
        .O(p_1_in[19]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[1]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[1]),
        .O(p_1_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[20]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[20]),
        .O(p_1_in[20]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[21]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[21]),
        .O(p_1_in[21]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[22]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[22]),
        .O(p_1_in[22]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[23]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[23]),
        .O(p_1_in[23]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[24]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[24]),
        .O(p_1_in[24]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[25]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[25]),
        .O(p_1_in[25]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[26]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[26]),
        .O(p_1_in[26]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[27]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[27]),
        .O(p_1_in[27]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[28]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[28]),
        .O(p_1_in[28]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[29]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[29]),
        .O(p_1_in[29]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[2]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[2]),
        .O(p_1_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[30]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[30]),
        .O(p_1_in[30]));
  LUT6 #(
    .INIT(64'h0400000000000000)) 
    \COUNT[31]_i_1 
       (.I0(TLAST_FLAG_reg_n_0),
        .I1(m00_axis_tready),
        .I2(INIT),
        .I3(m00_axis_aresetn),
        .I4(m00_axis_tvalid),
        .I5(COUNT2),
        .O(COUNT_0));
  LUT6 #(
    .INIT(64'hF4F4F4F4F4F4F4FF)) 
    \COUNT[31]_i_2 
       (.I0(addTLAST_VETO_reg_n_0),
        .I1(addTLAST),
        .I2(addTLAST_VETO),
        .I3(rd_en_reg_i_2_n_0),
        .I4(m00_axis_tlast),
        .I5(TLAST_FLAG_reg_n_0),
        .O(\COUNT[31]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[31]_i_3 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[31]),
        .O(p_1_in[31]));
  LUT6 #(
    .INIT(64'h0000000004000000)) 
    \COUNT[31]_i_4 
       (.I0(TLAST_FLAG_reg_n_0),
        .I1(m00_axis_tready),
        .I2(INIT),
        .I3(m00_axis_aresetn),
        .I4(m00_axis_tvalid),
        .I5(m00_axis_tlast),
        .O(\COUNT[31]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[3]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[3]),
        .O(p_1_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[4]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[4]),
        .O(p_1_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[5]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[5]),
        .O(p_1_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[6]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[6]),
        .O(p_1_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[7]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[7]),
        .O(p_1_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[8]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[8]),
        .O(p_1_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \COUNT[9]_i_1 
       (.I0(\COUNT[31]_i_4_n_0 ),
        .I1(COUNT0[9]),
        .O(p_1_in[9]));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[0] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[0]),
        .Q(COUNT[0]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[10] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[10]),
        .Q(COUNT[10]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[11] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[11]),
        .Q(COUNT[11]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[12] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[12]),
        .Q(COUNT[12]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[13] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[13]),
        .Q(COUNT[13]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[14] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[14]),
        .Q(COUNT[14]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[15] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[15]),
        .Q(COUNT[15]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[16] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[16]),
        .Q(COUNT[16]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[17] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[17]),
        .Q(COUNT[17]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[18] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[18]),
        .Q(COUNT[18]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[19] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[19]),
        .Q(COUNT[19]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[1] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[1]),
        .Q(COUNT[1]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[20] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[20]),
        .Q(COUNT[20]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[21] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[21]),
        .Q(COUNT[21]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[22] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[22]),
        .Q(COUNT[22]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[23] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[23]),
        .Q(COUNT[23]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[24] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[24]),
        .Q(COUNT[24]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[25] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[25]),
        .Q(COUNT[25]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[26] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[26]),
        .Q(COUNT[26]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[27] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[27]),
        .Q(COUNT[27]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[28] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[28]),
        .Q(COUNT[28]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[29] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[29]),
        .Q(COUNT[29]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[2] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[2]),
        .Q(COUNT[2]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[30] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[30]),
        .Q(COUNT[30]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[31] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[31]),
        .Q(COUNT[31]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[3] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[3]),
        .Q(COUNT[3]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[4] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[4]),
        .Q(COUNT[4]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[5] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[5]),
        .Q(COUNT[5]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[6] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[6]),
        .Q(COUNT[6]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[7] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[7]),
        .Q(COUNT[7]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[8] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[8]),
        .Q(COUNT[8]),
        .R(COUNT_0));
  FDRE #(
    .INIT(1'b0)) 
    \COUNT_reg[9] 
       (.C(m00_axis_aclk),
        .CE(\COUNT[31]_i_2_n_0 ),
        .D(p_1_in[9]),
        .Q(COUNT[9]),
        .R(COUNT_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    \M_AXIS_TDATA_reg[0]_i_1 
       (.I0(input_data_reg[0]),
        .I1(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I2(\M_AXIS_TDATA_reg[17]_i_2_n_0 ),
        .O(M_AXIS_TDATA_reg[0]));
  LUT6 #(
    .INIT(64'h8888888888888FF8)) 
    \M_AXIS_TDATA_reg[10]_i_1 
       (.I0(input_data_reg[10]),
        .I1(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I2(TLAST_COUNT[1]),
        .I3(TLAST_COUNT[2]),
        .I4(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .I5(TLAST_COUNT[0]),
        .O(M_AXIS_TDATA_reg[10]));
  LUT6 #(
    .INIT(64'h888F8F88888F8888)) 
    \M_AXIS_TDATA_reg[11]_i_1 
       (.I0(input_data_reg[11]),
        .I1(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I2(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .I3(TLAST_COUNT[1]),
        .I4(TLAST_COUNT[2]),
        .I5(TLAST_COUNT[0]),
        .O(M_AXIS_TDATA_reg[11]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFF040404)) 
    \M_AXIS_TDATA_reg[12]_i_1 
       (.I0(TLAST_COUNT[2]),
        .I1(TLAST_COUNT[1]),
        .I2(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .I3(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I4(input_data_reg[12]),
        .I5(\M_AXIS_TDATA_reg[18]_i_3_n_0 ),
        .O(M_AXIS_TDATA_reg[12]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    \M_AXIS_TDATA_reg[13]_i_1 
       (.I0(input_data_reg[13]),
        .I1(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I2(\M_AXIS_TDATA_reg[24]_i_3_n_0 ),
        .O(M_AXIS_TDATA_reg[13]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \M_AXIS_TDATA_reg[14]_i_1 
       (.I0(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I1(input_data_reg[14]),
        .O(\M_AXIS_TDATA_reg[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFABAAABAAABAA)) 
    \M_AXIS_TDATA_reg[16]_i_1 
       (.I0(\M_AXIS_TDATA_reg[16]_i_2_n_0 ),
        .I1(\M_AXIS_TDATA_reg[16]_i_3_n_0 ),
        .I2(\M_AXIS_TDATA_reg[16]_i_4_n_0 ),
        .I3(\M_AXIS_TDATA_reg[16]_i_5_n_0 ),
        .I4(input_data_reg[16]),
        .I5(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .O(M_AXIS_TDATA_reg[16]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h0130)) 
    \M_AXIS_TDATA_reg[16]_i_2 
       (.I0(TLAST_COUNT[0]),
        .I1(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .I2(TLAST_COUNT[2]),
        .I3(TLAST_COUNT[1]),
        .O(\M_AXIS_TDATA_reg[16]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \M_AXIS_TDATA_reg[16]_i_3 
       (.I0(almost_full),
        .I1(TLAST_FLAG_reg_n_0),
        .O(\M_AXIS_TDATA_reg[16]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFFDFFFFF)) 
    \M_AXIS_TDATA_reg[16]_i_4 
       (.I0(m00_axis_tready),
        .I1(INIT),
        .I2(m00_axis_aresetn),
        .I3(TLAST_COUNT[1]),
        .I4(TLAST_COUNT[0]),
        .O(\M_AXIS_TDATA_reg[16]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hC1)) 
    \M_AXIS_TDATA_reg[16]_i_5 
       (.I0(TLAST_COUNT[2]),
        .I1(m00_axis_tvalid),
        .I2(input_data_reg_VALID_FLAG_reg_n_0),
        .O(\M_AXIS_TDATA_reg[16]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    \M_AXIS_TDATA_reg[17]_i_1 
       (.I0(input_data_reg[17]),
        .I1(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I2(\M_AXIS_TDATA_reg[17]_i_2_n_0 ),
        .O(M_AXIS_TDATA_reg[17]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hABBAAAAA)) 
    \M_AXIS_TDATA_reg[17]_i_2 
       (.I0(\M_AXIS_TDATA_reg[30]_i_4_n_0 ),
        .I1(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .I2(TLAST_COUNT[2]),
        .I3(TLAST_COUNT[1]),
        .I4(TLAST_COUNT[0]),
        .O(\M_AXIS_TDATA_reg[17]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFF888)) 
    \M_AXIS_TDATA_reg[18]_i_1 
       (.I0(\M_AXIS_TDATA_reg[18]_i_2_n_0 ),
        .I1(TLAST_COUNT[0]),
        .I2(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I3(input_data_reg[18]),
        .I4(\M_AXIS_TDATA_reg[18]_i_3_n_0 ),
        .O(M_AXIS_TDATA_reg[18]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \M_AXIS_TDATA_reg[18]_i_2 
       (.I0(TLAST_COUNT[2]),
        .I1(TLAST_COUNT[1]),
        .I2(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .O(\M_AXIS_TDATA_reg[18]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000000000808FF08)) 
    \M_AXIS_TDATA_reg[18]_i_3 
       (.I0(TLAST_COUNT[0]),
        .I1(input_data_reg_VALID_FLAG_reg_n_0),
        .I2(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .I3(\M_AXIS_TDATA_reg[18]_i_4_n_0 ),
        .I4(input_data_reg_VALID_FLAG_i_3_n_0),
        .I5(TLAST_COUNT[2]),
        .O(\M_AXIS_TDATA_reg[18]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000400000000)) 
    \M_AXIS_TDATA_reg[18]_i_4 
       (.I0(almost_full),
        .I1(TLAST_FLAG_reg_n_0),
        .I2(input_data_reg_VALID_FLAG_reg_n_0),
        .I3(m00_axis_tvalid),
        .I4(TLAST_COUNT[1]),
        .I5(TLAST_COUNT[0]),
        .O(\M_AXIS_TDATA_reg[18]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF8F888888)) 
    \M_AXIS_TDATA_reg[19]_i_1 
       (.I0(input_data_reg[19]),
        .I1(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I2(TLAST_COUNT[1]),
        .I3(TLAST_COUNT[2]),
        .I4(\M_AXIS_TDATA_reg[25]_i_2_n_0 ),
        .I5(\M_AXIS_TDATA_reg[26]_i_2_n_0 ),
        .O(M_AXIS_TDATA_reg[19]));
  LUT6 #(
    .INIT(64'h8888888888888FF8)) 
    \M_AXIS_TDATA_reg[1]_i_1 
       (.I0(input_data_reg[1]),
        .I1(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I2(TLAST_COUNT[1]),
        .I3(TLAST_COUNT[2]),
        .I4(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .I5(TLAST_COUNT[0]),
        .O(M_AXIS_TDATA_reg[1]));
  LUT6 #(
    .INIT(64'h888F8F8888888888)) 
    \M_AXIS_TDATA_reg[20]_i_1 
       (.I0(input_data_reg[20]),
        .I1(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I2(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .I3(TLAST_COUNT[2]),
        .I4(TLAST_COUNT[1]),
        .I5(TLAST_COUNT[0]),
        .O(M_AXIS_TDATA_reg[20]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \M_AXIS_TDATA_reg[21]_i_1 
       (.I0(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I1(input_data_reg[21]),
        .O(\M_AXIS_TDATA_reg[21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \M_AXIS_TDATA_reg[22]_i_1 
       (.I0(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I1(input_data_reg[22]),
        .O(\M_AXIS_TDATA_reg[22]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFF040404)) 
    \M_AXIS_TDATA_reg[24]_i_1 
       (.I0(TLAST_COUNT[2]),
        .I1(TLAST_COUNT[1]),
        .I2(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .I3(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I4(input_data_reg[24]),
        .I5(\M_AXIS_TDATA_reg[24]_i_3_n_0 ),
        .O(M_AXIS_TDATA_reg[24]));
  LUT6 #(
    .INIT(64'hFFFFDFFFFFFFFFFF)) 
    \M_AXIS_TDATA_reg[24]_i_2 
       (.I0(TLAST_FLAG_reg_n_0),
        .I1(almost_full),
        .I2(m00_axis_tvalid),
        .I3(m00_axis_aresetn),
        .I4(INIT),
        .I5(m00_axis_tready),
        .O(\M_AXIS_TDATA_reg[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0030002100000000)) 
    \M_AXIS_TDATA_reg[24]_i_3 
       (.I0(TLAST_COUNT[2]),
        .I1(input_data_reg_VALID_FLAG_i_3_n_0),
        .I2(m00_axis_tvalid),
        .I3(\M_AXIS_TDATA_reg[16]_i_3_n_0 ),
        .I4(input_data_reg_VALID_FLAG_reg_n_0),
        .I5(input_data_reg_VALID_FLAG_i_2_n_0),
        .O(\M_AXIS_TDATA_reg[24]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hF8F8FFF8FFF8F8F8)) 
    \M_AXIS_TDATA_reg[25]_i_1 
       (.I0(input_data_reg[25]),
        .I1(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I2(\M_AXIS_TDATA_reg[26]_i_2_n_0 ),
        .I3(\M_AXIS_TDATA_reg[25]_i_2_n_0 ),
        .I4(TLAST_COUNT[2]),
        .I5(TLAST_COUNT[1]),
        .O(M_AXIS_TDATA_reg[25]));
  LUT6 #(
    .INIT(64'h0000000000200000)) 
    \M_AXIS_TDATA_reg[25]_i_2 
       (.I0(m00_axis_tready),
        .I1(addTLAST_VETO),
        .I2(m00_axis_tvalid),
        .I3(almost_full),
        .I4(TLAST_FLAG_reg_n_0),
        .I5(TLAST_COUNT[0]),
        .O(\M_AXIS_TDATA_reg[25]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    \M_AXIS_TDATA_reg[26]_i_1 
       (.I0(input_data_reg[26]),
        .I1(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I2(\M_AXIS_TDATA_reg[26]_i_2_n_0 ),
        .O(M_AXIS_TDATA_reg[26]));
  LUT6 #(
    .INIT(64'h0000000000400004)) 
    \M_AXIS_TDATA_reg[26]_i_2 
       (.I0(TLAST_COUNT[2]),
        .I1(input_data_reg_VALID_FLAG_i_2_n_0),
        .I2(input_data_reg_VALID_FLAG_reg_n_0),
        .I3(input_data_reg_VALID_FLAG_i_3_n_0),
        .I4(m00_axis_tvalid),
        .I5(\M_AXIS_TDATA_reg[16]_i_3_n_0 ),
        .O(\M_AXIS_TDATA_reg[26]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \M_AXIS_TDATA_reg[27]_i_1 
       (.I0(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I1(input_data_reg[27]),
        .O(\M_AXIS_TDATA_reg[27]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \M_AXIS_TDATA_reg[29]_i_1 
       (.I0(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I1(input_data_reg[29]),
        .O(\M_AXIS_TDATA_reg[29]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h88888F8888888FF8)) 
    \M_AXIS_TDATA_reg[2]_i_1 
       (.I0(input_data_reg[2]),
        .I1(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I2(TLAST_COUNT[2]),
        .I3(TLAST_COUNT[1]),
        .I4(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .I5(TLAST_COUNT[0]),
        .O(M_AXIS_TDATA_reg[2]));
  LUT6 #(
    .INIT(64'hEFFFEFEF00000000)) 
    \M_AXIS_TDATA_reg[30]_i_1 
       (.I0(\M_AXIS_TDATA_reg[31]_i_3_n_0 ),
        .I1(INIT),
        .I2(m00_axis_aresetn),
        .I3(almost_full),
        .I4(\M_AXIS_TDATA_reg[31]_i_4_n_0 ),
        .I5(M_AXIS_TVALID_reg11_out),
        .O(\M_AXIS_TDATA_reg[30]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \M_AXIS_TDATA_reg[30]_i_2 
       (.I0(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I1(input_data_reg[30]),
        .O(\M_AXIS_TDATA_reg[30]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'hABBA)) 
    \M_AXIS_TDATA_reg[30]_i_3 
       (.I0(\M_AXIS_TDATA_reg[30]_i_4_n_0 ),
        .I1(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .I2(TLAST_COUNT[1]),
        .I3(TLAST_COUNT[2]),
        .O(M_AXIS_TVALID_reg11_out));
  LUT6 #(
    .INIT(64'h0000100000000000)) 
    \M_AXIS_TDATA_reg[30]_i_4 
       (.I0(input_data_reg_VALID_FLAG_i_3_n_0),
        .I1(TLAST_COUNT[1]),
        .I2(TLAST_COUNT[0]),
        .I3(TLAST_FLAG_reg_n_0),
        .I4(almost_full),
        .I5(\M_AXIS_TDATA_reg[16]_i_5_n_0 ),
        .O(\M_AXIS_TDATA_reg[30]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EFFFEFEF)) 
    \M_AXIS_TDATA_reg[31]_i_1 
       (.I0(\M_AXIS_TDATA_reg[31]_i_3_n_0 ),
        .I1(INIT),
        .I2(m00_axis_aresetn),
        .I3(almost_full),
        .I4(\M_AXIS_TDATA_reg[31]_i_4_n_0 ),
        .I5(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .O(\M_AXIS_TDATA_reg[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hEFFFEFEF)) 
    \M_AXIS_TDATA_reg[31]_i_2 
       (.I0(\M_AXIS_TDATA_reg[31]_i_3_n_0 ),
        .I1(INIT),
        .I2(m00_axis_aresetn),
        .I3(almost_full),
        .I4(\M_AXIS_TDATA_reg[31]_i_4_n_0 ),
        .O(\M_AXIS_TDATA_reg[31]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h0001FFFF)) 
    \M_AXIS_TDATA_reg[31]_i_3 
       (.I0(TLAST_COUNT[1]),
        .I1(TLAST_COUNT[2]),
        .I2(almost_full),
        .I3(TLAST_COUNT[0]),
        .I4(TLAST_FLAG_reg_n_0),
        .O(\M_AXIS_TDATA_reg[31]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h00C0C084)) 
    \M_AXIS_TDATA_reg[31]_i_4 
       (.I0(input_data_reg_VALID_FLAG_reg_n_0),
        .I1(m00_axis_tready),
        .I2(m00_axis_tvalid),
        .I3(TLAST_COUNT[1]),
        .I4(TLAST_COUNT[2]),
        .O(\M_AXIS_TDATA_reg[31]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0000000055555557)) 
    \M_AXIS_TDATA_reg[31]_i_5 
       (.I0(TLAST_FLAG_reg_n_0),
        .I1(TLAST_COUNT[0]),
        .I2(almost_full),
        .I3(TLAST_COUNT[2]),
        .I4(TLAST_COUNT[1]),
        .I5(addTLAST_VETO),
        .O(\M_AXIS_TDATA_reg[31]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h88888F8888888FF8)) 
    \M_AXIS_TDATA_reg[3]_i_1 
       (.I0(input_data_reg[3]),
        .I1(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I2(TLAST_COUNT[1]),
        .I3(TLAST_COUNT[2]),
        .I4(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .I5(TLAST_COUNT[0]),
        .O(M_AXIS_TDATA_reg[3]));
  LUT6 #(
    .INIT(64'h8888888888F88888)) 
    \M_AXIS_TDATA_reg[4]_i_1 
       (.I0(input_data_reg[4]),
        .I1(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I2(TLAST_COUNT[0]),
        .I3(TLAST_COUNT[2]),
        .I4(TLAST_COUNT[1]),
        .I5(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .O(M_AXIS_TDATA_reg[4]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \M_AXIS_TDATA_reg[5]_i_1 
       (.I0(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I1(input_data_reg[5]),
        .O(\M_AXIS_TDATA_reg[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \M_AXIS_TDATA_reg[6]_i_1 
       (.I0(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I1(input_data_reg[6]),
        .O(\M_AXIS_TDATA_reg[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFEEE)) 
    \M_AXIS_TDATA_reg[8]_i_1 
       (.I0(\M_AXIS_TDATA_reg[8]_i_2_n_0 ),
        .I1(\M_AXIS_TDATA_reg[18]_i_3_n_0 ),
        .I2(input_data_reg[8]),
        .I3(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .O(M_AXIS_TDATA_reg[8]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h0130)) 
    \M_AXIS_TDATA_reg[8]_i_2 
       (.I0(TLAST_COUNT[0]),
        .I1(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .I2(TLAST_COUNT[1]),
        .I3(TLAST_COUNT[2]),
        .O(\M_AXIS_TDATA_reg[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF8F888888)) 
    \M_AXIS_TDATA_reg[9]_i_1 
       (.I0(input_data_reg[9]),
        .I1(\M_AXIS_TDATA_reg[31]_i_5_n_0 ),
        .I2(TLAST_COUNT[1]),
        .I3(TLAST_COUNT[2]),
        .I4(\M_AXIS_TDATA_reg[25]_i_2_n_0 ),
        .I5(\M_AXIS_TDATA_reg[26]_i_2_n_0 ),
        .O(M_AXIS_TDATA_reg[9]));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[0] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(M_AXIS_TDATA_reg[0]),
        .Q(m00_axis_tdata[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[10] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(M_AXIS_TDATA_reg[10]),
        .Q(m00_axis_tdata[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[11] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(M_AXIS_TDATA_reg[11]),
        .Q(m00_axis_tdata[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[12] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(M_AXIS_TDATA_reg[12]),
        .Q(m00_axis_tdata[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[13] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(M_AXIS_TDATA_reg[13]),
        .Q(m00_axis_tdata[13]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[14] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(\M_AXIS_TDATA_reg[14]_i_1_n_0 ),
        .Q(m00_axis_tdata[14]),
        .S(\M_AXIS_TDATA_reg[30]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[15] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(input_data_reg[15]),
        .Q(m00_axis_tdata[15]),
        .R(\M_AXIS_TDATA_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[16] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(M_AXIS_TDATA_reg[16]),
        .Q(m00_axis_tdata[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[17] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(M_AXIS_TDATA_reg[17]),
        .Q(m00_axis_tdata[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[18] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(M_AXIS_TDATA_reg[18]),
        .Q(m00_axis_tdata[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[19] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(M_AXIS_TDATA_reg[19]),
        .Q(m00_axis_tdata[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[1] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(M_AXIS_TDATA_reg[1]),
        .Q(m00_axis_tdata[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[20] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(M_AXIS_TDATA_reg[20]),
        .Q(m00_axis_tdata[20]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[21] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(\M_AXIS_TDATA_reg[21]_i_1_n_0 ),
        .Q(m00_axis_tdata[21]),
        .S(\M_AXIS_TDATA_reg[30]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[22] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(\M_AXIS_TDATA_reg[22]_i_1_n_0 ),
        .Q(m00_axis_tdata[22]),
        .S(\M_AXIS_TDATA_reg[30]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[23] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(input_data_reg[23]),
        .Q(m00_axis_tdata[23]),
        .R(\M_AXIS_TDATA_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[24] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(M_AXIS_TDATA_reg[24]),
        .Q(m00_axis_tdata[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[25] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(M_AXIS_TDATA_reg[25]),
        .Q(m00_axis_tdata[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[26] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(M_AXIS_TDATA_reg[26]),
        .Q(m00_axis_tdata[26]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[27] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(\M_AXIS_TDATA_reg[27]_i_1_n_0 ),
        .Q(m00_axis_tdata[27]),
        .S(\M_AXIS_TDATA_reg[30]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[28] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(input_data_reg[28]),
        .Q(m00_axis_tdata[28]),
        .R(\M_AXIS_TDATA_reg[31]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[29] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(\M_AXIS_TDATA_reg[29]_i_1_n_0 ),
        .Q(m00_axis_tdata[29]),
        .S(\M_AXIS_TDATA_reg[30]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[2] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(M_AXIS_TDATA_reg[2]),
        .Q(m00_axis_tdata[2]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[30] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(\M_AXIS_TDATA_reg[30]_i_2_n_0 ),
        .Q(m00_axis_tdata[30]),
        .S(\M_AXIS_TDATA_reg[30]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[31] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(input_data_reg[31]),
        .Q(m00_axis_tdata[31]),
        .R(\M_AXIS_TDATA_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[3] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(M_AXIS_TDATA_reg[3]),
        .Q(m00_axis_tdata[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[4] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(M_AXIS_TDATA_reg[4]),
        .Q(m00_axis_tdata[4]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[5] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(\M_AXIS_TDATA_reg[5]_i_1_n_0 ),
        .Q(m00_axis_tdata[5]),
        .S(\M_AXIS_TDATA_reg[30]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[6] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(\M_AXIS_TDATA_reg[6]_i_1_n_0 ),
        .Q(m00_axis_tdata[6]),
        .S(\M_AXIS_TDATA_reg[30]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[7] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(input_data_reg[7]),
        .Q(m00_axis_tdata[7]),
        .R(\M_AXIS_TDATA_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[8] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(M_AXIS_TDATA_reg[8]),
        .Q(m00_axis_tdata[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \M_AXIS_TDATA_reg_reg[9] 
       (.C(m00_axis_aclk),
        .CE(\M_AXIS_TDATA_reg[31]_i_2_n_0 ),
        .D(M_AXIS_TDATA_reg[9]),
        .Q(m00_axis_tdata[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000080000000000)) 
    M_AXIS_TLAST_reg_i_1
       (.I0(m00_axis_tlast),
        .I1(m00_axis_tready),
        .I2(addTLAST_VETO),
        .I3(m00_axis_tvalid),
        .I4(almost_full),
        .I5(TLAST_FLAG_reg_n_0),
        .O(M_AXIS_TLAST_reg));
  LUT6 #(
    .INIT(64'h5755555503000000)) 
    M_AXIS_TLAST_reg_i_2
       (.I0(addTLAST_VETO),
        .I1(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .I2(TLAST_COUNT[1]),
        .I3(TLAST_COUNT[0]),
        .I4(TLAST_COUNT[2]),
        .I5(m00_axis_tlast),
        .O(M_AXIS_TLAST_reg_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    M_AXIS_TLAST_reg_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(M_AXIS_TLAST_reg_i_2_n_0),
        .Q(m00_axis_tlast),
        .R(M_AXIS_TLAST_reg));
  LUT6 #(
    .INIT(64'hFFFF2030FFFF2000)) 
    M_AXIS_TVALID_reg_i_1
       (.I0(input_data_reg_VALID_FLAG_reg_n_0),
        .I1(INIT),
        .I2(m00_axis_aresetn),
        .I3(\M_AXIS_TDATA_reg[31]_i_3_n_0 ),
        .I4(M_AXIS_TVALID_reg11_out),
        .I5(m00_axis_tvalid),
        .O(M_AXIS_TVALID_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    M_AXIS_TVALID_reg_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(M_AXIS_TVALID_reg_i_1_n_0),
        .Q(m00_axis_tvalid),
        .R(M_AXIS_TLAST_reg));
  LUT6 #(
    .INIT(64'hF0FFF00020222000)) 
    \TLAST_COUNT[0]_i_1 
       (.I0(\TLAST_COUNT[0]_i_2_n_0 ),
        .I1(m00_axis_tlast),
        .I2(\TLAST_COUNT[0]_i_3_n_0 ),
        .I3(\TLAST_COUNT[0]_i_4_n_0 ),
        .I4(TLAST_COUNT[0]),
        .I5(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .O(\TLAST_COUNT[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    \TLAST_COUNT[0]_i_2 
       (.I0(TLAST_COUNT[1]),
        .I1(TLAST_COUNT[0]),
        .I2(TLAST_COUNT[2]),
        .O(\TLAST_COUNT[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0005005500050057)) 
    \TLAST_COUNT[0]_i_3 
       (.I0(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .I1(\M_AXIS_TDATA_reg[16]_i_3_n_0 ),
        .I2(TLAST_COUNT[2]),
        .I3(TLAST_COUNT[0]),
        .I4(TLAST_COUNT[1]),
        .I5(addTLAST_VETO),
        .O(\TLAST_COUNT[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFEEEFEEEE)) 
    \TLAST_COUNT[0]_i_4 
       (.I0(\TLAST_COUNT[0]_i_5_n_0 ),
        .I1(\TLAST_COUNT[2]_i_3_n_0 ),
        .I2(TLAST_FLAG_reg_n_0),
        .I3(rd_en_reg_i_2_n_0),
        .I4(COUNT2),
        .I5(addTLAST_VETO),
        .O(\TLAST_COUNT[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h000C00AF000C00AE)) 
    \TLAST_COUNT[0]_i_5 
       (.I0(\TLAST_COUNT[0]_i_6_n_0 ),
        .I1(\TLAST_COUNT[0]_i_7_n_0 ),
        .I2(TLAST_COUNT[2]),
        .I3(\M_AXIS_TDATA_reg[16]_i_3_n_0 ),
        .I4(rd_en_reg_i_2_n_0),
        .I5(input_data_reg_VALID_FLAG_reg_n_0),
        .O(\TLAST_COUNT[0]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h35)) 
    \TLAST_COUNT[0]_i_6 
       (.I0(TLAST_COUNT[0]),
        .I1(TLAST_COUNT[2]),
        .I2(TLAST_COUNT[1]),
        .O(\TLAST_COUNT[0]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h0100)) 
    \TLAST_COUNT[0]_i_7 
       (.I0(TLAST_COUNT[1]),
        .I1(input_data_reg_VALID_FLAG_reg_n_0),
        .I2(m00_axis_tvalid),
        .I3(m00_axis_tready),
        .O(\TLAST_COUNT[0]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAAABFFFFAAAA0000)) 
    \TLAST_COUNT[1]_i_1 
       (.I0(\M_AXIS_TDATA_reg[24]_i_3_n_0 ),
        .I1(TLAST_COUNT[2]),
        .I2(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .I3(TLAST_COUNT[0]),
        .I4(\TLAST_COUNT[2]_i_2_n_0 ),
        .I5(TLAST_COUNT[1]),
        .O(\TLAST_COUNT[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h03FF0800)) 
    \TLAST_COUNT[2]_i_1 
       (.I0(TLAST_COUNT[0]),
        .I1(TLAST_COUNT[1]),
        .I2(\M_AXIS_TDATA_reg[24]_i_2_n_0 ),
        .I3(\TLAST_COUNT[2]_i_2_n_0 ),
        .I4(TLAST_COUNT[2]),
        .O(\TLAST_COUNT[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFABAA)) 
    \TLAST_COUNT[2]_i_2 
       (.I0(\TLAST_COUNT[2]_i_3_n_0 ),
        .I1(TLAST_FLAG_reg_n_0),
        .I2(rd_en_reg_i_2_n_0),
        .I3(COUNT2),
        .I4(addTLAST_VETO),
        .I5(\TLAST_COUNT[2]_i_4_n_0 ),
        .O(\TLAST_COUNT[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h00010000)) 
    \TLAST_COUNT[2]_i_3 
       (.I0(TLAST_COUNT[1]),
        .I1(TLAST_COUNT[0]),
        .I2(TLAST_COUNT[2]),
        .I3(almost_full),
        .I4(TLAST_FLAG_reg_n_0),
        .O(\TLAST_COUNT[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000070006100)) 
    \TLAST_COUNT[2]_i_4 
       (.I0(TLAST_COUNT[2]),
        .I1(TLAST_COUNT[1]),
        .I2(m00_axis_tvalid),
        .I3(m00_axis_tready),
        .I4(input_data_reg_VALID_FLAG_reg_n_0),
        .I5(\M_AXIS_TDATA_reg[16]_i_3_n_0 ),
        .O(\TLAST_COUNT[2]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \TLAST_COUNT_reg[0] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\TLAST_COUNT[0]_i_1_n_0 ),
        .Q(TLAST_COUNT[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \TLAST_COUNT_reg[1] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\TLAST_COUNT[1]_i_1_n_0 ),
        .Q(TLAST_COUNT[1]),
        .R(M_AXIS_TLAST_reg));
  FDRE #(
    .INIT(1'b0)) 
    \TLAST_COUNT_reg[2] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\TLAST_COUNT[2]_i_1_n_0 ),
        .Q(TLAST_COUNT[2]),
        .R(M_AXIS_TLAST_reg));
  LUT6 #(
    .INIT(64'hFFFFFFFF00D50055)) 
    TLAST_FLAG_i_1
       (.I0(rd_en_reg_i_3_n_0),
        .I1(COUNT2),
        .I2(m00_axis_tvalid),
        .I3(addTLAST_VETO),
        .I4(m00_axis_tready),
        .I5(TLAST_FLAG_reg_n_0),
        .O(TLAST_FLAG_i_1_n_0));
  FDRE TLAST_FLAG_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(TLAST_FLAG_i_1_n_0),
        .Q(TLAST_FLAG_reg_n_0),
        .R(M_AXIS_TLAST_reg));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h40)) 
    addTLAST_VETO_i_1
       (.I0(INIT),
        .I1(m00_axis_aresetn),
        .I2(addTLAST),
        .O(addTLAST_VETO_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    addTLAST_VETO_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(addTLAST_VETO_i_1_n_0),
        .Q(addTLAST_VETO_reg_n_0),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    input_DATA_VALID_FLAG_i_1
       (.I0(rd_en),
        .I1(empty),
        .O(input_DATA_VALID_FLAG_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    input_DATA_VALID_FLAG_reg
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_DATA_VALID_FLAG_i_1_n_0),
        .Q(input_DATA_VALID_FLAG),
        .R(\input_data_reg[31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \input_data_reg[31]_i_1 
       (.I0(INIT),
        .I1(m00_axis_aresetn),
        .O(\input_data_reg[31]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \input_data_reg[31]_i_2 
       (.I0(TLAST_FLAG_reg_n_0),
        .O(\input_data_reg[31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFDFFFFFD00000000)) 
    input_data_reg_VALID_FLAG_i_1
       (.I0(input_data_reg_VALID_FLAG_i_2_n_0),
        .I1(input_data_reg_VALID_FLAG_i_3_n_0),
        .I2(input_data_reg_VALID_FLAG_i_4_n_0),
        .I3(m00_axis_tvalid),
        .I4(input_data_reg_VALID_FLAG_reg_n_0),
        .I5(input_data_reg_VALID_FLAG_i_5_n_0),
        .O(input_data_reg_VALID_FLAG_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    input_data_reg_VALID_FLAG_i_2
       (.I0(TLAST_COUNT[0]),
        .I1(TLAST_COUNT[1]),
        .O(input_data_reg_VALID_FLAG_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hDF)) 
    input_data_reg_VALID_FLAG_i_3
       (.I0(m00_axis_aresetn),
        .I1(INIT),
        .I2(m00_axis_tready),
        .O(input_data_reg_VALID_FLAG_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    input_data_reg_VALID_FLAG_i_4
       (.I0(TLAST_FLAG_reg_n_0),
        .I1(almost_full),
        .I2(TLAST_COUNT[2]),
        .O(input_data_reg_VALID_FLAG_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h0C080008)) 
    input_data_reg_VALID_FLAG_i_5
       (.I0(input_DATA_VALID_FLAG),
        .I1(m00_axis_aresetn),
        .I2(INIT),
        .I3(TLAST_FLAG_reg_n_0),
        .I4(input_data_reg_VALID_FLAG_reg_n_0),
        .O(input_data_reg_VALID_FLAG_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    input_data_reg_VALID_FLAG_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(input_data_reg_VALID_FLAG_i_1_n_0),
        .Q(input_data_reg_VALID_FLAG_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[0] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[0]),
        .Q(input_data_reg[0]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[10] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[10]),
        .Q(input_data_reg[10]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[11] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[11]),
        .Q(input_data_reg[11]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[12] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[12]),
        .Q(input_data_reg[12]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[13] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[13]),
        .Q(input_data_reg[13]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[14] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[14]),
        .Q(input_data_reg[14]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[15] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[15]),
        .Q(input_data_reg[15]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[16] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[16]),
        .Q(input_data_reg[16]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[17] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[17]),
        .Q(input_data_reg[17]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[18] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[18]),
        .Q(input_data_reg[18]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[19] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[19]),
        .Q(input_data_reg[19]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[1] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[1]),
        .Q(input_data_reg[1]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[20] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[20]),
        .Q(input_data_reg[20]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[21] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[21]),
        .Q(input_data_reg[21]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[22] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[22]),
        .Q(input_data_reg[22]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[23] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[23]),
        .Q(input_data_reg[23]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[24] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[24]),
        .Q(input_data_reg[24]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[25] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[25]),
        .Q(input_data_reg[25]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[26] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[26]),
        .Q(input_data_reg[26]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[27] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[27]),
        .Q(input_data_reg[27]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[28] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[28]),
        .Q(input_data_reg[28]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[29] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[29]),
        .Q(input_data_reg[29]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[2] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[2]),
        .Q(input_data_reg[2]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[30] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[30]),
        .Q(input_data_reg[30]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[31] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[31]),
        .Q(input_data_reg[31]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[3] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[3]),
        .Q(input_data_reg[3]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[4] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[4]),
        .Q(input_data_reg[4]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[5] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[5]),
        .Q(input_data_reg[5]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[6] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[6]),
        .Q(input_data_reg[6]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[7] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[7]),
        .Q(input_data_reg[7]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[8] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[8]),
        .Q(input_data_reg[8]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \input_data_reg_reg[9] 
       (.C(m00_axis_aclk),
        .CE(\input_data_reg[31]_i_2_n_0 ),
        .D(input_data[9]),
        .Q(input_data_reg[9]),
        .R(\input_data_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF0000FB510000)) 
    rd_en_reg_i_1
       (.I0(TLAST_FLAG_reg_n_0),
        .I1(COUNT2),
        .I2(rd_en_reg_i_2_n_0),
        .I3(rd_en_reg_i_3_n_0),
        .I4(rd_en_reg_i_4_n_0),
        .I5(addTLAST_VETO),
        .O(rd_en_reg_i_1_n_0));
  LUT2 #(
    .INIT(4'h7)) 
    rd_en_reg_i_2
       (.I0(m00_axis_tready),
        .I1(m00_axis_tvalid),
        .O(rd_en_reg_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'hB)) 
    rd_en_reg_i_3
       (.I0(addTLAST_VETO_reg_n_0),
        .I1(addTLAST),
        .O(rd_en_reg_i_3_n_0));
  LUT6 #(
    .INIT(64'h00F0001000000010)) 
    rd_en_reg_i_4
       (.I0(almost_full),
        .I1(empty),
        .I2(m00_axis_aresetn),
        .I3(INIT),
        .I4(TLAST_FLAG_reg_n_0),
        .I5(rd_en),
        .O(rd_en_reg_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'hB)) 
    rd_en_reg_i_5
       (.I0(INIT),
        .I1(m00_axis_aresetn),
        .O(addTLAST_VETO));
  FDRE #(
    .INIT(1'b0)) 
    rd_en_reg_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(rd_en_reg_i_1_n_0),
        .Q(rd_en),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
