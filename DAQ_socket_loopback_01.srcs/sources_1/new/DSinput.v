`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/13/2018 01:16:15 PM
// Design Name: 
// Module Name: DSinput
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module DSinput(
    input   data_in_from_pins_p,
    input   data_in_from_pins_n,
    output  data_in_to_device
);
    
    IBUFDS#(
        .DIFF_TERM  ("FALSE"),             // Differential termination
        .IOSTANDARD ("LVDS_25")
    )
    ibufds_inst(
        .I          (data_in_from_pins_p ),
        .IB         (data_in_from_pins_n ),
        .O          (data_in_to_device)
    );
    
    
endmodule
