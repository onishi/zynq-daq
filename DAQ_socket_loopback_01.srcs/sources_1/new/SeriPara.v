`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/13/2018 04:30:53 PM
// Design Name: 
// Module Name: SeriPara
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SeriPara(
input clk_in,
input SerialSignal,
output [31:0] ParallelSignal,
output ParallelEnable
    );
    
    reg [31:0] ParallelSignal_reg;
    reg ParallelEnable_reg;
    reg [5:0] Counter;
    
    initial begin
        ParallelSignal_reg <=32'b0;
        ParallelEnable_reg <= 1'b0;;
        Counter <= 6'b0;
    end
    
    always@ (posedge clk_in)begin
    
        Counter <= Counter + 5'b1;  
        
        if(Counter!=6'd0)begin
            ParallelSignal_reg[31:0] <= { ParallelSignal_reg[30:0],SerialSignal};
        end
        if(Counter==6'd31)begin
            ParallelEnable_reg <= 1'b1;
        end
        if(Counter==6'd32)begin
            Counter<=6'b1;
            ParallelSignal_reg[31:0] <= { {31{1'b0}},SerialSignal};
            ParallelEnable_reg <= 1'b0;
        end

    end
   
    assign ParallelSignal =ParallelSignal_reg;
    assign ParallelEnable = ParallelEnable_reg;


endmodule
