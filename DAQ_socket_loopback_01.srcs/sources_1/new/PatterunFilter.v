`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/14/2018 10:23:49 PM
// Design Name: 
// Module Name: PatterunFilter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PatterunFilter(
input through_filter,
input [31:0] filter_pattern,

input [31:0] in_data,
input in_data_Enable,
output [31:0] out_data,
output out_data_Enable,

input clk_in

    );
    
        reg out_data_Enable_reg;
        reg [31:0] out_data_reg;
        
        initial begin
            out_data_Enable_reg <=1'b0;
            out_data_reg <=1'b0;
        end
    
        always @( posedge clk_in)begin
            
            out_data_reg <=in_data;
            
            out_data_Enable_reg<=
                (in_data_Enable==1'b1) && (through_filter==1'b1) && !( 
                    ( in_data==32'hffffffff ) || 
                    ( in_data==32'b0 ) ||
                    ( in_data==filter_pattern ) 
                );
         
        end
    
    assign out_data = out_data_reg;
    assign out_data_Enable = out_data_Enable_reg;

endmodule
